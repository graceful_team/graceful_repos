
EU GRAcEFUL Project 
https://www.graceful-project.eu

D5.1: Domain-Specific Language for the
Constraint Functional Programming Platform

Tom Schrijvers, Paolo Torrini, Klara Marntirosian

-----------------------------------------------------------------------------
** CPL-Backend  (Constraint Programming Language Backend)

== REQUIREMENTS ==
 * GHC 7.10.3
 * MiniZinc 2.0
 * JDK 8+

== Installation == 
1. You need to provide the directory of the MiniZinc bundle. 
   Depending on your OS, follow the instructions in WindowsAux.hs or 
   LinuxAux.hs.  
2. You then need to import either module WindowsAux or LinuxAux. 
   Depending on your OS, make the appropriate changes in testPrinter.hs, 
   as indicated in the comments.

== Documentation == 

Detailed instructions can be found in

   CPL-Backend/User_Guide.pdf


-----------------------------------------------------------------------------

** ModCP (Modular Constraint Programming Framework)

== REQUIREMENTS ==
 * GHC 7.10

== Installation == 
  Either compile with GHC or build the executable with Cabal.

== Documentation == 

Detailed instructions can be found in

   ModCP/User_Guide.pdf

-----------------------------------------------------------------------------
