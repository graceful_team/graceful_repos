{-# LANGUAGE CPP #-}

module Control.CP.Debug (
  debug,
  imdebug,
  Debug
) where

import Debug.Trace

type Debug = Bool

debug :: String -> a -> a
imdebug :: Show a => String -> a -> a

{-# INLINE debug #-}
{-# INLINE imdebug #-}

#ifdef DEBUG
debug = trace
imdebug s a = trace ("imdebug " ++ s ++ ": " ++ (show a)) a
#else
debug = flip const
imdebug = flip const
#endif
