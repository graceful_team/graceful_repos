{- 
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Tom Schrijvers, Paul Torrini
 -}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE BangPatterns #-}

-- corresponds to Control.CP.Transformers in monadiccp. Evaluation is now 
-- implemented as an effect handler.


module Control.CP.FMTransformers (
  eval,
  continue,
  Carrier(..),
  dbs,
  continueDB,
  DBCarrier(..),
  nbs,
  continueNB,
  NBCarrier(..)
) where 

import Control.CP.Solver
import Control.CP.FMSearchTree
import Control.CP.Queue
import Control.CP.Debug

--------------------------------------------------------------------------------
-- EVALUATION
--------------------------------------------------------------------------------

-- essentially, defines the lifting of 
-- the result type R (i.e. solver[a]) to a function that takes a
-- continuation : K = L*K -> R 
newtype Carrier solver a  =  
        C { unC :: [(Label solver, Carrier solver a)] -> solver [a] }

-- the algebra is defined on the lifted result type 
-- gen lifts the result type
eval :: (Solver solver, Term solver t)
     => Tree solver t a -> solver [a]
eval tree = debug "eval" $ flip unC [] $ fold alg gen tree where
-- eval tree = unC (fold alg gen tree) [] 

  alg :: (Solver solver, Term solver t) => 
                (CP solver t) (Carrier solver a) -> (Carrier solver a)
  alg Fail
    =  C continue
  alg (NewVar k)
    =  C (\q -> do v <- newvar
                   unC (k v) q)
  alg (Add c k)
    =  C (\q -> do b <- Control.CP.Solver.add c
                   if b then unC k q
                        else continue q)
  alg (Try l r)
    =  C (\q -> do now <- markn 2
                   let q' = (now,l) : (now,r) : q
                   continue q')
  alg (Label p)
    =  C (\q -> do c <- p
                   unC c q)

  gen :: (Solver solver) => a -> Carrier solver a
  gen x  =  C (\q -> do  xs <- continue q
                         return (x:xs))


{-- when the continuation list is empty, lifts it to the result type
  - when the continuation list is not empty, extracs the CPS-style 
   Carrier argument from the head and applies it to the tail 
   (the new continuation) 
-}
continue :: (Solver solver) => [(Label solver, Carrier solver a)] -> solver [a]
continue []
  =  return []
continue ((l,p):q)
  =  do goto l
        unC p q


---------------------------------------------------------------

{- Depth-Bounded Search -}

newtype DBCarrier solver a  =  
    DBC { unDBC :: 
           Int -> [(Label solver, DBCarrier solver a, Int)] -> solver [a] }


dbs :: (Solver solver, Term solver t)
     => Int -> Tree solver t a -> solver [a]
dbs bound tree = debug "eval" $ unDBC (fold alg (gen bound) tree) bound [] where

  alg :: (Solver solver, Term solver t) => 
                (CP solver t) (DBCarrier solver a) -> 
                                  (DBCarrier solver a)
  alg Fail
    =  DBC (continueDB)
  alg (NewVar k)
    =  DBC (\n q -> do v <- newvar
                       unDBC (k v) n q)
  alg (Add c k)
    =  DBC (\n q -> do b <- Control.CP.Solver.add c
                       if b then unDBC k n q
                          else continueDB n q)
  alg (Try l r)
    =  DBC (\n q -> do now <- markn 2
                       let q' = (now, l, n-1) : (now, r, n-1) : q
                       continueDB n q')
  alg (Label p)
    =  DBC (\n q -> do c <- p
                       unDBC c n q)

  gen :: (Solver solver) => Int -> a -> DBCarrier solver a
  gen n x  =  DBC (\_ q -> do  xs <- continueDB n q  
                               return (x:xs))

continueDB :: (Solver solver) => Int -> 
             [(Label solver, DBCarrier solver a, Int)] -> solver [a]
continueDB n [] =  return []
continueDB n ((l,p,x):q) 
  =  do goto l
        nextDB p q n x 

nextDB :: (Solver solver) => DBCarrier solver a -> 
          [(Label solver, DBCarrier solver a, Int)] -> 
                 Int -> Int -> solver [a]
nextDB p q n x 
 | x == 0    =  continueDB n q     
 | otherwise = unDBC p x q

-------------------------------------------------------------------------

{- Node-Bounded Search -}

newtype NBCarrier solver a  =  
    NBC { unNBC :: Int -> [(Label solver, NBCarrier solver a)] -> solver [a] }


nbs :: (Solver solver, Term solver t)
     => Int -> Tree solver t a -> solver [a]
nbs bound tree = debug "eval" $ unNBC (fold alg (gen bound) tree) bound [] where

  alg :: (Solver solver, Term solver t) => 
                (CP solver t) (NBCarrier solver a) -> 
                                  (NBCarrier solver a)
  alg Fail
    =  NBC (continueNB)
  alg (NewVar k)
    =  NBC (\n q -> do v <- newvar
                       unNBC (k v) n q)
  alg (Add c k)
    =  NBC (\n q -> do b <- Control.CP.Solver.add c
                       if b then unNBC k n q
                          else continueNB n q)
  alg (Try l r)
    =  NBC (\n q -> do now <- markn 2
                       let q' = (now, l) : (now, r) : q
                       continueNB n q')
  alg (Label p)
    =  NBC (\n q -> do c <- p
                       unNBC c n q)

  gen :: (Solver solver) => Int -> a -> NBCarrier solver a
  gen n x  =  NBC (\_ q -> do  xs <- continueNB n q  
                               return (x:xs))

continueNB :: (Solver solver) => Int -> 
             [(Label solver, NBCarrier solver a)] -> solver [a]
continueNB n [] =  return []
continueNB n ((l,p):q) 
  =  do goto l
        nextNB p q n 

nextNB :: (Solver solver) => NBCarrier solver a -> 
          [(Label solver, NBCarrier solver a)] -> 
                 Int -> solver [a]
nextNB p q n  
 | n == 0    =  return []     
 | otherwise =  unNBC p (n-1) q





{-
newtype NBCarrier solver a  =  
    C_NB { unC :: [(Label solver, NBCarrier solver a)] -> solver [a] }
-}
{-
newtype Carrier solver a  =  
    C { unC :: [(Label solver, CarrierDB solver a, Int)] -> solver [a] }
-}

{-
dbs :: (Solver solver, Term solver t)
     => Int -> Tree solver t a -> solver [a]
dbs bound tree = debug "eval" $ flip unC [] $ fold alg gen tree where

  alg :: (Solver solver, Term solver t) => 
                (CP solver t) (Carrier solver a) -> (Carrier solver a)
  alg Fail
    =  C continueDB
  alg (NewVar k)
    = C (\q -> do v <- newvar
                  unC (k v) q)
  alg (Add c k)
    =  C (\q -> do b <- Control.CP.Solver.add c
                   if b then unC k q
                        else continueDB q)
  alg (Try l r)
    =  C (\q -> do now <- markn 2
                   let q' = (now,l, n-1) : (now,r, n-1) : q
                   continueDB q'))
  alg n (Label p)
    =  (n, C (\q -> do c <- p
                       unC c q))
-}
---------------------------------------------------------------
{-
dbs :: (Solver solver, Term solver t)
     => Int -> Tree solver t a -> solver [a]
dbs bound tree = debug "eval" $ flip unC [] $ fold alg gen tree where

  alg :: (Solver solver, Term solver t) => 
                (CP solver t) (Int, Carrier solver a) -> 
                                  (Int, Carrier solver a)
  alg n Fail
    =  (n, C (continueDB n))
  alg n (NewVar k)
    =  (n, C (\q -> do v <- newvar
                       unC (k v) q))
  alg n (Add c k)
    =  (n, C (\q -> do b <- Control.CP.Solver.add c
                       if b then unC k q
                            else continueDB n q))
  alg n (Try l r)
    =  (n, C (\q -> do now <- markn 2
                       let q' = (now,l, n-1) : (now,r, n-1) : q
                       continueDB (n-1) q'))
  alg n (Label p)
    =  (n, C (\q -> do c <- p
                       unC c q))

  gen :: (Solver solver) => a -> (Int, Carrier solver a)
  gen n x  =  (n, C (\q -> do  xs <- continue n q  
                               return (x:xs)))


continueDB :: (Solver solver) => Int -> 
             [(Label solver, Carrier solver a, Int)] -> solver [a]
continueDB n [] =  return []
continueDB n ((l,p,x):q) 
  =  do goto l
        nextT p q n x 

nextT :: Carrier solver a -> [(Label solver, Carrier solver a, Int)] -> 
                 Int -> Int -> solver [a]
nextT p q n x 
 | n == 0    =  continueDB x q     
 | otherwise = unC p q
-}

--------------------------------------------------------------------------

{-
newtype DepthBoundedST (solver :: * -> *) a = DBST Int

instance Solver solver => Transformer (DepthBoundedST solver a) where
  type EvalState (DepthBoundedST solver a)  = ()
  type TreeState (DepthBoundedST solver a)  = Int
  type ForSolver (DepthBoundedST solver a)  = solver
  type ForResult (DepthBoundedST solver a)  = a
  initT (DBST n) _  = return ((),n)
  leftT _ _ ts      = ts - 1
  nextT i q t es ts tree
    | ts == 0    = continue i q t es
    | otherwise  = eval' i tree q t es ts

newtype Carrier1 solver 

eval :: (Solver solver, Term solver r, Queue q, 
         Elem q ~ (Label solver,Tree solver r (ForResult t),TreeState t), 
         Transformer t, ForSolver t ~ solver) 
     => Tree solver r (ForResult t) -> q -> t -> solver (Int,[ForResult t])

-}

-------------------------------------------------------------------------


{-
eval :: (Solver solver, Term solver r, Queue q, Elem q ~ (Label solver,Tree solver r (ForResult t),TreeState t), Transformer t,
         ForSolver t ~ solver) 
     => Tree solver r (ForResult t) -> q -> t -> solver (Int,[ForResult t])
eval tree q t  = debug "eval" $ 
                   do (es,ts) <- initT t tree
                      evalR 0 q t es ts tree

{- Int -> q -> t -> EvalState t -> TreeState t -> 
       Tree solver a -> solver (Int,[a]) -}
evalR :: SearchSig solver q t r (ForResult t) 
evalR i wl h = fold (eval' i wl t es ts) return where 
 eval' i wl t es ts Fail = continue (i+1) wl t es
 eval' !i wl t es ts (Return x) = do (j,xs) <- returnT (i+1) wl t es
                                     return (j,(x:xs)) 
 eval' i wl t es ts (NewVar f) = do v <- newvar
                                   evalR (i+1) wl t es ts (f v)
 eval' i wl t es ts (Add c k) = do b <- Control.CP.Solver.add c 
                                   if b then evalR (i+1) wl t es ts k 
                                        else continue (i+1) wl t es
 eval' i wl t es ts (Try l r) = 
  do now <- markn 2
     let wl' = pushQ (now,l,leftT t es ts) $ pushQ (now,r,rightT t es ts) wl
     continue (i+1) wl' t es
 eval' i wl t es ts (Label m) = do tree <- m
                                    evalR (i+1) wl t es ts tree

{-
eval' i wl h = fold (alg i wl h) return where
  alg i wl h Fail = continue (i+1) wl h
  alg i wl h (NewVar f) = newvar >>= f
  alg i wl h (Add c k) = do b <- Control.CP.Solver.add c 
                                   if b then eval' (i+1) wl h k
                                        else continue (i+1) wl h
  alg i wl h (Try l r) = do now <- markn 2
          let wl' = pushQ (now,l) $ pushQ (now,r) wl
     continue (i+1) wl' h
  alg i wl h (Label m) = do tree <- m 
                            eval' (i+1) wl h tree
-}

continue :: ContinueSig solver q t (ForResult t) 
-- Int -> q -> t -> EvalState t -> solver (Int,[ForResult t])
continue i wl t es  
	| isEmptyQ wl  = endT i wl t es -- return (i,[])
        | otherwise    = let ((past,tree,ts),wl') = popQ wl
                         in  do goto past
                                nextT i tree wl' t es ts 
-}
--------------------------------------------------------------------------------
-- TRANSFORMER
--------------------------------------------------------------------------------
{-
type SearchSig solver q t r a =
     (Solver solver, Term solver r, Queue q, Transformer t,   
          Elem q ~ (Label solver,Tree solver r a,TreeState t),
	  ForSolver t ~ solver) 
     => Int -> q -> t -> EvalState t -> TreeState t -> Tree solver r a -> solver (Int,[a])

type ContinueSig solver q t r a =
     (Solver solver, Term solver r, Queue q, Transformer t,   
          Elem q ~ (Label solver,Tree solver r a,TreeState t),
	  ForSolver t ~ solver) 
     => Int -> q -> t -> EvalState t -> solver (Int,[a])


class Transformer t where
  type EvalState t :: *
  type TreeState t :: *
  type ForSolver t :: (* -> *)
  type ForResult t :: *
  leftT, rightT :: t -> EvalState t -> TreeState t -> TreeState t
  leftT  _ _ = id
  rightT    = leftT
  nextT :: SearchSig (ForSolver t) q t (ForResult t)
  nextT  = eval'
  initT :: t -> Tree (ForSolver t) (ForResult t) -> (ForSolver t) (EvalState t,TreeState t)
  returnT :: ContinueSig solver q t (ForResult t) 
  returnT i wl t es  = continue i wl t es
  endT  :: ContinueSig solver q t (ForResult t)
  endT i wl t es     = return (i,[])

newtype DepthBoundedST (solver :: * -> *) a = DBST Int

instance Solver solver => Transformer (DepthBoundedST solver a) where
  type EvalState (DepthBoundedST solver a)  = ()
  type TreeState (DepthBoundedST solver a)  = Int
  type ForSolver (DepthBoundedST solver a)  = solver
  type ForResult (DepthBoundedST solver a)  = a
  initT (DBST n) _  = return ((),n)
  leftT _ _ ts      = ts - 1
  nextT i q t es ts tree
    | ts == 0    = continue i q t es
    | otherwise  = eval' i tree q t es ts

newtype NodeBoundedST (solver :: * -> *) a = NBST Int

instance Solver solver => Transformer (NodeBoundedST solver a)  where
  type EvalState (NodeBoundedST solver a) = Int
  type TreeState (NodeBoundedST solver a) = ()
  type ForSolver (NodeBoundedST solver a) = solver
  type ForResult (NodeBoundedST solver a) = a
  initT (NBST n) _  = return (n,())
  nextT i q t es ts tree
    | es == 0    = return (i,[])
    | otherwise  = eval' i tree q t (es - 1) ts

-}
