{-
 - Enumerable terms  
 -   
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Paul Torrini, Tom Schrijvers
 -      refactored from monadiccp
-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TransformListComp #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}

module Control.CP.EnumTerm (
  EnumTerm(..),
  assignment, assignments,
  inOrder, firstFail, middleOut, endsOut,
  labelling, levelList, enumerate,
  assignmentWT, assignmentsWT, assignmentsFirst
) where

import GHC.Exts (sortWith)

import Control.CP.Solver
import Control.CP.FMSearchTree
import Control.CP.FMFunctorTree

import Debug.Trace


class (Solver s, Term s t, Show (TermBaseType s t)) => EnumTerm s t where
  type TermBaseType s t :: *

  getDomainSize :: t -> s (Int)
  getDomain :: t -> s [TermBaseType s t]
  setValue :: t -> TermBaseType s t -> s [Constraint s]
  splitDomain :: t -> s ([[Constraint s]],Bool)
  splitDomains :: [t] -> s ([[Constraint s]],[t])
  getValue :: t -> s (Maybe (TermBaseType s t))
  defaultOrder :: [t] -> s [t]
  enumerator :: (FunctorTree f t, TreeSolver f t ~ s) => 
                                   Maybe ([t] -> Free (f t) ())

  getDomainSize x = do
    r <- getDomain x
    return $ length r

  getValue x = do
    d <- getDomain x
    return $ case d of
      [v] -> Just v
      _ -> Nothing

  splitDomain x = do
    d <- getDomain x
    case d of
      [] ->  return ([],True)
      [_] -> return ([[]],True)
      _ ->   do
        rr <- mapM (setValue x) d
        return (rr,True)

  splitDomains [] = return ([[]],[])
  splitDomains (a@(x:b)) = do
    s <- getDomainSize x
    if s==0
      then return ([],[])
      else if s==1 
        then splitDomains b
        else do
          (r,v) <- splitDomain x
          if v
            then return (r,b)
            else return (r,a)

  defaultOrder = firstFail

  enumerator = Nothing


-- notice that enumerator is Nothing
enumerate :: (FunctorTree f t, Term s t, TreeSolver f t ~ s, 
        EnumTerm s t, EnumTerm (TreeSolver f t) t) => [t] -> Free (f t) ()
enumerate = case enumerator of
  Nothing -> labelling defaultOrder
  Just x -> x


assignment :: Show t => (EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  t -> Free (f t) (TermBaseType s t)
assignment q = label $ do 
      y <- getValue q   -- :: Maybe (TermBaseType s t)
      case y of 
          Just x -> return (return x) 
                       -- :: (TreeSolver f t) (Free (f t) (TermBaseType s t))
          _ -> return false 
                      -- = Op Fail


assignmentsFirst :: (Show t, EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  [t] -> Free (f t) [TermBaseType s t]
assignmentsFirst ls = mapM assignment ls >>= \x -> fail $ show x 


assignments :: (Show t, EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  [t] -> Free (f t) [TermBaseType s t]
assignments = mapM assignment


assignmentsWT :: (Show t, EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  [t] -> Free (f t) [TermBaseType s t]
assignmentsWT = mapM assignmentWT 

---------------------------------------------------------------------------

-- functions with some tracing

assignmentWT :: Show t => (EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  t -> Free (f t) (TermBaseType s t)
assignmentWT q = label $ do 
      y <- getValue q   -- :: Maybe (TermBaseType s t)
--------------- tracing
      trace ("\n Assignment: term " ++ (show q) ++ 
             " has value " ++ show y ++ 
             " end**") (return ())
---------------
      case y of 
          Just x -> return (return x) 
                       -- :: (TreeSolver f t) (Free (f t) (TermBaseType s t))
          _ -> return false
                      -- = Op Fail

--------------------------------------------------------------------------

firstFail :: EnumTerm s t => [t] -> s [t]
firstFail qs = do ds <- mapM getDomainSize qs 
                  return [ q | (d,q) <- zip ds qs 
                             , then sortWith by d ]

inOrder :: EnumTerm s t => [t] -> s [t]
inOrder = return

middleOut :: EnumTerm s t => [t] -> s [t]
middleOut l = let n = (length l) `div` 2 in
              return $ interleave (drop n l) (reverse $ take n l)

endsOut :: EnumTerm s t => [t] -> s [t]
endsOut  l = let n = (length l) `div` 2 in
             return $ interleave (reverse $ drop n l) (take n l)

interleave []     ys = ys
interleave (x:xs) ys = x:interleave ys xs

levelList :: (Solver s, FunctorTree f t, TreeSolver f t ~ s) => 
             [Free (f t) ()] -> Free (f t) ()
levelList [] = false
levelList [a] = a
levelList l = 
  let len = length l
      (p1,p2) = splitAt (len `div` 2) l
      in (levelList p1) \/ (levelList p2)
--levelList [] = false
--levelList [a] = a
--levelList (a:b) = a \/ levelList b

labelling :: (FunctorTree f t, TreeSolver f t ~ s, EnumTerm s t) => 
         ([t] -> s [t]) -> [t] -> Free (f t) ()
labelling _ [] = true
labelling o l = label $ do 
  ll <- o l
  (cl,c) <- splitDomains ll
  let ml = map (\l -> foldr (/\) true $ map addC l) cl
  return $ do
    levelList ml
    labelling return c
