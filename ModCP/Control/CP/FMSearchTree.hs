{-
 - The Tree data type, a generic modelling language for constraint solvers.
 -
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Tom Schrijvers, Paul Torrini
 -}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}


-- refactoring of Control.CP.SearchTree without the MonadTree part. 


module Control.CP.FMSearchTree (
  Tree(..),
  TreeX(..),
  transformTree,
  Free(..),
  CP(..),
  indent,
  showTree,
  fold,
  untree
) where

import Control.CP.Solver
import Control.Mixin.Mixin

import Control.Monad
import Control.Monad.Trans.Cont
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Writer
import Control.Monad.Trans.State
import Control.Applicative

import Data.Monoid


-------------------------------------------------------------------------------
----------------------------------- Tree --------------------------------------
-------------------------------------------------------------------------------


-- free monad of a functor f  (f not generally endo)
data Free f a = 
    Return a
  | Op (f (Free f a))

fold :: Functor f => (f b -> b) -> (a -> b) -> (Free f a -> b)
fold alg gen (Return x) = gen x
fold alg gen (Op x) = alg (fmap (fold alg gen) x)

instance Functor f => Monad (Free f) where
   return x     = Return x
   m >>= f      = fold Op f m     

instance Functor f => Functor (Free f) where
   fmap = liftM
--  fmap h m = fold Op (Return . h) m

instance Functor f => Applicative (Free f) where
   pure  = Return
   (<*>) = ap
   m1 *> m2  = fold Op (\x -> m2) m1
 

-- replaces Tree in CP.SearchTree
-- depends on CP.Solver for Term and Constraint
-- s - solver type
-- t - solver term type
-- a - return value type  
data CP (s :: * -> *) t a where 
     Fail :: CP s t a
     NewVar :: (Solver s, Term s t) =>  
                 (t -> a) -> CP s t a 
     Add :: Solver s => Constraint s -> a -> CP s t a
     Try :: Solver s => a -> a -> CP s t a
     Label :: s a -> CP s t a

instance Show a => Show (CP s t a) where
  show Fail        = "Fail"
  show (Try l r)   = "Try (" ++ show l ++ ") (" ++ show r ++ ")"
  show (Add _ t)   = "Add (" ++ show t ++ ")"
  show (NewVar _)  = "NewVar <function>"
  show (Label _)   = "Label <monadic value>"

instance Monad s => Functor (CP s t) where
  fmap g Fail       = Fail 
  fmap g (NewVar f) = NewVar (g . f)
  fmap g (Add c u)  = Add c (g u) 
  fmap g (Try u1 u2) = Try (g u1) (g u2)
  fmap g (Label x) = Label (x >>= \ y -> return $ g y) 
      

type Tree s t a = Free (CP s t) a

type TreeX s a = forall t. (Term s t) => Free (CP s t) a

-------------------------------------------------------------
{-

data Tree s a where
  Fail    :: Tree s a                                  -- failure
  Return  :: a -> Tree s a                             -- finished
  Try     :: Tree s a -> Tree s a -> Tree s a          -- disjunction
  Add     :: Constraint s -> Tree s a -> Tree s a      -- sequentially adding a constraint to a tree
  NewVar  :: Term s t => (t -> Tree s a) -> Tree s a   -- add a new variable to a tree
  Label   :: s (Tree s a) -> Tree s a      	       -- label with a strategy
-}
-------------------------------------------------------------


flattenTree :: Solver s => Tree s t a -> Maybe ([Constraint s],a)
flattenTree (Return a) = Just ([],a)
flattenTree (Op Fail) = Nothing
flattenTree (Op (NewVar _)) = Nothing
flattenTree (Op (Add c t)) = case flattenTree t of
  Nothing -> Nothing
  Just (l,a) -> Just (c:l,a)
flattenTree (Op (Try _ _)) = Nothing
flattenTree (Op (Label _)) = Nothing


transformTree :: Solver s => Mixin (Tree s t a -> Tree s t a)
transformTree _ _ (Return x) = Return x
transformTree _ _ (Op Fail) = (Op Fail)
transformTree _ t (Op (NewVar f)) = Op (NewVar (\x -> t $ f x))
transformTree _ t (Op (Add c x)) = Op (Add c (t x))
transformTree _ t (Op (Try x y)) = Op (Try (t x) (t y))
transformTree _ t (Op (Label m)) = Op (Label $ m >>= (\ y -> return $ t y))


-- Shortcut the search procedure for a Tree that does not contain Try nodes.
-- create a solver monad that returns the result of the Tree, or a specified
-- value upon failure
untree :: (Solver s, Term s t) => v -> Free (CP s t) v -> s v
untree _ (Return x) = return x
untree e (Op (Fail)) = return e
untree e (Op (NewVar f)) = do
    v <- newvar
    untree e (f v)
untree e (Op (Add c t)) = (add c) >>= (\x -> if x then untree e t else return e)
untree _ (Op (Try _ _)) = 
          error "convertion of Try nodes to solver is not supported"
untree e (Op (Label s)) = s >>= untree e


indent :: Int -> String
indent l = replicate (2*l) ' '



showTree :: (Show (Constraint s), Show a, Solver s, Term s t) => 
                            Int -> Tree s t a -> s String
showTree l (Return x) = return $ indent l ++ "Return [" ++ (show x) ++ "]\n"
showTree l (Op Fail) = return $ indent l ++ "Fail\n"
showTree l (Op (Try a b)) = do
  m <- mark
  s1 <- showTree (l+1) a
  goto m
  s2 <- showTree (l+1) b
  return $ indent l ++ "Try\n" ++ s1 ++ s2
showTree l (Op (Add c t)) = do
  s <- showTree (l+1) t
  return $ indent l ++ "Add (" ++ (show c) ++ ")\n" ++ s
showTree l (Op (NewVar f)) = do
  n <- newvar
  s <- showTree (l+1) (f n)
  return $ indent l ++ "NewVar\n" ++ s
showTree l (Op (Label a)) = do
  r <- a
  s <- showTree (l+1) r
  return $ indent l ++ "Label\n" ++ s


-- instance Show (Tree s t a)  where
instance (Show (f b), b~Free f a) => Show (Free f a)  where
  show (Return _)  = "Return"
  show (Op x)      = show x


