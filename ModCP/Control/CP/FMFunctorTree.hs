{-
 - The Tree data type, a generic modelling language for constraint solvers.
 -
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Tom Schrijvers, Paul Torrini
 -      refactored from monadiccp
 -}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}


-- replaces MonadTree from Control.CP.SearchTree, following the same lines

module Control.CP.FMFunctorTree (
  (/\),
  true,
  disj,
  conj,
  disj2,
  prim,
  addC,
  addT,
  exist,
  forall,
  mapTree,
  multiAdd,
  multiAddU,
  multiAdd2,
  multiConstr,
  conjB,
  disjB,
  disjT,
  FunctorTree(..)
) where

import Control.CP.Solver
import Control.Mixin.Mixin

import Control.Monad
import Control.Monad.Trans.Cont
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Writer
import Control.Monad.Trans.State

import Control.CP.FMSearchTree 

import Data.Monoid


------------------------------------------

mapTree :: (Solver s1, Solver s2, Term s1 t, Term s2 t, 
            FunctorTree f t, TreeSolver f t ~ s2) => 
       (forall x. s1 x -> s2 x) -> Tree s1 t a -> Free (f t) a
mapTree _ (Return a) = return a
mapTree _ (Op Fail) = false
mapTree f (Op (Try a b)) = mapTree f a \/ mapTree f b
-- mapTree f (Add c n) = label $ f $ (add c >>= \t -> if t then return (mapTree f n) else return false)
-- mapTree (NewVar _) = undefined
mapTree f (Op (Label l)) = label $ (f l) >>= (\t -> return (mapTree f t))



-------------------------------------------------------------------------------
----------------------------------- Functor Subclass --------------------------
-------------------------------------------------------------------------------

infixl 2 \/

class (Functor (f t), Solver (TreeSolver f t), Term (TreeSolver f t) t) => 
                                                    FunctorTree f t where
  type TreeSolver f t :: * -> *
  false   :: Free (f t) a
  exists  :: (t -> Free (f t) a) -> Free (f t) a
  addTo   :: Constraint (TreeSolver f t) -> Free (f t) a -> Free (f t) a
  (\/)    :: Free (f t) a -> Free (f t) a -> Free (f t) a
  label   :: (TreeSolver f t) (Free (f t) a) -> Free (f t) a


instance (Solver solver, Term solver t) => FunctorTree (CP solver) t where
  type TreeSolver (CP solver) t = solver
  false      =  Op Fail
  exists g   =  Op (NewVar g)
  addTo c x  =  Op (Add c x)
  (\/) x y   =  Op (Try x y)
  label g    =  Op (Label g)



-------------------------------------------------------------------------------
----------------------------------- Sugar -------------------------------------
-------------------------------------------------------------------------------


infixr 3 /\
(/\) :: FunctorTree f t => Free (f t) a -> Free (f t) b -> Free (f t) b
(/\) = (>>)
 
true :: FunctorTree f t => Free (f t) ()
true = return ()

disj :: FunctorTree f t => [Free (f t) a] -> Free (f t) a
disj [] = false
disj x = foldr1 (\/) x

conj :: FunctorTree f t => [Free (f t) ()] -> Free (f t) ()
conj [] = true 
conj a = foldr1 (/\) a

disj2 :: FunctorTree f t => [Free (f t) a] -> Free (f t) a
disj2 (x:  [])  = x
disj2 l        = let (xs,ys)      = split l
                     split []     = ([],[])
                     split (a:as) = let (bs,cs) = split as
                                    in  (a:cs,bs)
                 in  (disj2 xs) \/ (disj2 ys)

prim :: FunctorTree f t => TreeSolver f t a -> Free (f t) a
prim action = label (action >>= return . return)

addC :: FunctorTree f t => Constraint (TreeSolver f t) -> Free (f t) ()
addC c = c `addTo` true

addT :: FunctorTree f t => Constraint (TreeSolver f t) -> Free (f t) Bool
addT c = c `addTo` (return True)

exist :: (FunctorTree f t, Term (TreeSolver f t) t) => 
                              Int -> ([t] -> Free (f t) a) -> Free (f t) a
exist n ftree = f n []
         where f 0 acc  = ftree $ reverse acc
               f n acc  = exists $ \v -> f (n-1) (v:acc)

forall :: (FunctorTree f t, Term (TreeSolver f t) t)  => 
                           [t] -> (t -> Free (f t) ()) -> Free (f t) ()
forall list ftree = conj $ map ftree list


-----------------------------------


conjB :: FunctorTree f t => [Free (f t) Bool] -> Free (f t) Bool
conjB [] = return True 
conjB a = foldr1 (/\) a

disjB :: FunctorTree f t => [Free (f t) Bool] -> Free (f t) Bool
disjB = foldl (\/) false

disjT :: FunctorTree f t => [Free (f t) ()] -> Free (f t) ()
disjT = foldl (\/) false

multiConstr :: FunctorTree f t => [Constraint (TreeSolver f t)] -> [Free (f t) Bool]
multiConstr ls = map (prim . add) ls 

multiAdd :: FunctorTree f t => [Constraint (TreeSolver f t)] -> Free (f t) Bool
multiAdd [] = return True
multiAdd (c:ls) = addTo c (multiAdd ls)

multiAddU :: FunctorTree f t => [Constraint (TreeSolver f t)] -> Free (f t) ()
multiAddU [] = return ()
multiAddU (c:ls) = addTo c (multiAddU ls)

multiAdd2 :: FunctorTree f t => [Constraint (TreeSolver f t)] -> Free (f t) Bool
multiAdd2 ls = conjB $ multiConstr ls 


