{-
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Tom Schrijvers, Paul Torrini
 -      refactored from monadiccp
 -}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Control.CP.FD.Example (
  SFlag(..),
  runSolve,
  solve,
  solveDB,
  solveNB,
  ConcreteExModel,
  example_basic_main,
  example_sat_main_void,
  example_sat_main_single,
  example_sat_main_single_expr,
  example_sat_main_coll_expr,
  example_main_void,
  --example_sat_main,
  --example_min_main,
  --example_min_main_void,
  --example_min_main_single,
  --example_min_main_single_expr,
  --example_min_main_coll_expr,
  labeller,
  postMinimize,
  --
  enumOV,
  enumerateOV,
  propagate,
  enumerateDyn,
  solveBasic,
  solveBasicDB,
  solveBasicNB,
  dynLabelling,
  dynLabellingF,
  dynLabellingT,
  propagateWT,
  enumerateDynWT, 
  traceM,
  traceMB,
  traceDomain,
  ExampleModel, ExampleMinModel, 
  module Control.CP.FD.Interface,
) where


import System.Environment (getArgs)
import Data.Maybe (fromJust,isJust)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.List (init,last)

import Control.CP.FD.OvertonFD.OvertonFD
import Control.CP.FD.OvertonFD.Sugar
import Control.CP.FD.FD
import Control.CP.FD.Model

import Control.CP.Debug
import Debug.Trace hiding (traceM)

import Control.CP.FD.Interface
import Control.CP.FMSearchTree
import Control.CP.FMFunctorTree
import Control.CP.EnumTerm
import Control.CP.FMTransformers
import Control.CP.Solver

import Control.Monad.Trans.Cont

-------------------------------------------------------------------------------

traceM :: (Monad m) => String -> m ()
traceM string = trace string $ return ()

traceMB :: (Monad m) => String -> m Bool
traceMB string = trace string $ return True

traceDomain :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
traceDomain ls = traceMB (concat [lookupAndShow x ++ " ;; " | x <- ls]) 

------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- readapted functions

-- labelling of a variable
-- essentially, gives a disjunction of constraints for the variable
-- disj(x1,x2,...|v) = (v=x1) \/ (v=x2) \/ ... \/ false :: 
--                                           Free ((CP OvertonFD FDVar) Bool
-- where a1 \/ a2 = Op (Try a1 a2)
enumOV :: FDVar -> [Int] -> Free (CP OvertonFD FDVar) ()
enumOV var values = disj $ map addC [ OHasValue var val | val <- values ] 


-- Non-dynamic labelling for a list of variables - gives just a conjunction - with /\ = >>
enumerateOV :: [FDVar] -> [Int] -> Free (CP OvertonFD FDVar) ()
enumerateOV vars values = conj [ enumOV var values | var <- vars ] 


-- Dynamic labelling for a list of variables
propagate :: [FDVar] -> OvertonFD (Free (CP OvertonFD FDVar) Bool)
propagate [] = return (return True)
propagate (v:vs) = do d <- fd_domain v -- reads the current constraint store
                      return $ enumOV v d >> enumerateDyn vs  

-- gives:
--   label (return ((disj(d1|v1) /\ label(return (disj(d2|v2) /\ ... /\ label (return true)...))))) 
enumerateDyn :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
enumerateDyn = label . propagate 

--------------------------------------------------------------------------------

solveBasic :: (Show a, Solver solver, Term solver t) 
      => Tree solver t [a] -> IO ()
solveBasic cmod = print $ solve cmod

solveBasicDB :: (Show a, Solver solver, Term solver t) 
      => Int -> Tree solver t [a] -> IO ()
solveBasicDB n cmod = print $ solveDB n cmod

solveBasicNB :: (Show a, Solver solver, Term solver t) 
      => Int -> Tree solver t [a] -> IO ()
solveBasicNB n cmod = print $ solveNB n cmod


-- uses enumerateDyn and EnumTerm.assignments
dynLabellingT :: Debug -> [FDVar] -> [FDVar] -> 
                  Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
dynLabellingT False ls1 ls2 = enumerateDyn ls1 >> assignments ls2
dynLabellingT True ls1 ls2 = enumerateDynWT ls1 >> assignmentsWT ls2

dynLabelling :: [FDVar] -> [FDVar] -> 
                  Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
dynLabelling = dynLabellingT False 

dynLabellingF :: [FDVar] -> [FDVar] -> 
                  Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
dynLabellingF ls1 ls2 = enumerateDyn ls1 >> assignmentsFirst ls2

-----------------------------------------------------------------------

-- Labeling with some tracing

-- Dynamic labelling for a list of variables
propagateWT :: [FDVar] -> OvertonFD (Free (CP OvertonFD FDVar) Bool)
propagateWT [] = return (return True)
propagateWT (v:vs) = do d <- fd_domain v -- reads the current constraint store
------------- tracing
                        trace ("\n Propagate, start: variable " ++ 
                               show v ++ " has domain " ++ show d ++ 
                               " end// ") (return ())
-------------
                        return $ enumOV v d >> enumerateDyn vs  

-- gives:
--   label (return ((disj(d1|v1) /\ label(return (disj(d2|v2) /\ ... /\ label (return true)...))))) 
enumerateDynWT :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
enumerateDynWT = label . propagateWT 


--------------------------------------------------------------------------------
-- EVALUATION (adapted from ComposableTranformers)
--------------------------------------------------------------------------------

-- complete depth first
solve :: (Solver solver, Term solver t) 
      => Tree solver t a -> [a]
solve model = run $ eval model

-- depth bound
solveDB :: (Solver solver, Term solver t) 
      => Int -> Tree solver t a -> [a]
solveDB n model = run $ dbs n model

-- node bound
solveNB :: (Solver solver, Term solver t) 
      => Int -> Tree solver t a -> [a]
solveNB n model = run $ nbs n model

------------------------------------------------------------------------------
--runSolveSAT x = solve x -- fs x
-- used branch and bound search - NOT IMPLEMENTED
-- runSolveMIN x = solve dfs (bb boundMinimize) x    

data SFlag = EV | DB {dbond :: Int} | NB {nbond :: Int}

runSolve :: (Solver solver, Term solver t) 
      => SFlag -> Tree solver t a -> [a]
runSolve typ x = case typ of
   EV -> solve x
   DB n -> solveDB n x
   NB n -> solveNB n x

type ConcreteExModel u = 
         u -> Free (CP OvertonFD FDVar) ModelCol


example_basic_main :: ConcreteExModel [String] -> SFlag -> IO ()
example_basic_main f typ = do
  args <- getArgs
  case args of
    ("overton_run":r) -> print $ runSolve typ $ 
                 ((f r) :: Tree OvertonFD FDVar ModelCol) 
    [] -> putStr "Solver type required: must be overton_run\n"
    (a:r) -> putStr ("Unsupported solver: " ++ a ++ "\n")



example_sat_main_void :: ConcreteExModel () -> SFlag -> IO ()
example_sat_main_void f = example_basic_main (const $ f ()) 

example_sat_main_single :: Read n => ConcreteExModel n -> SFlag -> IO ()
example_sat_main_single f = example_basic_main (f . read . head) 

example_sat_main_single_expr :: ConcreteExModel ModelInt -> SFlag -> IO ()
example_sat_main_single_expr f = example_basic_main (f . fromInteger . read . head) 

example_sat_main_coll_expr :: ConcreteExModel ModelCol -> SFlag -> IO ()
example_sat_main_coll_expr f = example_basic_main (f . list . foldr (++) [] . map (map fromInteger . read . (\x -> "[" ++ x ++ "]"))) 



------------------------------------------------------------------------------
-- SEARCH STRATEGIES (adapted from Solvers)
------------------------------------------------------------------------------


type ExampleModel u t = (forall s f. (Show (FDIntTerm s), FDSolver s, 
                       Term s t, FunctorTree f t, 
                       TreeSolver f t ~ (FDInstance s)) => 
                         u -> Free (f t) (ModelCol))


type ExampleMinModel u t = (forall s f. (Show (FDIntTerm s), FDSolver s, 
                          Term s t, FunctorTree f t, 
                          TreeSolver f t ~ (FDInstance s)) => 
                            u -> Free (f t) (ModelInt,ModelCol))

postMinimize :: ExampleMinModel u t -> ExampleModel u t
postMinimize m = \x -> do
  (min,res) <- m x
  debug ("postMinimize: min="++(show min)) $ return ()
  label $ do
    setMinimizeVar min
    return $ return res

labeller
  :: (Show (FDIntTerm s), FunctorTree f t, EnumTerm s (FDIntTerm s),
      FDSolver s, Term s t, TreeSolver f t ~ FDInstance s, t ~ ModelInt,
      Expr (ModelIntTerm ModelFunctions)
           (ModelColTerm ModelFunctions)
           (ModelBoolTerm ModelFunctions)
                      ~ FDIntTerm s) =>
     ModelCol -> Free (f t) [TermBaseType s (FDIntTerm s)]
labeller col = do
  label $ do
    min <- getMinimizeVar
    case min of
      Nothing -> return $ labelCol col
      Just v -> return $ do
        enumerate [v]
        labelCol col


-- here min in labeller is Nothing (see fdsMinimizeVar in FD.hs)
labellerX
  :: (Show (FDIntTerm s), FunctorTree f t, EnumTerm s (FDIntTerm s),
      FDSolver s, Term s t, TreeSolver f t ~ FDInstance s, t ~ ModelInt,
      Expr (ModelIntTerm ModelFunctions)
           (ModelColTerm ModelFunctions)
           (ModelBoolTerm ModelFunctions)
                      ~ FDIntTerm s) =>
     ModelCol -> Free (f t) [TermBaseType s (FDIntTerm s)]
labellerX col = do
  label $ return $ labelCol col

 
labellerC :: (Show (FDIntTerm s), FunctorTree f t, EnumTerm s (FDIntTerm s),
      FDSolver s, Term s t, TreeSolver f t ~ FDInstance s, t ~ ModelInt,
      Expr (ModelIntTerm ModelFunctions)
           (ModelColTerm ModelFunctions)
           (ModelBoolTerm ModelFunctions)
                      ~ FDIntTerm s, 
                 s ~ FDInstance OvertonFD) =>
      ModelCol -> Free (f t) [TermBaseType s (FDIntTerm s)]
labellerC col = labellerX col 

-----------------------------------------------------------------------------

example_main ::  (EnumTerm (FDInstance OvertonFD) t, s ~ FDInstance OvertonFD, 
     Show (TermBaseType (FDInstance OvertonFD) (FDIntTerm (FDInstance OvertonFD))), t ~ Expr (ModelIntTerm ModelFunctions)
             (ModelColTerm ModelFunctions)
             (ModelBoolTerm ModelFunctions),
    FDIntTerm (FDInstance OvertonFD)
                      ~ Expr
                          (ModelIntTerm ModelFunctions)
                          (ModelColTerm ModelFunctions)
                          (ModelBoolTerm ModelFunctions),
                 FDSolver (FDInstance OvertonFD)   ) => 
     ExampleModel [String] t -> 
     ExampleModel ModelInt t -> 
     ExampleModel ModelCol t -> IO () -- Bool -> IO ()
example_main f fx fcx = do -- typ = do
  args <- getArgs
  case args of
    ("overton_run":r) -> print $ solve $ (f r) >>= labellerC 
           -- ((f r) :: Tree (FDInstance OvertonFD) t ModelCol) >>= 
           -- (labeller :: ModelCol -> Free (f t) 
           -- [TermBaseType (FDInstance OvertonFD) 
           --              (FDIntTerm (FDInstance OvertonFD))])
    [] -> putStr "Solver type required: must be overton_run\n"
    (a:r) -> putStr ("Unsupported solver: " ++ a ++ "\n")



example_main_void :: (EnumTerm (FDInstance OvertonFD) t, s ~ FDInstance OvertonFD, 
     Show (TermBaseType (FDInstance OvertonFD) (FDIntTerm (FDInstance OvertonFD))), t ~ Expr (ModelIntTerm ModelFunctions)
             (ModelColTerm ModelFunctions)
             (ModelBoolTerm ModelFunctions),
    FDIntTerm (FDInstance OvertonFD) -- FDVar  
                      ~ Expr
                          (ModelIntTerm ModelFunctions)
                          (ModelColTerm ModelFunctions)
                          (ModelBoolTerm ModelFunctions),
                 FDSolver (FDInstance OvertonFD)   ) => 
       ExampleModel () t -> IO ()
example_main_void f = example_main (const $ f ()) (const $ f ()) (const $ f ())


---------------------------------------------------------------------------
-- dfs = []
-- bfs = Data.Sequence.empty
--pfs :: Ord a => PriorityQueue.PriorityQueue a (a,b,c)
--pfs = PriorityQueue.empty
{-
nb :: Int -> CNodeBoundedST s a
nb = CNBST
db :: Int -> CDepthBoundedST s a
db = CDBST
-}
----------------------------------------------------------------------------

{-

from Model.hs
------------------------

t: ModelCol

ColExpr (ModelIntVar 0) (ModelColVar 0) (ModelBoolVar 0)



type Model = ModelBool
type ModelInt = ModelIntExpr ModelFunctions
type ModelBool = ModelBoolExpr ModelFunctions
type ModelCol = ModelColExpr ModelFunctions

-- in practice, ModelInt = ModelIntExpr (the dependency is not used)

type ModelIntExpr t       = Expr        (ModelIntTerm  t) (ModelColTerm  t) (ModelBoolTerm  t)
type ModelBoolExpr t      = BoolExpr    (ModelIntTerm  t) (ModelColTerm  t) (ModelBoolTerm  t)
type ModelColExpr t       = ColExpr     (ModelIntTerm  t) (ModelColTerm  t) (ModelBoolTerm  t)

data ModelIntTerm t = 
    ModelIntVar Int
  | ModelIntPar Int
  deriving (Show)

data ModelColTerm t = 
    ModelColVar Int
  | ModelColPar Int
  deriving (Show)

data ModelBoolTerm t = 
    ModelBoolVar Int
  | ModelBoolPar Int
  | ModelExtra t
  deriving (Show)

data ModelFunctions =
    ForNewBool (ModelBoolExpr ModelFunctions -> Model)
           -- Model = ModelBoolExpr ModelFunctions
--  ForNewBool (BoolExpr (ModelIntTerm ModeFunctions)  
--                       (ModelColTerm ModelFunctions)
--                       (ModelBoolTerm ModelFunctions) ->
--              BoolExpr (ModelIntTerm ModeFunctions)  
--                       (ModelColTerm ModelFunctions)
--                       (ModelBoolTerm ModelFunctions))           
-- BoolExpr (from Data.Expr.Data.hs) is the datatype of boolean expressions
-- where the parameters are the types or the variables.
-- notice that 'ModelIntTerm t' does not really depend on t, and so on.
  | ForNewInt (ModelIntExpr ModelFunctions -> Model)
  | ForNewCol (ModelColExpr ModelFunctions -> Model)

---------------------------------------------------------------------------

from Sugar.hs
----------------

instance FDSolver OvertonFD where
  type FDIntTerm OvertonFD = FDVar
  type FDBoolTerm OvertonFD = FDVar

  type FDIntSpec OvertonFD = FDVar
  type FDBoolSpec OvertonFD = FDVar
  type FDColSpec OvertonFD = [FDVar]
  
  type FDIntSpecType OvertonFD = ()
  type FDBoolSpecType OvertonFD = ()
  type FDColSpecType OvertonFD = ()


from FD.hs

instance FDSolver s => Term (FDInstance s) ModelInt where
  newvar = do
    s <- get
    let i = fdsVars s
    put $ s { fdsVars = 1+i }
    return $ Term $ ModelIntVar i
  type Help (FDInstance s) ModelInt = ()
  help _ _ = ()

instance FDSolver s => Term (FDInstance s) ModelBool where
  newvar = do
    s <- get
    let i = fdsVars s
    put $ s { fdsVars = 1+i }
    return $ BoolTerm $ ModelBoolVar i
  type Help (FDInstance s) ModelBool = ()
  help _ _ = ()

instance FDSolver s => Term (FDInstance s) ModelCol where
  newvar = do
    s <- get
    let i = fdsVars s
    put $ s { fdsVars = 1+i }
    return $ ColTerm $ ModelColVar i
  type Help (FDInstance s) ModelCol = ()
  help _ _ = ()

newCol :: FDSolver s => FDInstance s ModelCol
newCol = newvar

newInt :: FDSolver s => FDInstance s ModelInt
newInt = newvar

newBool :: FDSolver s => FDInstance s ModelBool
newBool = newvar

----------------------------------------------------------------

from OvertonFD.hs
----------------------

runOverton :: OvertonFD a -> a
runOverton fd = 
  let j = evalState (unFD fd) initState
      in j


fd_domain :: FDVar -> OvertonFD [Int]
fd_domain v = do d <- lookup v
	         return $ elems d

lookup :: FDVar -> OvertonFD Domain
lookup x = do
    s <- get
    return . domain $ varMap s ! x

-}

---------------------------------------------------------------------------

{-
enumOV :: FDVar -> [Int] -> Free (CP FDVar) Int 
enumOV var values = 

enumerateOV :: [FDVar] -> [Int] -> Free (CP FDVar) [Int]
enumerateOV vars values = 
-}

{-
labelCol :: (FDSolver s, Term s t, FunctorTree f t, t ~ ModelInt, TreeSolver f t ~ FDInstance s, EnumTerm s (FDIntTerm s),
  FDIntTerm s ~ Expr (Model.ModelIntTerm Model.ModelFunctions)
                     (Model.ModelColTerm Model.ModelFunctions)
                     (Model.ModelBoolTerm Model.ModelFunctions)) => ModelCol -> Free (f t) [TermBaseType s (FDIntTerm s)]
labelCol col = label $ do
  lst <- getColItems col maxBound
  return $ do
    lsti <- colList col $ length lst
--    enumerate lsti
    labelling firstFail
    assignments lsti
-}

{-
solveC :: (EnumTerm (FDInstance OvertonFD) t
Solver solver, Term solver t) 
      => Tree solver t a -> [a]
solve model = run $ eval model
-}
              
------------------------------------------------------------------------

{-
example_main :: 
 Term OvertonFD t => ExampleModel [String] -> ExampleModel ModelInt -> ExampleModel ModelCol -> Bool -> IO ()
example_main f fx fcx typ = do
  args <- getArgs
  case args of
    ("overton_run":r) -> print $ runSolve typ $ 
--((f r):: Tree (FDInstance OvertonFD) t ModelCol)
       ((f r) :: Tree (FDInstance OvertonFD) t ModelCol) >>= labeller
    [] -> putStr "Solver type required: must be overton_run\n"
    (a:r) -> putStr ("Unsupported solver: " ++ a ++ "\n")


example_sat_main :: ExampleModel [String] -> ExampleModel ModelInt -> ExampleModel ModelCol -> IO ()
example_sat_main f fx fcx = example_main f fx fcx False
-}
{-
example_sat_main_void :: ExampleModel () -> IO ()
example_sat_main_void f = example_sat_main (const $ f ()) (const $ f ()) (const $ f ())

example_sat_main_single :: Read n => ExampleModel n -> IO ()
example_sat_main_single f = example_sat_main (f . read . head) (error "Uncompilable model") (error "Uncompilable model")

example_sat_main_single_expr :: ExampleModel ModelInt -> IO ()
example_sat_main_single_expr f = example_sat_main (f . fromInteger . read . head) f (\x -> f $ x!(cte 0))

example_sat_main_coll_expr :: ExampleModel ModelCol -> IO ()
example_sat_main_coll_expr f = example_sat_main (f . list . foldr (++) [] . map (map fromInteger . read . (\x -> "[" ++ x ++ "]"))) (f. list . (\x -> [x])) f

-}

{-
example_min_main :: ExampleMinModel [String] -> ExampleMinModel ModelInt -> ExampleMinModel ModelCol -> IO ()
example_min_main f fx fcx = example_main (postMinimize f) (postMinimize fx) (postMinimize fcx) True
-}
{-
example_min_main_void :: ExampleMinModel () -> IO ()
example_min_main_void f = example_min_main (const $ f ()) (const $ f ()) (const $ f ())
-}
{-
example_min_main_single :: Read n => ExampleMinModel n -> IO ()
example_min_main_single f = example_min_main (f . read . head) (error "Uncompilable model") (error "Uncompilable model")
-}
{-
example_min_main_single_expr :: ExampleMinModel ModelInt -> IO ()
example_min_main_single_expr f = example_min_main (f . fromInteger . read . head) f (\x -> f $ x!(cte 0))
-}
{-
example_min_main_coll_expr :: ExampleMinModel ModelCol -> IO ()
example_min_main_coll_expr f = example_min_main (f . list . foldr (++) [] . map (map fromInteger . read . (\x -> "[" ++ x ++ "]"))) (f. list . (\x -> [x])) f
-}
