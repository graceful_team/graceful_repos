{-
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Tom Schrijvers, Paul Torrini
 -      refactored from monadiccp
 -}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Control.CP.FD.Interface (
  FDSolver,
  FDInstance,
  ModelExprClass,
  (@+),(@-),(@*),(@/),(@%),(!),(@!!),(@..),(@++),size,xfold,xsum,xhead,xtail,list,slice,xmap,cte,
  (Control.CP.FD.Interface.@||),
  (Control.CP.FD.Interface.@&&),
  Control.CP.FD.Interface.inv,
  (Control.CP.FD.Interface.@=),
  (Control.CP.FD.Interface.@/=),
  (Control.CP.FD.Interface.@<),
  (Control.CP.FD.Interface.@>),
  (Control.CP.FD.Interface.@<=),
  (Control.CP.FD.Interface.@>=),
  (Control.CP.FD.Interface.@:),
  (Control.CP.FD.Interface.@?),
  (Control.CP.FD.Interface.@??),
  Control.CP.FD.Interface.channel,
  val,
--  Control.CP.FD.Interface.newInt, Control.CP.FD.Interface.newBool, Control.CP.FD.Interface.newCol,
  Control.CP.FD.Interface.sorted, 
  Control.CP.FD.Interface.sSorted,
  Control.CP.FD.Interface.forall,
  Control.CP.FD.Interface.forany,
  Control.CP.FD.Interface.loopall,
  Control.CP.FD.Interface.allDiff,
  Control.CP.FD.Interface.allDiffD,
  Control.CP.FD.Interface.loopany,
  allin,
  asExpr, asCol, Control.CP.FD.Interface.asBool,
  colList, labelCol, 
  ModelInt, ModelCol, ModelBool,
  exists, true, false,
  assignments
--  Modelable,
) where

import Control.CP.FD.FD (FDSolver, FDInstance, FDIntTerm, getColItems)
import qualified Control.CP.FD.Model as Model
import Control.CP.FD.Model (Model, ModelBool, ModelCol, ModelInt, ToModelBool, asBool, asExpr, asCol, cte, newModelTerm, ModelIntArg, ModelBoolArg, ModelColArg)
import qualified Data.Expr.Sugar as Sugar
import Data.Expr.Util
import Data.Expr.Data
import Data.Expr.Sugar ((@+),(@-),(@*),(@/),(@%),(!),(@!!),(@..),(@++),size,xfold,xhead,xtail,slice,xmap,xsum,list)
import Control.CP.Solver
import Control.CP.FMSearchTree
import Control.CP.FMFunctorTree
import Control.CP.EnumTerm

import Control.Monad
import Control.Applicative

newtype DummySolver a = DummySolver ()

instance Monad DummySolver where
  return _ = DummySolver ()
  _ >>= _ = DummySolver ()

instance Functor DummySolver where
  fmap = liftM

instance Applicative DummySolver where
  pure _ = DummySolver ()
  (<*>)  = ap
  _ *> _   = DummySolver () 


data EQHelp b where
  EQHelp :: Model.ModelTermType b => ((b -> Model) -> Model) -> EQHelp b

instance Model.ModelTermType t => Term DummySolver t where
  type Help DummySolver t = EQHelp t
  help _ _ = EQHelp newModelTerm
  newvar = DummySolver ()

instance Solver DummySolver where
  type Constraint DummySolver = Either Model ()
  type Label DummySolver = ()
  add _ = DummySolver ()
  run _ = error "Attempt to run dummy solver"
  mark = DummySolver ()
  goto _ = DummySolver ()

data DummyTerm t = Model.ModelTermType t => DummyTerm t

-- class (Solver s, Term s ModelBool, Term s ModelInt, Term s ModelCol) => Modelable s where

-- instance Modelable DummySolver where

-- instance FDSolver s => Modelable (FDInstance s) where


treeToModel :: Term DummySolver t => Tree DummySolver t () -> Model
treeToModel (Return _) = BoolConst True
treeToModel (Op (Try a b)) = (Sugar.@||) (treeToModel a) (treeToModel b)
treeToModel (Op (Add (Left c) m)) = (Sugar.@&&) c (treeToModel m)
treeToModel (Op Fail) = BoolConst False
treeToModel (Op (Label _)) = error "Cannot turn labelled trees into expressions"
treeToModel (Op (NewVar (f :: t -> Tree DummySolver t ()))) = case (help ((error "treeToModel undefined 1") :: DummySolver ()) ((error "treeToModel undefined 2") :: t)) of EQHelp ff -> ff (\x -> treeToModel $ f (x :: t))

addM :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => Model -> Free (f t) ()
addM m = addC $ Left m

infixr 2 @||
(@||) :: (Constraint s ~ Either Model q, Term s t, Model.ModelTermType t, FunctorTree f t, TreeSolver f t  ~ s) => Tree DummySolver t () -> Tree DummySolver t () -> Free (f t) ()
(@||) a b = addM $ treeToModel $ a \/ b

infixr 3 @&&
(@&&) :: (Constraint s ~ Either Model q, Term s t, Model.ModelTermType t, FunctorTree f t, TreeSolver f t ~ s) => Tree DummySolver t () -> Tree DummySolver t () -> Free (f t) ()
(@&&) a b = addM $ treeToModel $ a /\ b

channel :: Model.ModelTermType t => Tree DummySolver t () -> ModelInt
channel a = Sugar.channel $ treeToModel a

inv :: (Constraint s ~ Either Model q, Term s t, Model.ModelTermType t, FunctorTree f t, TreeSolver f t ~ s) => Tree DummySolver t () -> Free (f t) ()
inv a = addM $ Sugar.inv $ treeToModel a

infix 4 @=, @/=, @<, @>, @<=, @>=

class ModelExprClass a where
  (@=) :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => a -> a -> Free (f t) ()
  (@/=) :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => a -> a -> Free (f t) ()

instance ModelExprClass ModelInt where
  a @= b  = addM $ (Sugar.@=)  a b
  a @/= b = addM $ (Sugar.@/=) a b

instance ModelExprClass ModelCol where
  a @= b  = addM $ (Sugar.@=)  a b
  a @/= b = addM $ (Sugar.@/=) a b

instance ModelExprClass ModelBool where
  a @= b  = addM $ (Sugar.@=)  a b
  a @/= b = addM $ (Sugar.@/=) a b

instance Model.ModelTermType t => ModelExprClass (Tree DummySolver t ()) where
  a @= b  = addM $ (Sugar.@=)  (treeToModel a) (treeToModel b)
  a @/= b = addM $ (Sugar.@/=) (treeToModel a) (treeToModel b)

(@<) :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelInt -> ModelInt -> Free (f t) ()
(@<) a b = addM $ (Sugar.@<) a b

(@>) :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelInt -> ModelInt -> Free (f t) ()
(@>) a b = addM $ (Sugar.@>) a b

(@>=) :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelInt -> ModelInt -> Free (f t) ()
(@>=) a b = addM $ (Sugar.@>=) a b

(@<=) :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelInt -> ModelInt -> Free (f t) ()
(@<=) a b = addM $ (Sugar.@<=) a b

val :: Model.ModelTermType t => Tree DummySolver t () -> ModelInt
val = Sugar.toExpr . treeToModel

{- newBool :: (Constraint s ~ Either Model q, MonadTree m, TreeSolver m ~ s) => (ModelBool -> Tree DummySolver a) -> m a
newBool = exists

newInt :: (Constraint s ~ Either Model q, MonadTree m, TreeSolver m ~ s) => (ModelInt -> m a) -> m a
newInt = exists

newCol :: (Constraint s ~ Either Model q, MonadTree m, TreeSolver m ~ s) => (ModelCol -> m a) -> m a
newCol = exists
-}

asBool :: (FDSolver s, Term s t, FunctorTree f t, TreeSolver f t ~ FDInstance s, ToModelBool u) => u -> Free (f t) ()
asBool = addM . Control.CP.FD.Model.asBool

sorted :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelCol -> Free (f t) ()
sorted = addM . Sugar.sorted

sSorted :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelCol -> Free (f t) ()
sSorted = addM . Sugar.sSorted

allDiff :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelCol -> Free (f t) ()
allDiff = addM . Sugar.allDiff

allDiffD :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelCol -> Free (f t) ()
allDiffD = addM . Sugar.allDiffD

mm (nv@(Term tv)) m x = 
     let tf t = if (t==tv) then x else Term t
         tb t = if (Term t==x) then nv else Term t
         in boolTransformEx (tf,ColTerm,BoolTerm,tb,ColTerm,BoolTerm) m

forall :: (Term s ModelInt, Term s ModelBool, Term s ModelCol, Term s t, Model.ModelTermType t, Constraint s ~ Either Model q, FunctorTree f t, TreeSolver f t ~ s) => ModelCol -> (ModelInt -> Tree DummySolver t ()) -> Free (f t) ()
-- forall col f = exists $ \nv -> addM $ Sugar.forall col $ mm nv $ treeToModel $ f nv
forall col f = addM $ Sugar.forall col (treeToModel . f)

forany :: (Term s ModelInt, Term s ModelBool, Term s ModelCol, Model.ModelTermType t, Term s t, Constraint s ~ Either Model q, FunctorTree f t, TreeSolver f t ~ s) => ModelCol -> (ModelInt -> Tree DummySolver t ()) -> Free (f t) ()
-- forany col f = exists $ \nv -> addM $ Sugar.forany col $ mm nv $ treeToModel $ f nv
forany col f = addM $ Sugar.forany col (treeToModel . f)

loopall :: (Term s ModelInt, Term s ModelBool, Term s ModelCol, Term s t, Model.ModelTermType t, Constraint s ~ Either Model q, FunctorTree f t, TreeSolver f t ~ s) => (ModelInt,ModelInt) -> (ModelInt -> Tree DummySolver t ()) -> Free (f t) ()
-- loopall r f = exists $ \nv -> addM $ Sugar.loopall r $ mm nv $ treeToModel $ f nv
loopall r f = addM $ Sugar.loopall r (treeToModel . f)

loopany :: (Term s ModelInt, Term s ModelBool, Term s ModelCol, Term s t, Model.ModelTermType t, Constraint s ~ Either Model q, FunctorTree f t, TreeSolver f t ~ s) => (ModelInt,ModelInt) -> (ModelInt -> Tree DummySolver t ()) -> Free (f t) ()
-- loopany r f = exists $ \nv -> addM $ Sugar.loopany r $ mm nv $ treeToModel $ f nv
loopany r f = addM $ Sugar.loopany r (treeToModel . f)

colList :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s) => ModelCol -> Int -> Free (f t) [ModelInt]
colList col len = do
  addM $ (Sugar.@=) (size col) (asExpr len)
  return $ map (\i -> col!cte i) [0..len-1]

labelCol :: (FDSolver s, Term s t, FunctorTree f t, t ~ ModelInt, TreeSolver f t ~ FDInstance s, EnumTerm s (FDIntTerm s),
  FDIntTerm s ~ Expr (Model.ModelIntTerm Model.ModelFunctions)
                     (Model.ModelColTerm Model.ModelFunctions)
                     (Model.ModelBoolTerm Model.ModelFunctions)) => ModelCol -> Free (f t) [TermBaseType s (FDIntTerm s)]
labelCol col = label $ do
  lst <- getColItems col maxBound -- :: FDInstance s [FDIntTerm s]
  return $ do
    lsti <- colList col $ length lst -- :: Free (f t) [ModelInt]
--    enumerate lsti
    labelling firstFail lsti -- :: Free (f t) ()
    assignments lsti -- :: Free (f t) (TermBaseType s t)

{- from FD.hs

getColTerm :: FDSolver s => [ModelCol] -> FDColSpecType s -> FDInstance s [FDColSpec s]
getColTerm m tp = do
  s <- get
  put $ s { fdsForceCol = m++(fdsForceCol s) }
  commit
  s2 <- get
  let ids = map (\x -> decompColLookup (fdsDecomp s2) x) m
  specs <- mapM (\(Just id) -> getColSpec_ id (Set.singleton tp)) ids
  return $ map (snd . myFromJust ("getColTerm(tp="++(show tp)++")")) specs

getColItems :: FDSolver s => ModelCol -> FDColSpecType s -> FDInstance s [FDIntTerm s]
getColItems c tp = do
  [cc] <- getColTerm [c] tp
--  lst <- liftFD $ fdColInspect cc
  lst <- liftFD $ return cc
  return lst

getColTerm :: FDSolver s => [ModelCol] -> FDColSpecType s -> FDInstance s [FDColSpec s]
getColTerm m tp = do
  s <- get
  put $ s { fdsForceCol = m++(fdsForceCol s) }
  commit
  s2 <- get
  let ids = map (\x -> decompColLookup (fdsDecomp s2) x) m
  specs <- mapM (\(Just id) -> getColSpec_ id (Set.singleton tp)) ids
  return $ map (snd . myFromJust ("getColTerm(tp="++(show tp)++")")) specs



-}

{- from EnumTerm.hs

labelling :: (FunctorTree f t, TreeSolver f t ~ s, EnumTerm s t) => 
         ([t] -> s [t]) -> [t] -> Free (f t) ()
labelling _ [] = true
labelling o l = label $ do 
  ll <- o l
  (cl,c) <- splitDomains ll
  let ml = map (\l -> foldr (/\) true $ map addC l) cl
  return $ do
    levelList ml
    labelling return c


assignment :: (EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  t -> Free (f t) (TermBaseType s t)
assignment q = label $ getValue q >>= \y -> (case y of Just x -> return $ return x; _ -> return false)

assignments :: (EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  [t] -> Free (f t) [TermBaseType s t]
assignments = mapM assignment

firstFail :: EnumTerm s t => [t] -> s [t]
firstFail qs = do ds <- mapM getDomainSize qs 
                  return [ q | (d,q) <- zip ds qs 
                             , then sortWith by d ]

inOrder :: EnumTerm s t => [t] -> s [t]
inOrder = return

-}

infix 5 @:

(@:) :: (Constraint s ~ Either Model q, Term s t, FunctorTree f t, TreeSolver f t ~ s, Sugar.ExprRange ModelIntArg ModelColArg ModelBoolArg r, Term s ModelInt, Term s ModelBool, Term s ModelCol) => ModelInt -> r -> Free (f t) ()
a @: b = addM $ (Sugar.@:) a b

infix 4 @?
infix 4 @??

a @? (t,f) = (Sugar.@?) (treeToModel a) (t,f)
a @?? (t,f) = addM $ (Sugar.@??) (treeToModel a) (treeToModel t, treeToModel f)

allin :: (Constraint s ~ Either Model q, Term s t, Model.ModelTermType t, FunctorTree f t, TreeSolver f t ~ s, Sugar.ExprRange ModelIntArg ModelColArg ModelBoolArg r, Term s ModelInt, Term s ModelBool, Term s ModelCol) => ModelCol -> r -> Free (f t) ()
allin c b = Control.CP.FD.Interface.forall c $ \x -> addM $ (Sugar.@:) x b
