{- 
 - Origin:
 - 	Constraint Programming in Haskell 
 - 	http://overtond.blogspot.com/2008/07/pre.html
 - 	author: David Overton, Melbourne Australia
 -
 - Modifications 1:
 - 	Monadic Constraint Programming
 - 	http://www.cs.kuleuven.be/~toms/Haskell/
 - 	Tom Schrijvers
 - 
 - Modifications 2:
 - 	Modular Constraint Programming
 - 	https://bitbucket.org/graceful_team/graceful_repos
 - 	Tom Schrijvers, Paul Torrini
 -
-} 
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Control.CP.FD.OvertonFD.OvertonFD (
  OvertonFD(..),
  fd_objective,
  fd_domain,
  FDVar(..),
  OConstraint(..),
  VarSupply,
  lookup,
  whenwhen,
  lookupAndShow,
  rmFromDomain,
  (.<.),
  (.<=.),
  different,
  same,
  inDomain,
  hasValue,
  hasNotValue
) where

import Prelude hiding (lookup)
import Data.Maybe (fromJust,isJust)

import Control.Monad
import Control.Monad.State.Class 
import Control.Monad.Trans.State.Lazy hiding (put, gets, get, modify)
import Control.Monad.Trans.RWS hiding (put, gets, get, modify)

import Control.Monad.Trans.Writer
import qualified Data.Map as Map
import Data.Map ((!), Map)
import qualified Data.IntSet as IntSet
import Control.Monad (liftM,(<=<))

import Control.CP.FD.OvertonFD.Domain as Domain
import Control.CP.FD.FD hiding ((!))
import Control.CP.Solver
import Control.CP.FMSearchTree
import Control.CP.EnumTerm

import Control.CP.Debug

import qualified Data.Sequence as S

-- import Debug.Trace

traceStack :: String -> a -> a
traceStack s x = x

--------------------------------------------------------------------------------
-- Solver instance -------------------------------------------------------------
--------------------------------------------------------------------------------

instance (Eq a1, Eq a2) => Eq (a1 -> a2) where
  (==) x y = False
  (/=) x y = True

instance (Show a1, Show a2)  => Show (a1 -> a2) where
  show x = show "Function"


data OConstraint =
    OHasValue FDVar Int
  | OHasNotValue FDVar Int
  | OInDomain FDVar Domain 
  | OIsLess FDVar Int
  | OIsGreater FDVar Int 
  | OSame FDVar FDVar
  | ODiff FDVar FDVar
  | OLess FDVar FDVar
  | OLessEq FDVar FDVar
--
-- arith. constraints
  | OAdd FDVar FDVar FDVar
  | OSub FDVar FDVar FDVar
  | OMult FDVar FDVar FDVar
  | OAbs FDVar FDVar
  | OFunc (Int -> Int) Domain FDVar FDVar 
  deriving (Show,Eq)

instance Solver OvertonFD where
  type Constraint OvertonFD  = OConstraint
  type Label      OvertonFD  = FDState
  add c  = debug ("addOverton("++(show c)++")") $ addOverton c
  run p  = debug ("runOverton") $ runOverton p
  mark   = get
  goto   = put 

instance Term OvertonFD FDVar where
  newvar                    = newVar ()
  type Help OvertonFD FDVar = ()
  help _ _                  = ()

instance EnumTerm OvertonFD FDVar where
  type TermBaseType OvertonFD FDVar = Int
  getDomain        = fd_domain
  setValue var val = return [var `OHasValue` val]

--------------------------------------------------------------------------------
-- Constraints -----------------------------------------------------------------
--------------------------------------------------------------------------------

addOverton c  =
  do b <- traceStack (show c) $ go c
     if b then do r <- runQueue; traceStack ("addOverton: " ++ show r) $ return r
          else traceStack ("addOverton: " ++ show False) $ return b
  where
    go (OHasValue v i) = v `hasValue` i
    go (OHasNotValue v i) = v `hasNotValue` i
    go (OInDomain v d) = inDomain v d
    go (OIsLess v i) = v `isLess` i
    go (OIsGreater v i) = v `isGreater` i
    go (OSame a b) = a `same` b
    go (ODiff a b) = a `different` b
    go (OLess a b) = a .<. b
    go (OLessEq a b) = a .<=. b
--
    go (OAdd a b c) = addSum a b c
    go (OSub a b c) = addSub a b c
    go (OMult a b c) = addMult a b c
    go (OAbs a b) = addAbs a b
    go (OFunc f d a b) = addFunc f d a b


fd_domain :: FDVar -> OvertonFD [Int]
fd_domain v = do d <- lookup v
                 return $ elems d

fd_objective :: OvertonFD FDVar
fd_objective =
  do s <- get
     return $ objective s

--------------------------------------------------------------------------------

-- The FD monad
newtype OvertonFD a = OvertonFD { unFD :: State FDState a }
    deriving (Monad, Functor, Applicative, MonadState FDState)

-- FD variables
newtype FDVar = FDVar { unFDVar :: Int } deriving (Ord, Eq, Show)

type VarSupply = FDVar

data VarInfo = VarInfo
     { delayedConstraints :: [OvertonFD Bool], domain :: Domain }

instance Show VarInfo where
  show x = show $ domain x


type VarMap = Map FDVar VarInfo


data FDState = FDState
     { varSupply  :: VarSupply
     , varMap     :: VarMap 
     , objective  :: FDVar 
     , queue      :: S.Seq (OvertonFD Bool)
     }

instance Eq FDState where
  s1 == s2 = f s1 == f s2
           where f s = head $ elems $ domain $ varMap s ! (objective s) 

instance Ord FDState where
  compare s1 s2  = compare (f s1) (f s2)
           where f s = head $ elems $  domain $ varMap s ! (objective s) 

-- Propagator queue

runQueue :: OvertonFD Bool
runQueue =
  do  q <- gets queue
      case S.viewl q of
        S.EmptyL   ->  
          return True
        p S.:< q'  ->
          do  modify (\s -> s { queue = q' })
              b <- p
              if b 
                then runQueue
                else return False

enqueue :: [OvertonFD Bool] -> OvertonFD ()
enqueue p =
  traceStack ("enqueue " ++ show (length p)) $ modify (\s -> s { queue = queue s S.>< S.fromList p}) 

  -- TOM: inconsistency is not observable within the OvertonFD monad
consistentFD :: OvertonFD Bool
consistentFD = return True


-- Run the FD monad and produce a lazy list of possible solutions.
runOverton :: OvertonFD a -> a
runOverton fd = 
  let j = evalState (unFD fd) initState
      in j


initState :: FDState
initState = 
  FDState           
    { varSupply  = FDVar 0                
    , varMap     = Map.empty        
    , objective  = FDVar 0 
    , queue      = S.empty
    }

-- Get a new FDVar
newVar :: ToDomain a => a -> OvertonFD FDVar
newVar d = do
    s <- get
    let v = varSupply s
    put $ s { varSupply = FDVar (unFDVar v + 1) }
    modify $ \s ->
        let vm = varMap s
            vi = VarInfo {
                delayedConstraints = [],
                domain = toDomain d}
        in
        s { varMap = Map.insert v vi vm }
    return v

newVars :: ToDomain a => Int -> a -> OvertonFD [FDVar]
newVars n d = replicateM n (newVar d)

-------------------------------------------------------------------

-- Lookup the current domain of a variable.
lookup :: FDVar -> OvertonFD Domain
lookup x = do
    s <- get
    return . domain $ varMap s ! x

-- Safely lookup the current domain of a variable.
lookupE :: FDVar -> OvertonFD String
lookupE x = do
    s <- get
    return (case (Map.lookup x (varMap s)) of 
                   Just y  -> show $ domain y
                   Nothing -> "empty")  

-- Lookup and show the current domain of a variable.
lookupAndShow :: FDVar -> String
lookupAndShow x = show $ runOverton $ lookupE x


-- Update the domain of a variable and fire all delayed constraints
-- associated with that variable.
update :: FDVar -> Domain -> OvertonFD ()
update x i = do
    s <- get
    let vm = varMap s
    let vi = vm ! x
    traceStack ("domain " ++ show x ++ " : " ++ show (domain vi) ++ " -> " ++ show i) $
      put $ s { varMap = Map.insert x (vi { domain = i}) vm }
    enqueue (delayedConstraints vi)


-- Add a new constraint for a variable to the constraint store.
addConstraint :: FDVar -> OvertonFD Bool -> OvertonFD ()
addConstraint x constraint = do
    s <- traceStack "addConstraint" get
    let vm = varMap s
    let vi = vm ! x
    let cs = delayedConstraints vi
    put $ s { varMap =
        Map.insert x (vi { delayedConstraints = constraint : cs }) vm }

 
-- Useful helper function for adding binary constraints between FDVars.
type BinaryConstraint = FDVar -> FDVar -> OvertonFD Bool

addBinaryConstraint :: BinaryConstraint -> BinaryConstraint 
addBinaryConstraint f x y = do
    let constraint  = f x y
    b <- constraint 
    when b $ (do addConstraint x constraint
                 addConstraint y constraint)
    return b


---------------------------------------------------------------------------

-- Constrain a variable to a particular value.
hasValue :: FDVar -> Int -> OvertonFD Bool
var `hasValue` val = do
    vals <- lookup var
    if val `member` vals
       then do let i = singleton val
               when (i /= vals) (update var i)
               return True
       else return False


-- remove an element from a domain
rmFromDomain :: Int -> Domain -> Domain
rmFromDomain n (Set d) = Set (IntSet.delete n d)
rmFromDomain n (Range i j) = Set (IntSet.delete n (IntSet.fromList [i..j]))

  
-- Constrain a variable to exclude a particular value.
hasNotValue :: FDVar -> Int -> OvertonFD Bool
var `hasNotValue` val = 
  prop 
  where prop =
          do vals <- lookup var                 
             let vals' = rmFromDomain val vals
             if Domain.null vals'
                then return False
                else do update var vals'
                        return True

-- Constrain a variable to range over a particular domain.
inDomain :: FDVar -> Domain -> OvertonFD Bool
inDomain var dom = do
    vals <- lookup var
    let i = vals `intersection` dom
    if not $ Domain.null i
       then do when (i /= vals) (update var i) 
               return True 
       else return False


-- Constrain a variable to be less than a particular value.
isLess :: FDVar -> Int -> OvertonFD Bool
var `isLess` val = do
    vals <- lookup var
    let i = filterLessThan val vals 
    if not $ Domain.null i
    then do when (i /= vals) (update var i)
            return True
    else return False


-- Constrain a variable to be greater than a particular value.
isGreater :: FDVar -> Int -> OvertonFD Bool
var `isGreater` val = do
    vals <- lookup var
    let i = filterGreaterThan val vals 
    if not $ Domain.null i
    then do when (i /= vals) (update var i)
            return True
    else return False


-- Constrain two variables to have the same value.
same :: FDVar -> FDVar -> OvertonFD Bool
same = addBinaryConstraint $ \x y -> do 
    debug "inside same" $ return ()
    xv <- lookup x
    yv <- lookup y
    debug (show xv ++ " same " ++ show yv) $ return ()
    let i = xv `intersection` yv
    if not $ Domain.null i
       then do whenwhen (i /= xv)  (i /= yv) (update x i) (update y i)
               return True
       else return False


whenwhen :: Monad m => Bool -> Bool -> m () -> m () -> m()
whenwhen c1 c2 a1 a2  =
  do when c1 a1 
     when c2 a2

-- Constrain two variables to have different values.
different :: FDVar  -> FDVar  -> OvertonFD Bool
different = addBinaryConstraint $ \x y -> do
    xv <- traceStack "different" $ 
            lookup x
    yv <- lookup y
    if traceStack ("different: " ++ show xv ++ " <-> " ++ show yv) $ not (isSingleton xv) || not (isSingleton yv) || xv /= yv
       then do whenwhen (isSingleton xv && xv `isSubsetOf` yv)
                        (isSingleton yv && yv `isSubsetOf` xv)
                        (update y (yv `difference` xv))
                        (update x (xv `difference` yv))
               return$ traceStack "True"  True
       else return $ traceStack "False" False



-- Constrain one variable to have a value less than the value of another
-- variable.
infix 4 .<.
(.<.) :: FDVar -> FDVar -> OvertonFD Bool
(.<.) = addBinaryConstraint $ \x y -> do
    xv <- lookup x
    yv <- lookup y
    let xv' = filterLessThan (findMax yv) xv
    let yv' = filterGreaterThan (findMin xv) yv
    if  not $ Domain.null xv'
        then if not $ Domain.null yv'
                then do whenwhen (xv /= xv') (yv /= yv') 
                            (update x xv') (update y yv')
                        return True
                else return False
        else return False

-- Constrain one variable to have a value less than or equal to the value of another 
-- variable.
infix 4 .<=.
(.<=.) :: FDVar -> FDVar -> OvertonFD Bool
(.<=.) = addBinaryConstraint $ \x y -> do
    xv <- lookup x
    yv <- lookup y
    let xv' = filterLessThan (1 + findMax yv) xv
    let yv' = filterGreaterThan ((findMin xv) - 1) yv
    if  not $ Domain.null xv'
        then if not $ Domain.null yv'
                then do whenwhen (xv /= xv') (yv /= yv') 
                             (update x xv') (update y yv')
                        return True
                else return False
        else return False


dump :: [FDVar] -> OvertonFD [Domain]
dump = mapM lookup

{-
-- Get all solutions for a constraint without actually updating the
-- constraint store.
solutions :: OvertonFD s a -> OvertonFD s [a]
solutions constraint = do
    s <- get
    return $ evalStateT (unFD constraint) s

-- Label variables using a depth-first left-to-right search.
labelling :: [FDVar s] -> OvertonFD s [Int]
labelling = mapM label where
    label var = do
        vals <- lookup var
        val <- OvertonFD . lift $ elems vals
        var `hasValue` val
        return val
-}

------------------ ARITHMETIC CONSTRAINT, ORIGINAL IMPLEMENTATION 

-- Add constraint (z = x `op` y) for var z
addArithmeticConstraint :: 
    (Domain -> Domain -> Domain) ->
    (Domain -> Domain -> Domain) ->
    (Domain -> Domain -> Domain) ->
    FDVar -> FDVar -> FDVar -> OvertonFD Bool
addArithmeticConstraint getZDomain getXDomain getYDomain x y z = do
    let zConstraint = constraint z x y getZDomain
        xConstraint = constraint x z y getXDomain
        yConstraint = constraint y z x getYDomain
    b1 <- xConstraint
    b2 <- yConstraint
    b3 <- zConstraint
    let b = b1 && b2 && b3
    if b then
      do addConstraint z xConstraint
         addConstraint z yConstraint
         addConstraint x zConstraint
         addConstraint x yConstraint
         addConstraint y zConstraint
         addConstraint y xConstraint
         return True
    else return b
  where constraint z x y getDomain = do
          xv <- lookup x
          yv <- lookup y
          zv <- lookup z
          let znew = zv `intersection` getDomain xv yv
          if not $ Domain.null znew
             then do when (znew /= zv) (update z znew)
                     return True
             else return False

-- Add constraint (z = op x) for var z
addUnaryArithmeticConstraint :: (Domain -> Domain) -> (Domain -> Domain) -> FDVar -> FDVar -> OvertonFD Bool
addUnaryArithmeticConstraint getZDomain getXDomain x z = do
    xv <- lookup x
    let constraint z x getDomain = do
          xv <- lookup x
          zv <- lookup z
          let znew = zv `intersection` (getDomain xv)
          debug ("unaryArith:" ++ show z ++ " before: "  ++ show zv ++ show "; after: " ++ show znew) (return ())
          if not $ Domain.null znew
             then do when (znew /= zv) (update z znew)
                     return True
             else return False
    let zConstraint = constraint z x getZDomain
        xConstraint = constraint x z getXDomain
    addConstraint z xConstraint
    addConstraint x zConstraint
    return True

{-
-- with inverted function arguments and different variable names
addUnaryArithmeticConstraint :: 
  (Domain -> Domain) -> (Domain -> Domain) -> FDVar -> FDVar -> OvertonFD Bool
addUnaryArithmeticConstraint getXDomain getZDomain x z = do
--    xv <- lookup x
    let constraint v1 v2 getD = do
        dv1 <- lookup v1       
        dv2 <- lookup v2
        let dnew = dv2 `intersection` (getD dv1)
        if not $ Domain.null dnew
           then do when (dnew /= dv2) (update v2 dnew)
                   return True
           else return False
    let zConstraint = constraint z x getXDomain 
                             -- changes x dom using (inverseAbsDom z)
        xConstraint = constraint x z getZDomain 
                             -- changes z dom using (absDomain x)
    addConstraint z xConstraint 
    addConstraint x zConstraint
    return True
-}


addFunc = \f d -> 
      addUnaryArithmeticConstraint (funDomain f) (revDomain f $ domToList d) 


funDomain :: (Int -> Int) -> Domain -> Domain 
funDomain f d = Set $ IntSet.fromList $ concatMap (\x -> [f x]) $ elems d

revDomain :: (Int -> Int) -> [Int] -> Domain -> Domain 
revDomain f ls d = Set $ IntSet.fromList [ x | x <- ls, member (f x) d ]



addSum = addArithmeticConstraint getDomainPlus getDomainMinus getDomainMinus

addSub = addArithmeticConstraint getDomainMinus getDomainPlus (flip getDomainMinus)


addMult = addArithmeticConstraint getDomainMult getDomainDiv getDomainDiv

addAbs = addUnaryArithmeticConstraint absDomain (\z -> mapDomain z (\i -> [i,-i]))

getDomainPlus :: Domain -> Domain -> Domain
getDomainPlus xs ys = toDomain (zl, zh) where
    zl = findMin xs + findMin ys
    zh = findMax xs + findMax ys

getDomainMinus :: Domain -> Domain -> Domain
getDomainMinus xs ys = toDomain (zl, zh) where
    zl = findMin xs - findMax ys
    zh = findMax xs - findMin ys

getDomainMult :: Domain -> Domain -> Domain
getDomainMult xs ys = (\d -> debug ("multDomain" ++ show d ++ "=" ++ show xs ++ "*" ++ show ys ) d) $ toDomain (zl, zh) where
    zl = minimum products
    zh = maximum products
    products = [x * y |
        x <- [findMin xs, findMax xs],
        y <- [findMin ys, findMax ys]]

getDomainDiv :: Domain -> Domain -> Domain
getDomainDiv xs ys = toDomain (zl, zh) where
    zl = minimum quotientsl
    zh = maximum quotientsh
    quotientsl = [if y /= 0 then x `div` y else minBound |
        x <- [findMin xs, findMax xs],
        y <- [findMin ys, findMax ys]]
    quotientsh = [if y /= 0 then x `div` y else maxBound |
        x <- [findMin xs, findMax xs],
        y <- [findMin ys, findMax ys]]

-----------------------------------------------------------------------------

-------- ARITHMETIC CONSTRAINTS, alternative implementation (not used)


type TernaryConstraint = FDVar -> FDVar -> FDVar -> OvertonFD Bool

addTernaryConstraint :: TernaryConstraint -> TernaryConstraint 
addTernaryConstraint f x y z = do
    let constraint  = f x y z
    b <- constraint 
    when b $ (do addConstraint x constraint
                 addConstraint y constraint
                 addConstraint z constraint)
    return b



addSumX :: FDVar -> FDVar -> FDVar -> OvertonFD Bool
addSumX = addTernaryConstraint $ \x y z -> do
    xv <- lookup x
    yv <- lookup y
    zv <- lookup z
    let min_x = findMin xv
        max_x = findMax xv
        min_y = findMin yv
        max_y = findMax yv 
        min_z = min_x + min_y 
        max_z = max_x + max_y
        zv'   = filterLessThan max_z zv
        zv''  = filterGreaterThan min_z zv'
    if  not $ Domain.null zv'' 
        then if zv /= zv''
             then do update z zv'' >> addSub z y x >> addSub z x y
             else return True
        else return False
            

addSubX :: FDVar -> FDVar -> FDVar -> OvertonFD Bool
addSubX = addTernaryConstraint $ \x y z -> do
    xv <- lookup x
    yv <- lookup y
    zv <- lookup z
    let min_x = findMin xv
        max_x = findMax xv
        min_y = findMin yv
        max_y = findMax yv 
        min_z = min_x - max_y 
        max_z = max_x + min_y
        zv'   = filterLessThan max_z zv
        zv''  = filterGreaterThan min_z zv'
    if  not $ Domain.null zv'' 
        then if zv /= zv''
             then do update z zv'' >> addSum z y x >> addSub x z y 
             else return True
        else return False
