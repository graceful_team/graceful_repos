{- 
 - Origin:
 - 	Constraint Programming in Haskell 
 - 	http://overtond.blogspot.com/2008/07/pre.html
 - 	author: David Overton, Melbourne Australia
 -
 - Modifications:
 - 	Monadic Constraint Programming
 - 	http://www.cs.kuleuven.be/~toms/Haskell/
 - 	Tom Schrijvers
 -
 - Modifications 2:
 - 	Modular Constraint Programming
 - 	https://bitbucket.org/graceful_team/graceful_repos
 - 	Tom Schrijvers, Paul Torrini

Not up-to-date wrt OvertonFD
 
 -} 

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Control.CP.FD.OvertonFD.OvertonWT (
  OvertonWR(..),
  OvertonWT(..),
  fd_objectiveW,
  fd_domainWT,
  fd_domainWR,
  OConstraintW(..),
  lookupW,
  lookupAndShowR,
  evalWriterW
) where

import Prelude hiding (lookup)
import Data.Maybe (fromJust,isJust)

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.State.Class 
import Control.Monad.Trans.State.Lazy hiding (put, gets, get, modify)
import Control.Monad.Trans.RWS hiding (put, gets, get, modify)

-- import Control.Monad.Trans.State.Lazy
-- import Control.Monad.Trans.RWS
-- import Control.Monad (liftM,(<=<))
import Control.Monad.Trans.Writer
import qualified Data.Map as Map
import Data.Map ((!), Map)
import qualified Data.IntSet as IntSet

import Control.CP.FD.OvertonFD.Domain as Domain
import Control.CP.FD.FD hiding ((!))
import Control.CP.Solver
import Control.CP.FMSearchTree
import Control.CP.EnumTerm

import Control.CP.FD.OvertonFD.OvertonFD

import Control.CP.Debug

--------------------------------------------------------------------------------
-- Solver instance -------------------------------------------------------------
--------------------------------------------------------------------------------

data OConstraintW =
    OHasValueW FDVar Int
  | OHasNotValueW FDVar Int
  | OInDomainW FDVar Domain 
  | OSameW FDVar FDVar
  | ODiffW FDVar FDVar
  | OLessW FDVar FDVar
  | OLessEqW FDVar FDVar
--
  | OAddW FDVar FDVar FDVar
  | OSubW FDVar FDVar FDVar
--  | OMult FDVar FDVar FDVar
--  | OAbs FDVar FDVar
  deriving (Show,Eq)


instance Solver OvertonWT where
  type Constraint OvertonWT  = OConstraintW
  type Label      OvertonWT  = FDStateW
  add c  = debug ("addOverton("++(show c)++")") $ addOvertonWT c
  run p  = debug ("runOverton") $ runOvertonWT p
  mark   = get
  goto   = put 

instance Term OvertonWT FDVar where
  newvar       = newVarWT ()
  type Help OvertonWT FDVar = ()
  help _ _ = ()

instance EnumTerm OvertonWT FDVar where
  type TermBaseType OvertonWT FDVar = Int
  getDomain = fd_domainWT 
  setValue var val = return [var `OHasValueW` val]


instance (Monoid w, EnumTerm s t) => EnumTerm (WriterT w s) t where
   type TermBaseType (WriterT w s) t = TermBaseType s t
   getDomain = lift . getDomain
   setValue var val = lift (setValue var val)


addOvertonWT :: OConstraintW -> OvertonWT Bool 
addOvertonWT c = do x <- runWriterT (addOvertonWR c)
                    return $ fst x

-- runOvertonWR :: WriterT [String] OvertonWT a -> a
runOvertonWT :: OvertonWT a -> a 
runOvertonWT a = runOvertonWR $ WriterT $ do 
                                  x <- a
                                  return (x,[]) 

newVarWT :: ToDomain a => a -> OvertonWT FDVar
newVarWT d = do x <- runWriterT (newVarWR d)
                return $ fst x
                
fd_domainWT :: FDVar -> OvertonWT [Int]
fd_domainWT t = do x <- runWriterT (fd_domainWR t)
                   return $ fst x



--------------------------------------------------------------------------------
-- Constraints -----------------------------------------------------------------
--------------------------------------------------------------------------------

addOvertonWR :: OConstraintW -> OvertonWR Bool
addOvertonWR (OHasValueW v i) = v `hasValueW` i
addOvertonWR (OHasNotValueW v i) = v `hasNotValueW` i
addOvertonWR (OInDomainW v d) = inDomainW v d
addOvertonWR (OSameW a b) = a `sameW` b
addOvertonWR (ODiffW a b) = a `differentW` b
addOvertonWR (OLessW a b) = a .<:. b
addOvertonWR (OLessEqW a b) = a .<=:. b
--
addOvertonWR (OAddW a b c) = addSumW a b c
addOvertonWR (OSubW a b c) = addSubW a b c
-- addOverton (OMult a b c) = addMult a b c
-- addOverton (OAbs a b) = addAbs a b

fd_domainWR :: FDVar -> OvertonWR [Int]
fd_domainWR v = do d <- lookupW v
                   return $ elems d

fd_objectiveW :: OvertonWR FDVar
fd_objectiveW =
  do s <- get
     return $ objectiveW s

--------------------------------------------------------------------------------

type OvertonWR a = WriterT [String] OvertonWT a
--    derives (Monad, MonadWriter [String] (OvertonFD FDState), 
--              MonadState FDState)

-- The FD monad with tracing
newtype OvertonWT a = OvertonWT { unFDW :: State FDStateW a }
    deriving (Monad, Functor, Applicative, MonadState FDStateW)

data VarInfoW = VarInfoW
     { delayedConstraintsW :: OvertonWR Bool, domainW :: Domain }

instance Show VarInfoW where
  show x = show $ domainW x

type VarMapW = Map FDVar VarInfoW

data FDStateW = FDStateW
     { varSupplyW :: VarSupply, varMapW :: VarMapW, objectiveW :: FDVar }
     deriving Show

instance Eq FDStateW where
  s1 == s2 = f s1 == f s2
           where f s = head $ elems $ domainW $ varMapW s ! (objectiveW s) 

instance Ord FDStateW where
  compare s1 s2  = compare (f s1) (f s2)
           where f s = head $ elems $  domainW $ varMapW s ! (objectiveW s) 


{-
  -- TOM: inconsistency is not observable within the OvertonFD monad
consistentFD :: OvertonFD Bool
consistentFD = return True
-}
consistentWR :: OvertonWR Bool
consistentWR = return True


evalWriterW :: (Monad m) => WriterT w m a -> m a
evalWriterW m = do
    (a, _) <- runWriterT m
    return a


-- Run the FD monad and produce a lazy list of possible solutions.
runOvertonWR :: OvertonWR a -> a
runOvertonWR fd = 
  let j = evalState (unFDW (evalWriterW fd)) initStateW
      in j

runOvertonWithTrace :: OvertonWR a -> (a, [String])
runOvertonWithTrace fd = 
  let j = evalState (unFDW (runWriterT fd)) initStateW
      in j


initStateW :: FDStateW
initStateW = FDStateW { varSupplyW = FDVar 0, 
                        varMapW = Map.empty, 
                        objectiveW = FDVar 0 }



-- Get a new FDVar
newVarWR :: ToDomain a => a -> OvertonWR FDVar
newVarWR d = WriterT $ do
    s <- get
    let v = varSupplyW s
    put $ s { varSupplyW = FDVar (unFDVar v + 1) }
    modify $ \s ->
        let vm = varMapW s
            vi = VarInfoW {
                delayedConstraintsW = return True,
                domainW = toDomain d}
        in
        s { varMapW = Map.insert v vi vm }
    return (v, ["newVar: " ++ (show v) ++ "\n"])


newVarsWR :: ToDomain a => Int -> a -> OvertonWR [FDVar]
newVarsWR n d = replicateM n (newVarWR d)

-------------------------------------------------------------------

-- Lookup the current domain of a variable.
lookupW :: FDVar -> OvertonWR Domain
lookupW x = do
    s <- get
    return . domainW $ varMapW s ! x

-- Safely lookup the current domain of a variable.
lookupEW :: FDVar -> OvertonWR String
lookupEW x = do
    s <- get
    return (case (Map.lookup x (varMapW s)) of 
                   Just y  -> show $ domainW y
                   Nothing -> "empty")  

-- Lookup and show the current domain of a variable.
lookupAndShowR :: FDVar -> String
lookupAndShowR x = show $ runOvertonWR $ lookupEW x

{-
-- Lookup and show the current domain of a variable.
lookupAndShow :: FDVar -> OvertonFD String
lookupAndShow x = do
    s <- get 
    return . show $ varMap s ! x

-- Lookup and show the current domain of a variable.
lookupAndPrint2 :: FDVar -> String
lookupAndPrint2 x = runOverton $ lookupAndShow x
-}


updateWR :: FDVar -> Domain -> OvertonWR Bool
updateWR x i = do
    debug (show x ++ " <- " ++ show i)  (return ())
    s <- get
    let vm = varMapW s
    let vi = vm ! x
    debug ("where old domain = " ++ show (domainW vi)) (return ())
    put $ s { varMapW = Map.insert x (vi { domainW = i}) vm }
    delayedConstraintsW vi



-- Add a new constraint for a variable to the constraint store.
addConstraintW :: FDVar -> OvertonWR Bool -> OvertonWR ()
addConstraintW x constraint = WriterT $ do
    s <- get
    let vm = varMapW s
    let vi = vm ! x
    let cs = delayedConstraintsW vi
    t <- put $ s { varMapW =
        Map.insert x (vi { delayedConstraintsW = 
                             do b <- cs 
                                if b then constraint 
                                     else return False}) vm }
    return (t, ["addConstraint for " ++ (show x) ++ "\n"])



-- Useful helper function for adding binary constraints between FDVars.
type BinaryConstraintW = FDVar -> FDVar -> OvertonWR Bool
addBinaryConstraintW :: BinaryConstraintW -> BinaryConstraintW 
addBinaryConstraintW f x y = do
    let constraint  = f x y
    b <- constraint 
    when b $ (do addConstraintW x constraint
                 addConstraintW y constraint)
    return b

---------------------------------------------------------------------------

-- Constrain a variable to a particular value.
hasValueW :: FDVar -> Int -> OvertonWR Bool
var `hasValueW` val = do
    vals <- lookupW var
    if val `member` vals
       then do let i = singleton val
               if (i /= vals) 
                  then updateWR var i
                  else return True
       else return False


-- Constrain a variable to exclude a particular value.
hasNotValueW :: FDVar -> Int -> OvertonWR Bool
var `hasNotValueW` val = do
    vals <- lookupW var
    updateWR var (rmFromDomain val vals)
    vals2 <- lookupW var 
    if Domain.null vals2 then return False else return True


-- Constrain a variable to range over a particular domain.
inDomainW :: FDVar -> Domain -> OvertonWR Bool
inDomainW var dom = do
    vals <- lookupW var
    let i = vals `intersection` dom
    if not $ Domain.null i
       then if (i /= vals) then (updateWR var i) else return True 
       else return False


-- Constrain two variables to have the same value.
sameW :: FDVar -> FDVar -> OvertonWR Bool
sameW = addBinaryConstraintW $ \x y -> do 
    debug "inside same" $ return ()
    xv <- lookupW x
    yv <- lookupW y
    debug (show xv ++ " same " ++ show yv) $ return ()
    let i = xv `intersection` yv
    if not $ Domain.null i
       then whenwhen (i /= xv)  (i /= yv) (updateWR x i) (updateWR y i)
       else return False


-- Constrain two variables to have different values.
differentW :: FDVar  -> FDVar  -> OvertonWR Bool
differentW = addBinaryConstraintW $ \x y -> do
    xv <- lookupW x
    yv <- lookupW y
    if not (isSingleton xv) || not (isSingleton yv) || xv /= yv
       then whenwhen (isSingleton xv && xv `isSubsetOf` yv)
                     (isSingleton yv && yv `isSubsetOf` xv)
                     (updateWR y (yv `difference` xv))
                     (updateWR x (xv `difference` yv))
       else return False


-- Constrain one variable to have a value less than the value of another
-- variable.
infix 4 .<:.
(.<:.) :: FDVar -> FDVar -> OvertonWR Bool
(.<:.) = addBinaryConstraintW $ \x y -> do
    xv <- lookupW x
    yv <- lookupW y
    let xv' = filterLessThan (findMax yv) xv
    let yv' = filterGreaterThan (findMin xv) yv
    if  not $ Domain.null xv'
        then if not $ Domain.null yv'
                then whenwhen (xv /= xv') (yv /= yv') 
                              (updateWR x xv') (updateWR y yv')
                else return False
        else return False

-- Constrain one variable to have a value less than or equal to the value of another 
-- variable.
infix 4 .<=:.
(.<=:.) :: FDVar -> FDVar -> OvertonWR Bool
(.<=:.) = addBinaryConstraintW $ \x y -> do
    xv <- lookupW x
    yv <- lookupW y
    let xv' = filterLessThan (1 + findMax yv) xv
    let yv' = filterGreaterThan ((findMin xv) - 1) yv
    if  not $ Domain.null xv'
        then if not $ Domain.null yv'
                then whenwhen (xv /= xv') (yv /= yv') 
                              (updateWR x xv') (updateWR y yv')
                else return False
        else return False

dumpWR :: [FDVar] -> OvertonWR [Domain]
dumpWR = mapM lookupW

------------------------------------------------------------------------

----------------------- ARITHMETIC CONSTRAINTS (alternative, not fixed)


type TernaryConstraintW = FDVar -> FDVar -> FDVar -> OvertonWR Bool

addTernaryConstraintW :: TernaryConstraintW -> TernaryConstraintW 
addTernaryConstraintW f x y z = do
    let constraint  = f x y z
    b <- constraint 
    when b $ (do addConstraintW x constraint
                 addConstraintW y constraint
                 addConstraintW z constraint)
    return b


addSumWX :: FDVar -> FDVar -> FDVar -> OvertonWR Bool
addSumWX = addTernaryConstraintW $ \x y z -> do
    xv <- lookupW x
    yv <- lookupW y
    zv <- lookupW z
    let min_x = findMin xv
        max_x = findMax xv
        min_y = findMin yv
        max_y = findMax yv 
        min_z = min_x + min_y 
        max_z = max_x + max_y
        zv'   = filterLessThan max_z zv
        zv''  = filterGreaterThan min_z zv'
    if  not $ Domain.null zv'' 
        then if zv /= zv''
             then do updateWR z zv'' >> addSubW z y x >> addSubW z x y
             else return True
        else return False
            

addSubWX :: FDVar -> FDVar -> FDVar -> OvertonWR Bool
addSubWX = addTernaryConstraintW $ \x y z -> do
    xv <- lookupW x
    yv <- lookupW y
    zv <- lookupW z
    let min_x = findMin xv
        max_x = findMax xv
        min_y = findMin yv
        max_y = findMax yv 
        min_z = min_x - max_y 
        max_z = max_x + min_y
        zv'   = filterLessThan max_z zv
        zv''  = filterGreaterThan min_z zv'
    if  not $ Domain.null zv'' 
        then if zv /= zv''
             then do updateWR z zv'' >> addSumW z y x >> addSubW x z y 
             else return True
        else return False



------------------ ARITHMETIC CONSTRAINT, ORIGINAL IMPLEMENTATION (not fixed)

-- Add constraint (z = x `op` y) for var z
addArithmeticConstraintW :: 
    (Domain -> Domain -> Domain) ->
    (Domain -> Domain -> Domain) ->
    (Domain -> Domain -> Domain) ->
    FDVar -> FDVar -> FDVar -> OvertonWR Bool
addArithmeticConstraintW getZDomain getXDomain getYDomain x y z = do
    xv <- lookupW x
    yv <- lookupW y
    let constraint z x y getDomain = do
        xv <- lookupW x
        yv <- lookupW y
        zv <- lookupW z
        let znew = debug "binaryArith:intersection" $ (debug "binaryArith:zv" $ zv) `intersection` (debug "binaryArith:getDomain" $ getDomain xv yv)
        debug ("binaryArith:" ++ show z ++ " before: "  ++ show zv ++ show "; after: " ++ show znew) (return ())
        if debug "binaryArith:null?" $ not $ Domain.null (debug "binaryArith:null?:znew" $ znew)
           then if (znew /= zv) 
                   then debug ("binaryArith:update") $ updateWR z znew
                   else return True
           else return False
    let zConstraint = debug "binaryArith: zConstraint" $ constraint z x y getZDomain
        xConstraint = debug "binaryArith: xConstraint" $ constraint x z y getXDomain
        yConstraint = debug "binaryArith: yConstraint" $ constraint y z x getYDomain
    debug ("addBinaryArith: z x") (return ())
    addConstraintW z xConstraint
    debug ("addBinaryArith: z y") (return ())
    addConstraintW z yConstraint
    debug ("addBinaryArith: x z") (return ())
    addConstraintW x zConstraint
    debug ("addBinaryArith: x y") (return ())
    addConstraintW x yConstraint
    debug ("addBinaryArith: y z") (return ())
    addConstraintW y zConstraint
    debug ("addBinaryArith: y x") (return ())
    addConstraintW y xConstraint
    debug ("addBinaryArith: done") (return ())
    return True

-- Add constraint (z = op x) for var z
addUnaryArithmeticConstraintW :: (Domain -> Domain) -> (Domain -> Domain) -> FDVar -> FDVar -> OvertonWR Bool
addUnaryArithmeticConstraintW getZDomain getXDomain x z = do
    xv <- lookupW x
    let constraint z x getDomain = do
        xv <- lookupW x
        zv <- lookupW z
        let znew = zv `intersection` (getDomain xv)
        debug ("unaryArith:" ++ show z ++ " before: "  ++ show zv ++ show "; after: " ++ show znew) (return ())
        if not $ Domain.null znew
           then if (znew /= zv) 
                   then updateWR z znew
                   else return True
           else return False
    let zConstraint = constraint z x getZDomain
        xConstraint = constraint x z getXDomain
    addConstraintW z xConstraint
    addConstraintW x zConstraint
    return True


addSumW = addArithmeticConstraintW getDomainPlus getDomainMinus getDomainMinus

addSubW = addArithmeticConstraintW getDomainMinus getDomainPlus (flip getDomainMinus)


addMultW = addArithmeticConstraintW getDomainMult getDomainDiv getDomainDiv

addAbsW = addUnaryArithmeticConstraintW absDomain (\z -> mapDomain z (\i -> [i,-i]))

getDomainPlus :: Domain -> Domain -> Domain
getDomainPlus xs ys = toDomain (zl, zh) where
    zl = findMin xs + findMin ys
    zh = findMax xs + findMax ys

getDomainMinus :: Domain -> Domain -> Domain
getDomainMinus xs ys = toDomain (zl, zh) where
    zl = findMin xs - findMax ys
    zh = findMax xs - findMin ys

getDomainMult :: Domain -> Domain -> Domain
getDomainMult xs ys = (\d -> debug ("multDomain" ++ show d ++ "=" ++ show xs ++ "*" ++ show ys ) d) $ toDomain (zl, zh) where
    zl = minimum products
    zh = maximum products
    products = [x * y |
        x <- [findMin xs, findMax xs],
        y <- [findMin ys, findMax ys]]

getDomainDiv :: Domain -> Domain -> Domain
getDomainDiv xs ys = toDomain (zl, zh) where
    zl = minimum quotientsl
    zh = maximum quotientsh
    quotientsl = [if y /= 0 then x `div` y else minBound |
        x <- [findMin xs, findMax xs],
        y <- [findMin ys, findMax ys]]
    quotientsh = [if y /= 0 then x `div` y else maxBound |
        x <- [findMin xs, findMax xs],
        y <- [findMin ys, findMax ys]]



