{-
 - High-level constraint syntax.
 -
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Paul Torrini
 - 
 -}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}

module Control.CP.FD.OvertonFD.UserSyntax (
  TExpr(..),
  CEx(..),
  Annot(..),
  tFEExpr2model,
  desugarT,
  OvAnswer
) where

import Prelude hiding (lookup)
import Data.Maybe (fromJust,isJust)
import Control.Monad.Trans.State.Lazy
import Control.Monad.Trans.RWS
import Control.Monad.Trans.Writer
import qualified Data.Map as Map
import Data.Map ((!), Map)
import qualified Data.IntSet as IntSet
import Control.Monad (liftM,(<=<))

import Control.Mixin.Mixin

import Control.CP.FD.OvertonFD.Domain as Domain
import Control.CP.FD.OvertonFD.OvertonFD
-- import Control.CP.FD.OvertonFD.OvertonWT
import Control.CP.FD.FD hiding ((!))
import Control.CP.FD.Example
import Control.CP.Solver
import Control.CP.FMSearchTree
import Control.CP.FMFunctorTree
import Control.CP.EnumTerm
import Data.List (tails)

import Control.CP.Debug

-- import Data.Functor.Foldable

------------------------------------------

class EnumExpr e where
  toInt :: e -> Int
  toBool :: e -> Bool

instance Enum e => EnumExpr e where
  toInt = fromEnum
  toBool x = if (fromEnum x == 0) then False else True

------------------------------------------

data Annot = ALL | FIRST | OTHER

data TExpr v e =  
    GEXISTS (v -> TExpr v e) 
  | GEXIST Int ([v] -> TExpr v e) 
  | SOLVE_FOR [v] (CEx v e)
  | SOLVE_FOR_SHOW [v] [v] (CEx v e)
  | ANN_SOLVE_FOR Annot [v] (CEx v e)
  | ANN_SOLVE_FOR_SHOW Annot [v] [v] (CEx v e)


data CEx v e = 
    FALSE
  | AND (CEx v e) (CEx v e)  
  | OR (CEx v e) (CEx v e)    
  | CONJ [CEx v e]
  | DISJ [CEx v e] 
  | IF_THEN_ELSE e (CEx v e) (CEx v e)  
  | CASE e [(e, CEx v e)] 
  | EXISTS (v -> CEx v e) 
  | EXIST Int ([v] -> CEx v e) 
  | HAS_VAL v e
  | DIFFER_VAL v e
  | LESS_VAL v e
  | LESS_EQ_VAL v e
  | GREATER_VAL v e
  | GREATER_EQ_VAL v e
  | IN_INTERVAL v e e
  | IN_SET v [e]
  | IN_INTERVAL_SET v [(e,e)]
--
  | SAME v v
  | DIFFER v v
  | LESS v v
  | LESS_EQ v v
  | GREATER v v
  | GREATER_EQ v v
--
  | BETWEEN v v v
  | STR_BETWEEN v v v
--
  | ADD v v v
  | SUB v v v
  | MULT v v v 
  | ABS v v
  | FUNC (Int -> Int) [Int] v v
--
  | ALL_HAS_VAL [v] e
  | ALL_DIFFER_VAL [v] e
  | ALL_LESS_VAL [v] e
  | ALL_LESS_EQ_VAL [v] e
  | ALL_GREATER_VAL [v] e
  | ALL_GREATER_EQ_VAL [v] e
  | ALL_IN_INTERVAL [v] e e
  | ALL_IN_SET [v] [e]
  | ALL_IN_INTERVAL_SET [v] [(e,e)]
-- 
  | ALL_DIFFER [v] v
  | ALL_LESS [v] v
  | ALL_LESS_EQ [v] v
  | ALL_GREATER [v] v
  | ALL_GREATER_EQ [v] v
  | SUM [v] v
--
  | ALL_SAME [v]
  | ALL_DIFFERENT [v]
  | STR_ORDERED_LIST [v]
  | ORDERED_LIST [v] 
--
  | ARR_HAS_VAL Int [v] [e]
  | ARR_DIFFER_VAL Int [v] [e]
  | ARR_LESS_VAL Int [v] [e]
  | ARR_LESS_EQ_VAL Int [v] [e]
  | ARR_GREATER_VAL Int [v] [e]
  | ARR_GREATER_EQ_VAL Int [v] [e]
  | ARR_IN_INTERVAL Int [v] [(e,e)]  
  | ARR_IN_SET Int [v] [[e]]  
  | ARR_IN_INTERVAL_SET Int [v] [[(e,e)]]   


desugarT :: TExpr v e -> FEExpr v e
desugarT = mixin desgT

desgT :: Mixin2 (TExpr v e) (FEExpr v e)
desgT _ r (GEXISTS f) = FEE $ B_GEXISTS (\x -> r $ f x)
desgT _ r (GEXIST n f) = FEE $ B_GEXIST n (\x -> r $ f x)
desgT _ r (SOLVE_FOR vs e) = fELift $ B_SOLVE_FOR vs (desugarC e)
desgT _ r (SOLVE_FOR_SHOW vs1 vs2 e) = 
                   fELift $ B_SOLVE_FOR_SHOW vs1 vs2 (desugarC e)
desgT _ r (ANN_SOLVE_FOR a vs e) = 
                   fELift $ B_ANN_SOLVE_FOR a vs (desugarC e)
desgT _ r (ANN_SOLVE_FOR_SHOW a vs1 vs2 e) = 
                   fELift $ B_ANN_SOLVE_FOR_SHOW a vs1 vs2 (desugarC e)


desugarC :: CEx v e -> FCExpr v e
desugarC = mixin desgC

desgC :: Mixin2 (CEx v e) (FCExpr v e)

desgC _ r (AND c1 c2) = FCE (B_AND (r c1) (r c2))
desgC _ r (OR c1 c2) = FCE (B_OR (r c1) (r c2))
desgC _ r (CONJ cs) = FCE (B_CONJ (map r cs))
desgC _ r (DISJ cs) = FCE (B_DISJ (map r cs))
desgC _ r (IF_THEN_ELSE e c1 c2) = FCE (B_IF_ELSE e (r c1) (r c2))
desgC _ r (CASE e ls) = FCE (B_CASE e [(x, r y) | (x,y) <- ls])
desgC _ r (EXISTS f) = FCE (B_EXISTS (r . f))
desgC _ r (EXIST n f) = FCE (B_EXIST n (r . f))
desgC _ _ x = fCLift (desgB x)

desgB :: CEx v e -> CExpr v e
desgB FALSE = B_FALSE
desgB (HAS_VAL v e ) = B_HAS_VAL v e
desgB (DIFFER_VAL v e) = B_DIFFER_VAL v e
desgB (LESS_VAL v e) = B_LESS_VAL v e
desgB (LESS_EQ_VAL v e) = B_LESS_EQ_VAL v e
desgB (GREATER_VAL v e) = B_GREATER_VAL v e
desgB (GREATER_EQ_VAL v e) = B_GREATER_EQ_VAL v e
desgB (IN_INTERVAL v e1 e2) = B_IN_INTERVAL v e1 e2
desgB (IN_SET v es) = B_IN_SET v es
desgB (IN_INTERVAL_SET v ls) = B_IN_INTERVAL_SET v ls
--
desgB (SAME v1 v2) = B_SAME v1 v2
desgB (DIFFER v1 v2) = B_DIFFER v1 v2
desgB (LESS v1 v2) = B_LESS v1 v2
desgB (LESS_EQ v1 v2) = B_LESS_EQ v1 v2
desgB (GREATER v1 v2) = B_LESS v2 v1
desgB (GREATER_EQ v1 v2) = B_LESS_EQ v2 v1
--
desgB (BETWEEN v1 v2 v3) = B_BETWEEN v1 v2 v3
desgB (STR_BETWEEN v1 v2 v3) = B_STR_BETWEEN v1 v2 v3
--
desgB (ADD v1 v2 v3) = B_ADD v1 v2 v3
desgB (SUB v1 v2 v3) = B_SUB v1 v2 v3
desgB (MULT v1 v2 v3) = B_MULT v1 v2 v3 
desgB (ABS v1 v2) = B_ABS v1 v2
desgB (FUNC f ls v1 v2) = B_FUNC f ls v1 v2
--
desgB (ALL_HAS_VAL vs e) = B_ALL_HAS_VAL vs e
desgB (ALL_DIFFER_VAL vs e) = B_ALL_DIFFER_VAL vs e
desgB (ALL_LESS_VAL vs e) = B_ALL_LESS_VAL vs e
desgB (ALL_LESS_EQ_VAL vs e) = B_ALL_LESS_EQ_VAL vs e
desgB (ALL_GREATER_VAL vs e) = B_ALL_GREATER_VAL vs e
desgB (ALL_GREATER_EQ_VAL vs e) = B_ALL_GREATER_EQ_VAL vs e
desgB (ALL_IN_INTERVAL vs e1 e2) = B_ALL_IN_INTERVAL vs e1 e2
desgB (ALL_IN_SET vs es) = B_ALL_IN_SET vs es
desgB (ALL_IN_INTERVAL_SET vs ls) = B_ALL_IN_INTERVAL_SET vs ls 
-- 
desgB (ALL_DIFFER vs v) = B_ALL_DIFFER vs v
desgB (ALL_LESS vs v) = B_ALL_LESS vs v
desgB (ALL_LESS_EQ vs v) = B_ALL_LESS_EQ vs v
desgB (ALL_GREATER vs v) = B_ALL_GREATER vs v
desgB (ALL_GREATER_EQ vs v) = B_ALL_GREATER_EQ vs v
desgB (SUM vs v) = B_SUM vs v
--
desgB (ALL_SAME vs) = B_ALL_SAME vs
desgB (ALL_DIFFERENT vs) = B_ALL_DIFFERENT vs
desgB (STR_ORDERED_LIST vs) = B_STR_ORDERED_LIST vs
desgB (ORDERED_LIST vs) = B_STR_ORDERED_LIST vs 
--
desgB (ARR_HAS_VAL n vs es) = B_ARR_HAS_VAL n vs es
desgB (ARR_DIFFER_VAL n vs es) = B_ARR_HAS_VAL n vs es
desgB (ARR_LESS_VAL n vs es) = B_ARR_HAS_VAL n vs es
desgB (ARR_LESS_EQ_VAL n vs es) = B_ARR_LESS_EQ_VAL n vs es
desgB (ARR_GREATER_VAL n vs es) = B_ARR_GREATER_VAL n vs es
desgB (ARR_GREATER_EQ_VAL n vs es) = B_ARR_GREATER_EQ_VAL n vs es
desgB (ARR_IN_INTERVAL n vs ls) = B_ARR_IN_INTERVAL n vs ls  
desgB (ARR_IN_SET n vs ls) = B_ARR_IN_SET n vs ls  
desgB (ARR_IN_INTERVAL_SET n vs ls) = B_ARR_IN_INTERVAL_SET n vs ls   


--------------------------------------------
data FEExpr v e = FEE { unFEE :: LEExpr v e (FEExpr v e) }

data LEExpr v e a = 
    ELift (EExpr v e) 
  | B_GEXISTS (v -> a) 
  | B_GEXIST Int ([v] -> a)

fELift :: EExpr v e -> FEExpr v e
fELift = FEE . ELift


data EExpr v e = 
    B_SOLVE_FOR [v] (FCExpr v e)
  | B_SOLVE_FOR_SHOW [v] [v] (FCExpr v e)
  | B_ANN_SOLVE_FOR Annot [v] (FCExpr v e)
  | B_ANN_SOLVE_FOR_SHOW Annot [v] [v] (FCExpr v e)


data FCExpr v e = FCE { unFCE :: LCExpr v e (FCExpr v e) }

data LCExpr v e a = 
    CLift (CExpr v e)
  | B_AND a a  
  | B_OR a a   
  | B_CONJ [a]
  | B_DISJ [a] 
  | B_IF_ELSE e a a 
  | B_CASE e [(e, a)] 
  | B_EXISTS (v -> a) 
  | B_EXIST Int ([v] -> a) 


fCLift :: CExpr v e -> FCExpr v e
fCLift = FCE . CLift


data CExpr v e = 
    B_FALSE
  | B_HAS_VAL v e
  | B_DIFFER_VAL v e
  | B_LESS_VAL v e
  | B_LESS_EQ_VAL v e
  | B_GREATER_VAL v e
  | B_GREATER_EQ_VAL v e
  | B_IN_INTERVAL v e e
  | B_IN_SET v [e]
  | B_IN_INTERVAL_SET v [(e,e)]
--
  | B_SAME v v
  | B_DIFFER v v
  | B_LESS v v
  | B_LESS_EQ v v
--
  | B_BETWEEN v v v
  | B_STR_BETWEEN v v v
--
  | B_ADD v v v
  | B_SUB v v v
  | B_MULT v v v 
  | B_ABS v v
  | B_FUNC (Int -> Int) [Int] v v
--
--
  | B_ALL_HAS_VAL [v] e
  | B_ALL_DIFFER_VAL [v] e
  | B_ALL_LESS_VAL [v] e
  | B_ALL_LESS_EQ_VAL [v] e
  | B_ALL_GREATER_VAL [v] e
  | B_ALL_GREATER_EQ_VAL [v] e
  | B_ALL_IN_INTERVAL [v] e e
  | B_ALL_IN_SET [v] [e]
  | B_ALL_IN_INTERVAL_SET [v] [(e,e)]
-- 
  | B_ALL_DIFFER [v] v
  | B_ALL_LESS [v] v
  | B_ALL_LESS_EQ [v] v
  | B_ALL_GREATER [v] v
  | B_ALL_GREATER_EQ [v] v
  | B_SUM [v] v
--
  | B_ALL_SAME [v] 
  | B_ALL_DIFFERENT [v]
  | B_STR_ORDERED_LIST [v]
  | B_ORDERED_LIST [v] 
--
--
  | B_ARR_HAS_VAL Int [v] [e]
  | B_ARR_DIFFER_VAL Int [v] [e]
  | B_ARR_LESS_VAL Int [v] [e]
  | B_ARR_LESS_EQ_VAL Int [v] [e]
  | B_ARR_GREATER_VAL Int [v] [e]
  | B_ARR_GREATER_EQ_VAL Int [v] [e]
  | B_ARR_IN_INTERVAL Int [v] [(e,e)]  
  | B_ARR_IN_SET Int [v] [[e]]  
  | B_ARR_IN_INTERVAL_SET Int [v] [[(e,e)]]   
  deriving (Show, Eq)  

---------------------------------------------------------------------

instance Functor (LEExpr v e) where
   fmap f (ELift w) = ELift w
   fmap f (B_GEXISTS g) = B_GEXISTS (f . g)
   fmap f (B_GEXIST n g) = B_GEXIST n (f . g)

instance Functor (LCExpr v e) where
   fmap f (CLift w) = CLift w
   fmap f (B_AND w1 w2) = B_AND (f w1) (f w2)
   fmap f (B_OR w1 w2) = B_OR (f w1) (f w2)
   fmap f (B_CONJ ls) = B_CONJ (map f ls) 
   fmap f (B_DISJ ls) = B_DISJ (map f ls) 
   fmap f (B_IF_ELSE e w1 w2) = B_IF_ELSE e (f w1) (f w2) 
   fmap f (B_CASE e ls) = B_CASE e [(a, f b) | (a,b) <- ls] 
   fmap f (B_EXISTS g) = B_EXISTS (f . g)
   fmap f (B_EXIST n g) = B_EXIST n (f . g)


cataE :: (LEExpr v e a -> a) -> FEExpr v e -> a
cataE f x = f (fmap (cataE f) (unFEE x))

cataME :: (forall r. (r -> a) -> LEExpr v e r -> a) -> FEExpr v e -> a 
cataME f x = f id (fmap (cataME f) (unFEE x)) 

cataC :: (LCExpr v e a -> a) -> FCExpr v e -> a
cataC f x = f (fmap (cataC f) (unFCE x))

cataMC :: (forall r. (r -> a) -> LCExpr v e r -> a) -> FCExpr v e -> a 
cataMC f x = f id (fmap (cataMC f) (unFCE x)) 


---------------------------------------------

type Mixin2 a b = Mixin (a -> b)

class (Show v, EnumTerm (TreeSolver f v) v, FunctorTree f v) => 
                                            ExtFunctorTree f v where 
   fHasValue :: Int -> v -> Free (f v) Bool 
   fHasNotValue :: Int -> v -> Free (f v) Bool 
   fInDomain :: Domain -> v -> Free (f v) Bool  
   fIsLess :: Int -> v -> Free (f v) Bool
   fIsGreater :: Int -> v -> Free (f v) Bool
   fSame :: v -> v -> Free (f v) Bool 
   fDiff :: v -> v -> Free (f v) Bool
   fLess :: v -> v -> Free (f v) Bool 
   fLessEq :: v -> v -> Free (f v) Bool
   fAdd :: v -> v -> v -> Free (f v) Bool
   fSub :: v -> v -> v -> Free (f v) Bool
   fMult :: v -> v -> v -> Free (f v) Bool
   fAbs :: v -> v -> Free (f v) Bool
   fFunc :: (Int -> Int) -> Domain -> v -> v -> Free (f v) Bool
--   fEnumerate :: [v] -> Free (f v) Bool


cons2mod :: ExtFunctorTree f v => 
               Constraint (TreeSolver f v) -> Free (f v) Bool
cons2mod c = addTo c (return True)


instance ExtFunctorTree (CP OvertonFD) FDVar where
   fHasValue i v = cons2mod (OHasValue v i) 
   fHasNotValue i v = cons2mod (OHasNotValue v i)
   fInDomain d v = cons2mod (OInDomain v d)
   fIsLess i v = cons2mod (OIsLess v i)
   fIsGreater i v = cons2mod (OIsGreater v i)
   fSame v1 v2 = cons2mod (OSame v1 v2)
   fDiff v1 v2 = cons2mod (ODiff v1 v2) 
   fLess v1 v2 = cons2mod (OLess v1 v2)
   fLessEq v1 v2 = cons2mod (OLessEq v1 v2)
   fAdd v1 v2 v3 = cons2mod (OAdd v1 v2 v3)
   fSub v1 v2 v3 = cons2mod (OSub v1 v2 v3)
   fMult v1 v2 v3 = cons2mod (OMult v1 v2 v3)
   fAbs v1 v2 = cons2mod (OAbs v1 v2)
   fFunc f d v1 v2 = cons2mod (OFunc f d v1 v2)
--   fEnumerate vs = enumerateDyn vs

----------------------------------------------

type OvAnswer = [TermBaseType OvertonFD FDVar]



tFEExpr2model :: FEExpr FDVar Int -> Free (CP OvertonFD FDVar) OvAnswer 
tFEExpr2model = cataME tLEExpr2modM 


tLEExpr2modM :: forall a. (a -> Free (CP OvertonFD FDVar) OvAnswer) 
                   -> LEExpr FDVar Int a -> 
                      Free (CP OvertonFD FDVar) OvAnswer
tLEExpr2modM r (ELift t) = tEExpr2model t
tLEExpr2modM r (B_GEXISTS f)  = exists (\x -> r (f x))
tLEExpr2modM r (B_GEXIST 0 f) = r (f [])
tLEExpr2modM r (B_GEXIST n f) = exists (\x -> 
                      tLEExpr2modM r (B_GEXIST (n-1) (\ls -> f (x:ls))))


---------------

tEExpr2model :: EExpr FDVar Int -> Free (CP OvertonFD FDVar) OvAnswer 
tEExpr2model (B_SOLVE_FOR vs e) = 
          tFCExpr2model e >> enumerateDyn vs >> assignments vs 
tEExpr2model (B_SOLVE_FOR_SHOW vs1 vs2 e) = 
          tFCExpr2model e >> enumerateDyn vs1 >> assignments vs2 
tEExpr2model (B_ANN_SOLVE_FOR a vs e) = case a of 
    ALL -> tFCExpr2model e >> enumerateDyn vs >> assignments vs 
    FIRST -> tFCExpr2model e >> enumerateDyn vs >> assignmentsFirst vs
    OTHER -> undefined 
tEExpr2model (B_ANN_SOLVE_FOR_SHOW a vs1 vs2 e) = case a of 
    ALL -> tFCExpr2model e >> enumerateDyn vs1 >> assignments vs2 
    FIRST -> tFCExpr2model e >> enumerateDyn vs1 >> assignmentsFirst vs2
    OTHER -> undefined 

----------------

tFCExpr2model :: FCExpr FDVar Int -> Free (CP OvertonFD FDVar) Bool
tFCExpr2model = cataMC tLCExpr2modM


----------------

tLCExpr2modM :: forall a. (a -> Free (CP OvertonFD FDVar) Bool) 
                   -> LCExpr FDVar Int a -> 
                      Free (CP OvertonFD FDVar) Bool
--
tLCExpr2modM r (CLift t) = tCExpr2model t
--
tLCExpr2modM r (B_AND c1 c2) = (r c1) >> (r c2)
tLCExpr2modM r (B_OR c1 c2) = (r c1) \/ (r c2)
tLCExpr2modM r (B_CONJ ls) = forallLift r ls 
--  foldl (\x y -> x >> r y) (return True) ls  
tLCExpr2modM r (B_DISJ ls) = foldl (\x y -> x \/ r y) false ls
tLCExpr2modM r (B_IF_ELSE e c1 c2) = if (toBool e) then r c1 else r c2  
tLCExpr2modM r (B_CASE e ls) = foldl (\x y -> x \/ r y) false 
                                 [ x | (e',x) <- ls, e == e' ]
--
tLCExpr2modM r (B_EXISTS f)  = exists (\x -> r (f x))
tLCExpr2modM r (B_EXIST 0 f) = r (f [])
tLCExpr2modM r (B_EXIST n f) = exists (\x -> 
                     tLCExpr2modM r (B_EXIST (n-1) (\ls -> f (x:ls))))


------------
tCExpr2model :: CExpr FDVar Int -> Free (CP OvertonFD FDVar) Bool 
tCExpr2model = mixin tCExpr2modM
----------

tCExpr2modM :: Mixin2 (CExpr FDVar Int) (Free (CP OvertonFD FDVar) Bool) 

tCExpr2modM _ r (B_FALSE) = false
tCExpr2modM _ r (B_HAS_VAL v e) = fHasValue e v
tCExpr2modM _ r (B_DIFFER_VAL v e) = fHasNotValue e v
tCExpr2modM _ r (B_LESS_VAL v e) = fIsLess e v
tCExpr2modM _ r (B_LESS_EQ_VAL v e) = fIsLess (e+1) v
tCExpr2modM _ r (B_GREATER_VAL v e) = fIsGreater e v
tCExpr2modM _ r (B_GREATER_EQ_VAL v e) = fIsGreater (e-1) v
tCExpr2modM _ r (B_IN_INTERVAL v e1 e2) = fInDomain (Range e1 e2) v
tCExpr2modM _ r (B_IN_SET v ls) = 
                 fInDomain (Set (IntSet.fromList ls)) v
tCExpr2modM _ r (B_IN_INTERVAL_SET v lxs) = foldl (\x y -> x \/ r y) false 
                    [B_IN_SET v (elems (Range a b)) | (a,b) <- lxs]
--
tCExpr2modM _ r (B_SAME v1 v2) = fSame v1 v2
tCExpr2modM _ r (B_DIFFER v1 v2) = fDiff v1 v2 
tCExpr2modM _ r (B_LESS v1 v2) = fLess v1 v2
tCExpr2modM _ r (B_LESS_EQ v1 v2) = fLessEq v1 v2
--
tCExpr2modM _ r (B_BETWEEN v1 v2 v3) = fLess v1 v2 >> fLess v2 v3
tCExpr2modM _ r (B_STR_BETWEEN v1 v2 v3) = fLessEq v1 v2 >> fLessEq v2 v3
--
tCExpr2modM _ r (B_ADD v1 v2 v3) = fAdd v1 v2 v3
tCExpr2modM _ r (B_SUB v1 v2 v3) = fSub v1 v2 v3
tCExpr2modM _ r (B_MULT v1 v2 v3) = fMult v1 v2 v3
tCExpr2modM _ r (B_ABS v1 v2) = fAbs v1 v2
tCExpr2modM _ r (B_FUNC f ls v1 v2) = fFunc f (Set $ IntSet.fromList ls) v1 v2
--
tCExpr2modM _ r (B_ALL_HAS_VAL ls e) = forallLift (fHasValue e) ls
tCExpr2modM _ r (B_ALL_DIFFER_VAL ls e) = forallLift (fHasNotValue e) ls
tCExpr2modM _ r (B_ALL_LESS_VAL ls e) = forallLift (fIsLess e) ls
tCExpr2modM _ r (B_ALL_LESS_EQ_VAL ls e) = forallLift (fIsLess (e+1)) ls
tCExpr2modM _ r (B_ALL_GREATER_VAL ls e) = forallLift (fIsGreater e) ls
tCExpr2modM _ r (B_ALL_GREATER_EQ_VAL ls e) = forallLift (fIsGreater (e-1)) ls
tCExpr2modM _ r (B_ALL_IN_INTERVAL ls e1 e2) = 
                                   forallLift (fInDomain (Range e1 e2)) ls
tCExpr2modM _ r (B_ALL_IN_SET ls es) = forallLiftA r (\v -> B_IN_SET v es) ls
tCExpr2modM _ r (B_ALL_IN_INTERVAL_SET ls ps) = 
                      forallLiftA r (\v -> B_IN_INTERVAL_SET v ps) ls
--
tCExpr2modM _ r (B_ALL_DIFFER vs v) = forallLift (fDiff v) vs 
tCExpr2modM _ r (B_ALL_LESS vs v) = forallLift (\x -> fLess x v) vs 
tCExpr2modM _ r (B_ALL_LESS_EQ vs v) = forallLift (\x -> fLessEq x v) vs  
tCExpr2modM _ r (B_ALL_GREATER vs v) = forallLift (fLess v) vs 
tCExpr2modM _ r (B_ALL_GREATER_EQ vs v) = forallLift (fLessEq v) vs    
tCExpr2modM _ r (B_SUM vs v) = if (Prelude.null vs) then (r $ B_HAS_VAL v 0)  
       else if (Prelude.null $ tail vs) then (r $ B_SAME (head vs) v)
       else (exists  
            (\v' -> (r $ B_SUM (tail vs) v') >> (r $ B_ADD v' (head vs) v)))  
--
tCExpr2modM _ r (B_ALL_SAME vs) = if length vs < 2 then (return True)
       else forallLift r [B_SAME v (head vs) | v <- vs] 
tCExpr2modM _ r (B_ALL_DIFFERENT ls) =  
          forallLift r [ B_DIFFER qi qj | qi:qls <- tails ls, qj <- qls ]
tCExpr2modM _ r (B_STR_ORDERED_LIST ls) = 
          forallLift r [ B_LESS qi qj | qi:qj:_ <- tails ls]
tCExpr2modM _ r (B_ORDERED_LIST ls) = 
          forallLift r [ B_LESS_EQ qi qj | qi:qj:_ <- tails ls]
--
tCExpr2modM _ r (B_ARR_HAS_VAL n vs es) = 
          forallLift r [B_HAS_VAL e v | (e,v) <- zip vs es]  
tCExpr2modM _ r (B_ARR_DIFFER_VAL n vs es) = 
          forallLift r [B_DIFFER_VAL e v | (e,v) <- zip vs es]  
tCExpr2modM _ r (B_ARR_LESS_VAL n vs es) = 
          forallLift r [B_LESS_VAL e v | (e,v) <- zip vs es]  
tCExpr2modM _ r (B_ARR_LESS_EQ_VAL n vs es) = 
          forallLift r [B_LESS_EQ_VAL e v | (e,v) <- zip vs es]  
tCExpr2modM _ r (B_ARR_GREATER_VAL n vs es) = 
          forallLift r [B_GREATER_VAL e v | (e,v) <- zip vs es]  
tCExpr2modM _ r (B_ARR_GREATER_EQ_VAL n vs es) = 
          forallLift r [B_GREATER_EQ_VAL e v | (e,v) <- zip vs es]  
tCExpr2modM _ r (B_ARR_IN_INTERVAL n vs es) = 
          forallLift r [B_IN_INTERVAL e v1 v2 | (e,(v1,v2)) <- zip vs es]  
tCExpr2modM _ r (B_ARR_IN_SET n vs es) = 
          forallLift r [B_IN_SET e v | (e,v) <- zip vs es]  
tCExpr2modM _ r (B_ARR_IN_INTERVAL_SET n vs es) = 
          forallLift r [B_IN_INTERVAL_SET e v | (e,v) <- zip vs es]  


forallLift :: Monad m => (v -> m Bool) -> [v] -> m Bool
forallLift f ls = foldl (\x y -> x >> f y) (return True) ls  

forallLiftA :: Monad m => (e -> m Bool) -> (v -> e) -> [v] -> m Bool
forallLiftA r f ls = foldl (\x y -> x >> r y) (return True) (map f ls) 



