{-
 -      Translation from high-level constraint syntax to 
 -      CPL-backend MZTypes syntax 
 -
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Paul Torrini
 - 
 -}
module Control.CP.FD.OvertonFD.US2MZ (
  texpr2mzn,
  runText2mzn
) where

import Prelude hiding (lookup)
import Data.Maybe (fromJust,isJust)
import Control.Monad.Trans.State.Lazy 
import Control.Monad.Trans.Writer
import qualified Data.Map as Map
import Data.Map ((!), Map)
import qualified Data.IntSet as IntSet
import Control.Monad (liftM,(<=<))

import Control.Mixin.Mixin

import Control.CP.FD.OvertonFD.Domain as Domain
import Control.CP.FD.OvertonFD.OvertonFD
-- import Control.CP.FD.OvertonFD.OvertonWT
import Control.CP.FD.FD hiding ((!))
import Control.CP.FD.Example
import Control.CP.Solver
import Control.CP.FMSearchTree
import Control.CP.FMFunctorTree
import Control.CP.EnumTerm
import Data.List (tails)

import Control.CP.Debug

import Data.Foldable

import Test.MZTypes as MZ
import Control.CP.FD.OvertonFD.UserSyntax



runText2mzn :: TExpr FDVar Int -> MZ.Model
runText2mzn e = evalState (texpr2mzn e) 0 


texpr2mzn :: TExpr FDVar Int -> State Int MZ.Model

texpr2mzn (GEXISTS f) =   
     do n <- get
        put (n+1)
        let d1 = (Declare Dec Int ("var" ++ (show n)) Nothing) 
        ds <- texpr2mzn (f $ FDVar n)
        return (d1:ds)

texpr2mzn (GEXIST i f) = 
     do n <- get
        put (n+i)
        let vls = [FDVar (n+x) | x <- [0..(i-1)]]
        let ds1 = [Declare Dec Int ("var" ++ (show (n+x))) Nothing | 
                      x <- [0..(i-1)]] 
        ds2 <- texpr2mzn (f vls)
        return (ds1 ++ ds2)


texpr2mzn (SOLVE_FOR vs c) = texpr2mznSolve vs c

texpr2mzn (SOLVE_FOR_SHOW _ vs c) = texpr2mznSolve vs c

texpr2mzn (ANN_SOLVE_FOR _ vs c) = texpr2mznSolve vs c

texpr2mzn (ANN_SOLVE_FOR_SHOW _ _ vs c) = texpr2mznSolve vs c



texpr2mznSolve :: [FDVar] -> CEx FDVar Int -> State Int MZ.Model
texpr2mznSolve vs c = do
   d1 <- xx2mzn c  
   let d2 = Solve Satisfy  
   let d3 = Output $ ArrayLit (
              concat [[SConst ("var" ++ (show x) ++ "="),
                      Call Show [Var $ "var" ++ (show x)], 
                      SConst "\n"] | x <- [y | FDVar y <- vs]])
   let ds = [d2,d3]
   return (d1:ds)


-- State Int = StateT Int Identity     
xx2mzn :: CEx FDVar Int -> State Int MZ.Item
xx2mzn c = do x <- cex2mzn c
              return (Constraint x)

   
cex2mzn :: CEx FDVar Int -> State Int MZ.Expr
 
cex2mzn (FALSE) = return (BConst False)

cex2mzn (AND c1 c2) = do
       x1 <- cex2mzn c1
       x2 <- cex2mzn c2 
       return (Bi And x1 x2)

cex2mzn (OR c1 c2) = do
       x1 <- cex2mzn c1
       x2 <- cex2mzn c2 
       return (Bi Or x1 x2)

-- cex2mzn (CONJ cs) = ...

-- cex2mzn (DISJ cs) = ...

cex2mzn (IF_THEN_ELSE e c1 c2) = do 
       x1 <- cex2mzn c1
       x2 <- cex2mzn c2 
       let toBool e = if (e>0) then True else False 
       return $ 
          Bi Or (Bi Rarrow (Bi MZ.Eq (BConst (toBool e)) (BConst True)) x1) 
                (Bi Rarrow (Bi MZ.Eq (BConst (toBool e)) (BConst False)) x2) 

-- cex2mzn (CASE e pls) = ...

cex2mzn (EXISTS f) = do 
        n <- get
        put (n+1)
        k <- cex2mzn (f $ FDVar n) 
        let gs = [([show ("var" ++ (show n))], intExpr)]
        return (GenCall Exists (gs, Nothing) k)

 
cex2mzn (EXIST i f) = do 
        n <- get
        let vls = [FDVar (n+x) | x <- [0..(i-1)]]
        k <- cex2mzn (f vls)  
        foldlM (\c x -> 
                 do n <- get
                    put (n+i)
                    let gs = [([show ("var" ++ (show $ n + x))], intExpr)]
                    return $ 
                        GenCall Exists (gs,Nothing) c)
             k [0..(i-1)]
 

{-
-- need to add item list to the state
cex2mzn (HAS_VAL (FDVar n) e) = 
        return $ Assign (show ("var" ++ (show n))) (anyexp2mzn e)        
-}

cex2mzn (HAS_VAL (FDVar n) e) = 
   return $ Bi Eq (Var $ "var" ++ (show n)) (anyexp2mzn e)

cex2mzn (DIFFER_VAL (FDVar n) e) = 
   return $ Bi Neq (Var $ "var" ++ (show n)) (anyexp2mzn e)

cex2mzn (LESS_VAL (FDVar n) e) = 
   return $ Bi Lt (Var $ "var" ++ (show n)) (anyexp2mzn e)

cex2mzn (LESS_EQ_VAL (FDVar n) e) = 
   return $ Bi Lte (Var $ "var" ++ (show n)) (anyexp2mzn e)

cex2mzn (GREATER_VAL (FDVar n) e) = 
   return $ Bi Gt (Var $ "var" ++ (show n)) (anyexp2mzn e)

cex2mzn (GREATER_EQ_VAL (FDVar n) e) = 
   return $ Bi Gte (Var $ "var" ++ (show n)) (anyexp2mzn e)

cex2mzn (IN_INTERVAL (FDVar n) e1 e2) = 
   return $ Bi In (Var $ "var" ++ (show n)) 
                  (MZ.Interval (anyexp2mzn e1) (anyexp2mzn e2))

cex2mzn (IN_SET (FDVar n) ls) = 
   return $ Bi In (Var $ "var" ++ (show n)) 
                  (SetLit (map anyexp2mzn ls))

{-
use foldl
cex2mzn (IN_INTERVAL_SET (FDVar n) ls) = ...
-}

cex2mzn (SAME (FDVar n1) (FDVar n2)) = 
   return $ Bi Eq (Var $ "var" ++ (show n1)) (Var $ "var" ++ (show n2)) 

cex2mzn (DIFFER (FDVar n1) (FDVar n2)) = 
   return $ Bi Neq (Var $ "var" ++ (show n1)) (Var $ "var" ++ (show n2)) 

cex2mzn (LESS (FDVar n1) (FDVar n2)) = 
   return $ Bi Lt (Var $ "var" ++ (show n1)) (Var $ "var" ++ (show n2)) 

cex2mzn (LESS_EQ (FDVar n1) (FDVar n2)) = 
   return $ Bi Lte (Var $ "var" ++ (show n1)) (Var $ "var" ++ (show n2)) 

cex2mzn (BETWEEN (FDVar n1) (FDVar n2) (FDVar n3)) = do
     x1 <- cex2mzn (LESS_EQ (FDVar n1) (FDVar n2))  
     x2 <- cex2mzn (LESS_EQ (FDVar n1) (FDVar n3))  
     return (Bi And x1 x2)
 
cex2mzn (STR_BETWEEN (FDVar n1) (FDVar n2) (FDVar n3)) = do
     x1 <- cex2mzn (LESS (FDVar n1) (FDVar n2))  
     x2 <- cex2mzn (LESS (FDVar n1) (FDVar n3))  
     return (Bi And x1 x2)

cex2mzn (ADD (FDVar n1) (FDVar n2) (FDVar n3)) = 
   let x = Bi BPlus (Var $ "var" ++ (show n1)) (Var $ "var" ++ (show n2)) 
   in return $ (Bi Eq (Var $ "var" ++ (show n3))) x

cex2mzn (SUB (FDVar n1) (FDVar n2) (FDVar n3)) = 
   let x = Bi BMinus (Var $ "var" ++ (show n1)) (Var $ "var" ++ (show n2)) 
   in return $ (Bi Eq (Var $ "var" ++ (show n3))) x

cex2mzn (MULT (FDVar n1) (FDVar n2) (FDVar n3)) = 
   let x = Bi Times (Var $ "var" ++ (show n1)) (Var $ "var" ++ (show n2)) 
   in return $ (Bi Eq (Var $ "var" ++ (show n3))) x

-- cex2mzn (ABS (FDVar n1) (FDVar n2)) = ...

cex2mzn (FUNC f _ v1 v2) = return $ Bi Eq (Var $ "var" ++ (show $ unFDVar v1)) 
    (Bi Times (fun2mzn f) (Var $ "var" ++ (show $ unFDVar v2))) 

----------------------------------------------------------------

anyexp2mzn :: Int -> MZ.Expr
anyexp2mzn x = IConst x

intExpr :: MZ.Expr
intExpr = undefined -- Call Domain [] -- Interval (IConst min) (IConst max)

fun2mzn :: (Int -> Int) -> MZ.Expr
fun2mzn = undefined

--------------------------------------------------------------------------

