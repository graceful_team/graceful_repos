{-
 - ModCP main.
 -
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Paul Torrini, Tom Schrijvers
 -      
 -      Part of the development is a refactoring of monadiccp
 -      by Tom Schrijvers and Peter Wuille 
 -}

--------------------------------------------------------------------------------

-- needed: cabal install mtl

-- Compiles with GHC 7.10 using either the make mode or the interactive mode:

-- ghc -o Main Main.hs

-- ghci
-- :load Main.hs 

-- test examples are in the Test directory

-- to run test examples (eg my_example) in interactive mode: 
-- my_example [args]

-------------------------------------------------------------------------------

module Main where 

import Control.Monad
import Control.Monad.Trans.Cont
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Writer
import Control.Monad.Trans.State

import Data.Monoid

import Data.Expr.Sugar
import Data.Expr.Data

import Control.Mixin.Mixin
import Control.CP.Debug
import Control.CP.FD.Decompose
import Control.CP.FD.Graph
import Control.CP.FD.Model
import Control.CP.Solver
import Control.CP.PriorityQueue
import Control.CP.Queue

-- search model
import Control.CP.FMSearchTree

-- search model syntax
import Control.CP.FMFunctorTree

-- evaluation by effect handlers
import Control.CP.FMTransformers

-- labeling functions
import Control.CP.EnumTerm

-- Overton FD variable domains
import Control.CP.FD.OvertonFD.Domain as Domain

import Control.CP.FD.FD
import Control.CP.FD.Model
import Control.CP.FD.Graph
import Control.CP.FD.SimpleFD

-- Overton FD solver
import Control.CP.FD.OvertonFD.OvertonFD

-- Overton solver wrapped in a writer
--import Control.CP.FD.OvertonFD.OvertonWT

-- high-level syntax
import Control.CP.FD.OvertonFD.UserSyntax
import Control.CP.FD.OvertonFD.US2MZ

import Control.CP.FD.OvertonFD.Sugar
import Control.CP.FD.Interface
import Control.CP.FD.Example

-- files with test cases
import Test.FMTest
import Test.FMTest1
import Test.FMTest2

-- replaces dependency on CPL-backend
import Test.MZTypes

-- replaces main in Control.Search.Generator
main = putStrLn "This is a test..."

