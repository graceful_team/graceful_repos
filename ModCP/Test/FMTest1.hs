{-
 -     Testing examples      
 -
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Paul Torrini
 - 
 -}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Test.FMTest1 (
   runExp1,
   debugExp1,
   runExp2,
   debugExp2,  
   runExp3,
   debugExp3,  
   runExp4,
   debugExp4,
   runExp5,
   debugExp5,
   runExp6,
   debugExp6,
   runExp7,
   debugExp7,
   runExp8,
   debugExp8,
   runExp9,
   debugExp9,
   runExp10,
   debugExp10,
   runExp11,
   debugExp11,         
   runExp12,
   debugExp12         
) where 

import Control.CP.FD.Example
import Control.CP.FD.Model
import Data.Expr.Data
import Control.CP.FMSearchTree
import Control.CP.FMFunctorTree
import Control.CP.Solver
import Control.CP.EnumTerm
import Control.CP.FD.OvertonFD.OvertonFD
import qualified Control.CP.FD.OvertonFD.OvertonFD as OvertonFD
import Control.CP.FD.OvertonFD.Domain 
import Data.List (tails)
import Test.FMTest

import qualified Data.IntSet as IntSet

import Control.CP.Debug
import Debug.Trace

{-
traceM :: (Monad m) => String -> m ()
traceM string = trace string $ return ()

traceMB :: (Monad m) => String -> m Bool
traceMB string = trace string $ return True

traceDomain :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
traceDomain ls = traceMB (concat [lookupAndShow x ++ " ;; " | x <- ls]) 
-}

------------------------------------------------------------------------

{-
allinOV :: [FDVar] -> Int -> Int -> Free (CP OvertonFD FDVar) Bool 
allinOV ls x y = 
    exists (\xq ->
    exists (\yq ->  
     addTo (OHasValue xq x)
     (addTo (OHasValue yq y)             
     (multiAdd [ OLess xq q | q <- ls ] >> multiAdd [ OLess q yq | q <- ls ]))))
-}

{-
alldiffOVu :: [FDVar] -> Free (CP OvertonFD FDVar) () 
alldiffOVu ls = multiAddU [ ODiff qi qj | qi:qls <- tails ls, qj <- qls ]

alldiffOV :: [FDVar] -> Free (CP OvertonFD FDVar) Bool 
alldiffOV ls = multiAdd [ ODiff qi qj | qi:qls <- tails ls, qj <- qls ]

allinOV :: [FDVar] -> Int -> Int -> Free (CP OvertonFD FDVar) Bool 
allinOV ls x y = multiAdd [ OInDomain q (Range x y) | q <- ls ]
-}

runExp1 = solveBasic (experiment1 False)
debugExp1 = solveBasic (experiment1 True)

experiment1 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment1 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 1 5 >>
          alldiffOV [v1,v2] >> -- superfluous (diff cannot be 0)
          (addTo (OSub v1 v2 v3) 
          (addTo (OHasNotValue v3 1) (return ()))) >> 
          dynLabellingT dd [v1,v2,v3] [v1,v2,v3])))
-- OK
-- [[3,1,2],[4,1,3],[4,2,2],[5,1,4],[5,2,3],[5,3,2]]

-- changed 1 so that interval is 0-5 rather than 1-5 
runExp2 = solveBasic (experiment2 False)
debugExp2 = solveBasic (experiment2 True)

experiment2 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment2 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 0 5 >>   -- 1 5 >>
          alldiffOV [v1,v2] >>
          (addTo (OSub v1 v2 v3) 
          (addTo (OHasNotValue v3 1) (return ()))) >> 
          dynLabellingT dd [v1,v2,v3] [v1,v2,v3])))
-- OK

-- removed alldiffOV from 2
runExp3 = solveBasic (experiment3 False)
debugExp3 = solveBasic (experiment3 True)

experiment3 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment3 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 0 5 >>
          (addTo (OSub v1 v2 v3) 
          (addTo (OHasNotValue v3 1) (return ()))) >> 
          dynLabellingT dd [v1,v2,v3] [v1,v2,v3])))
-- OK
--[[0,0,0],[1,1,0],[2,0,2],[2,1,0],[2,1,2],[2,2,0],[3,0,3],[3,1,2],[3,2,0],[3,2,2],[3,2,3],[3,3,0],[4,0,4],[4,1,3],[4,2,2],[4,3,0],[4,3,2],[4,3,3],[4,3,4],[4,4,0],[5,0,5],[5,1,4],[5,2,3],[5,3,2],[5,4,0],[5,4,2],[5,4,3],[5,4,4],[5,4,5],[5,5,0]]

-- replaced alldiff with ODiff in 2 - same as 2
runExp4 = solveBasic (experiment2 False)
debugExp4 = solveBasic (experiment2 True)

experiment4 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment4 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 0 5 >>
          (addTo (ODiff v1 v2) 
          (addTo (OSub v1 v2 v3) 
          (addTo (OHasNotValue v3 1) (return ())))) >> 
          dynLabellingT dd [v1,v2,v3] [v1,v2,v3])))
-- OK

-- removed Hasnotvalue from 2
runExp5 = solveBasic (experiment5 False)
debugExp5 = solveBasic (experiment5 True)

experiment5 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment5 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 0 5 >>
          alldiffOV [v1,v2] >>
          (addTo (OSub v1 v2 v3) (return ())) >> 
          dynLabellingT dd [v1,v2,v3] [v1,v2,v3])))
-- OK

-- revoved alldiff from 5
runExp6 = solveBasic (experiment6 False)
debugExp6 = solveBasic (experiment6 True)

experiment6 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment6 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 0 5 >>
          (addTo (OSub v1 v2 v3) (return ())) >> 
          dynLabellingT dd [v1,v2,v3] [v1,v2,v3])))
-- OK
-- [[0,0,0],[1,0,1],[1,1,0],[2,0,2],[2,1,1],[2,2,0],[3,0,3],[3,1,2],[3,2,1],[3,3,0],[4,0,4],[4,1,3],[4,2,2],[4,3,1],[4,4,0],[5,0,5],[5,1,4],[5,2,3],[5,3,2],[5,4,1],[5,5,0]]

-- added to 2 'noteq v2 0' 
runExp7 = solveBasic (experiment7 False)
debugExp7 = solveBasic (experiment7 True)

experiment7 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment7 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 0 5 >>  
          alldiffOV [v1,v2] >>
          (addTo (OHasNotValue v2 0) 
          (addTo (OSub v1 v2 v3) 
          (addTo (OHasNotValue v3 1) (return ())))) >> 
          dynLabellingT dd [v1,v2,v3] [v1,v2,v3])))
-- OK

-- does not use arithmetics
runExp8 = solveBasic (experiment8 False)
debugExp8 = solveBasic (experiment8 True)

experiment8 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment8 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          allinOV [v1,v2] 0 5 >>
          allinOV [v3,v4] 2 7 >>  
          (addTo (OSame v1 v3) 
          (addTo (ODiff v2 v4) 
          (addTo (OHasNotValue v2 5)
          (addTo (OHasNotValue v3 5) 
          (addTo (OHasValue v4 7)
          (addTo (ODiff v3 v4) (return ()))))))) >> 
          dynLabellingT dd [v1,v2,v3,v4] [v1,v2,v3,v4]))))
-- OK
-- [[2,0,2,7],[2,1,2,7],[2,2,2,7],[2,3,2,7],[2,4,2,7],[3,0,3,7],[3,1,3,7],[3,2,3,7],[3,3,3,7],[3,4,3,7],[4,0,4,7],[4,1,4,7],[4,2,4,7],[4,3,4,7],[4,4,4,7]]


runExp9 = solveBasic (experiment9 False)
debugExp9 = solveBasic (experiment9 True)

experiment9 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment9 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          exists (\v5 -> 
          allinOV [v1,v2] 0 5 >>
          allinOV [v3,v4,v5] 2 9 >>   -- 2 7 >>
          (addTo (OLess v5 v4)      
          (addTo (OLess v1 v5)      
          (addTo (OSame v1 v3) 
          (addTo (ODiff v2 v4) 
          (addTo (OHasNotValue v2 5)
          (addTo (OHasNotValue v3 5) 
          (addTo (OHasValue v4 7)
          (addTo (ODiff v3 v4) (return ()))))))))) >> 
          dynLabellingT dd [v1,v2,v3,v4,v5] [v1,v2,v3,v4,v5])))))
-- OK
-- [[2,0,2,7,3],[2,0,2,7,4],[2,0,2,7,5],[2,0,2,7,6],[2,1,2,7,3],[2,1,2,7,4],[2,1,2,7,5],[2,1,2,7,6],[2,2,2,7,3],[2,2,2,7,4],[2,2,2,7,5],[2,2,2,7,6],[2,3,2,7,3],[2,3,2,7,4],[2,3,2,7,5],[2,3,2,7,6],[2,4,2,7,3],[2,4,2,7,4],[2,4,2,7,5],[2,4,2,7,6],[3,0,3,7,4],[3,0,3,7,5],[3,0,3,7,6],[3,1,3,7,4],[3,1,3,7,5],[3,1,3,7,6],[3,2,3,7,4],[3,2,3,7,5],[3,2,3,7,6],[3,3,3,7,4],[3,3,3,7,5],[3,3,3,7,6],[3,4,3,7,4],[3,4,3,7,5],[3,4,3,7,6],[4,0,4,7,5],[4,0,4,7,6],[4,1,4,7,5],[4,1,4,7,6],[4,2,4,7,5],[4,2,4,7,6],[4,3,4,7,5],[4,3,4,7,6],[4,4,4,7,5],[4,4,4,7,6]]


runExp10 = solveBasic (experiment10 False)
debugExp10 = solveBasic (experiment10 True)

experiment10 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment10 dd = exists (\v1 -> 
          exists (\v2 -> 
          allinOV [v1] (-10) 10 >>   
          allinOV [v2] (-5) 2 >>
          (addTo (OAbs v2 v1) 
          (addTo (OAbs v1 v2) (return ()))) >> 
          dynLabellingT dd [v1,v2] [v1,v2]))
-- OK

runExp11 = solveBasic (experiment11 False)
debugExp11 = solveBasic (experiment11 True)

experiment11 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment11 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          exists (\v5 ->
          allinOV [v5] (-5) 1 >>
          allinOV [v2] (-10) 10 >> 
          allinOV [v1] 0 5 >>
          allinOV [v3,v4] 2 7 >>  
          (addTo (OAbs v5 v2)
          (addTo (OSame v1 v3) 
          (addTo (ODiff v2 v4) 
          (addTo (OHasNotValue v2 5)
          (addTo (OHasNotValue v3 5) 
          (addTo (OHasValue v4 7)
          (addTo (ODiff v3 v4) (return ())))))))) >> 
          dynLabellingT dd [v1,v2,v3,v4,v5] [v1,v2,v3,v4])))))
-- OK

-----------------------------------------------------------------

runExp12 = solveBasic (experiment12 False)
debugExp12 = solveBasic (experiment12 True)

experiment12 :: Debug -> Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
experiment12 dd = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 1 5 >>
          alldiffOV [v1,v2,v3] >> 
          (addTo (OAdd v1 v2 v3) (return ())) >> 
          dynLabellingT dd [v1,v2,v3] [v1,v2,v3])))



----------------------------------------------------------------

diagonals1 :: [FDVar] -> Free (CP OvertonFD FDVar) Bool 
diagonals1 ls = conjB [ 
   exists (\qm ->
   exists (\qn ->
     addTo (OSub qi qj qm) 
     (addTo (OHasValue qn (i - j)) 
     (addTo (ODiff qm qn) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 

diagonals2 :: [FDVar] -> Free (CP OvertonFD FDVar) Bool 
diagonals2 ls = conjB [ 
   exists (\qm ->
   exists (\qn ->
     addTo (OSub qi qj qm) 
     (addTo (OHasValue qn (j - i)) 
     (addTo (ODiff qm qn) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 

diagonals :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals ls d = conjB [ 
   exists (\qm ->
     addTo (OInDomain qm d)  
     (addTo (OSub qi qj qm) 
     (addTo (OHasNotValue qm (i - j)) 
     (addTo (OHasNotValue qm (j - i)) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 


diagonals03 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals03 ls d = conjB [ 
     (addTo (OHasNotValue qi i) (return True)) | 
                  (qi,i) <- (zip ls [1..]) ] 


diagonals04 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals04 ls d = conjB [ 
   exists (\qm -> 
     addTo (OHasValue qm i)  
     (addTo (ODiff qi qm) (return True))) | 
                  (qi,i) <- (zip ls [1..]) ] 

diagonalsBad1 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonalsBad1 ls d = conjB [ 
   exists (\qm -> 
     addTo (OInDomain qm (Set (IntSet.fromList [(-3),(-1),0,1,3])))  
     (addTo (OSub qi qj qm)  
     (addTo (OHasNotValue qm 2) (return True)))) | 
            qi:qls <- tails ls, qj <- qls ] 




diagonalsBad2 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonalsBad2 ls d = conjB [ 
   exists (\qm -> 
   (exists (\qj ->
     addTo (OHasValue qj 1) 
     (addTo (OInDomain qm (Set (IntSet.fromList [1,3])))  
     (addTo (OSub qi qj qm)  
     (addTo (OHasNotValue qm 2) (return True))))))) | 
            qi <- ls ] 


nqueensA :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensA ls = allinOV ls 1 4 >> alldiffOV ls >> diagonals ls (Range (-4) 4) 
       >> trace (concat [lookupAndShow x ++ " ;; " | x <- []]) (alldiffOV ls)

nqueens :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueens ls = allinOV ls 1 4  >> traceDomain ls >> alldiffOV ls >> traceDomain ls    >> diagonals ls (Range (-4) 4) >> traceDomain ls
-- >> (alldiffOVx ls)


nqueensX :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensX ls = allinOV ls 1 4 >> alldiffOV ls -- >> diagonals ls 


nqueensY :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensY qs@(a:b:ls) = allinOV qs 1 4 >> alldiffOV qs >> 
   (exists (\qm ->
      addTo (OInDomain qm (Range 0 4))
      (addTo (OSub a b qm) 
      (addTo (OHasValue qm 1) (return True)))))


nqueensZ :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensZ qs@(a:b:ls) = allinOV ls 1 4 >> alldiffOV qs >> 
   (exists (\qm ->
      addTo (OSub a b qm) 
      (addTo (OHasNotValue qm 1) (return True))))


nqueensW :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensW ls = allinOV ls 1 4 >> alldiffOV ls >> 
   conjB [ exists (\qm ->
      addTo (OSub qi qj qm) 
      (addTo (OHasNotValue qm 1) (return True))) |
                           qi:qls <- tails ls, qj <- qls ] 



{-
queensDynX :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDynX = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in
          allinOV ls 1 4 >>
          (addTo (ODiff v1 v2) 
          (addTo (ODiff v1 v3)
          (addTo (ODiff v1 v4)
          (addTo (ODiff v2 v3)
          (addTo (ODiff v2 v4)
          (addTo (ODiff v3 v4)
          (return())))))))        
          >> dynLabelling ls ls))))
-}


--queens :: Free (CP OvertonFD FDVar) a -> Free (CP OvertonFD FDVar) a
queens :: Free (CP OvertonFD FDVar) ()
queens = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueens ls >> enumerateOV ls [1..4])))) 
--          alldiffOV ls >> diagonals1 ls >> diagonals2 ls)))))

queensDyn :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDyn = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueens ls >> dynLabelling ls ls))))


subExper1 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
subExper1 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 1 5 >>
          alldiffOV [v1,v2] >>
          (addTo (OSub v1 v2 v3) 
          (addTo (OHasNotValue v3 1) (return ()))) >> 
          dynLabelling [v1,v2,v3] [v1,v2,v3])))

subExper2 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
subExper2 = exists (\v1 -> 
          allinOV [v1] 1 5 >>
          (addTo (OHasNotValue v1 3) (return ())) >> 
          dynLabelling [v1] [v1])


runSubExper = print $ solve subExper1

runQueensAnsSt = print $ solve queens

runQueensAns = print $ solve queensDyn


-- TermBaseType OvertonFD FDVar = Int (from OvertonFD.hs)
queensAns :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensAns = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueens ls >> enumerateDyn ls >> assignments ls)))) 

queensAns0 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensAns0 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueens ls >> enumerateOV ls [1..4] >> assignments ls)))) 


{-
enumerateOV :: [FDVar] -> [Int] -> Free (CP OvertonFD FDVar) Bool
enumerateOV vars values = conjB [ enumOV var values | var <- vars ] 

-- essentially, gives 
-- disj(x1,x2,...|v) = (v=x1) \/ (v=x2) \/ ... \/ false :: 
--                                           Free ((CP OvertonFD FDVar) Bool
-- where a1 \/ a2 = Op (Try a1 a2)
enumOV :: FDVar -> [Int] -> Free (CP OvertonFD FDVar) Bool
enumOV var values = disjB $ multiConstr [ OHasValue var val | val <- values ] 

-- Dynamic
propagate :: [FDVar] -> OvertonFD (Free (CP OvertonFD FDVar) Bool)
propagate [] = return (return True)
propagate (v:vs) = do d <- fd_domain v -- reads the current constraint store
                      return $ enumOV v d >> enumerateDyn vs  

-- gives
--  label (return ((disj(d1|v1) /\ label(return (disj(d2|v2) /\ ... /\ label (return true)...))))) 
enumerateDyn :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
enumerateDyn = label . propagate 
-}

------------------------------------------------------------

-- model :: ExampleModel FDVar ModelInt
{-
model n = exists $ \p -> do
  size p @= n
  p `allin` (cte 0,n-1)
  allDiff p
  loopall (cte 0,n-2) $ \i -> 
    loopall (i+1,n-1) $ \j ->
      noattack i j (p!i) (p!j)
  return p
-}
-- main = example_sat_main_single_expr model

{-
main = example_main_void model

model :: ConcreteExModel ()
model _ = exists $ \arr -> do
  arr `allin` (cte 0,cte 10)
  size arr @= 4
  xsum arr @= 10
  xsum (xmap (\x -> x*x) arr) @= 30
  sorted arr
  return arr

runEx = example_sat_main_void model EV
-}

