{-
 -     Testing examples with high-level syntax     
 -
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Paul Torrini
 - 
 -}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Test.FMTest2 (
   transExp0,
   runAExp0,
   runAExp1,
   runAExp2,  
   runAExp3,
   runAExp4,
   runAExp5,
   runAExp6,
   runAExp7,
   runAExp8,
   runAExp9,
   runAExp10,
   runAExp11,         
   runAExp12,
   runAExp13,
   runAExp14,
   runQueensAns,
   knapsackRun,
   runAnsX8,
   runAnsY8,
   runGrMod1,         
   runGrMod1F,         
   runGrMod2,
   experiment1,
   queensDyn,
   knapsackMod,
   grMod1,
   grMod1F,         
   solveBasicA,
   solveBasicN,
   solveBasicPA,
   solveBasicPN,
   writeBasic
) where 

import Control.CP.FD.Example
import Control.CP.FD.Model
import Data.Expr.Data
import Control.CP.FMSearchTree
import Control.CP.FMFunctorTree
import Control.CP.Solver
import Control.CP.EnumTerm
import Control.CP.FD.OvertonFD.OvertonFD
import qualified Control.CP.FD.OvertonFD.OvertonFD as OvertonFD
import Control.CP.FD.OvertonFD.Domain 
import Data.List (tails)
import Test.FMTest
import Data.List

import Control.CP.FD.OvertonFD.UserSyntax 
import Control.CP.FD.OvertonFD.US2MZ 

import qualified Data.IntSet as IntSet

import Control.CP.Debug
import Debug.Trace


infixr 3 /\.
(/\.) :: CEx v e -> CEx v e -> CEx v e
(/\.) = (AND)


infixr 3 \/.
(\/.) :: CEx v e -> CEx v e -> CEx v e
(\/.) = (OR)

{-
traceM :: (Monad m) => String -> m ()
traceM string = trace string $ return ()

traceMB :: (Monad m) => String -> m Bool
traceMB string = trace string $ return True

traceDomain :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
traceDomain ls = traceMB (concat [lookupAndShow x ++ " ;; " | x <- ls]) 
-}

------------------------------------------------------------------------

-- to redirect output: 
-- writeFile "myoutput.txt" 
--       (show $ solve (tFEExpr2model $ desugarT grMod1))

writeBasic :: FilePath -> TExpr FDVar Int -> IO ()
writeBasic fName mod = writeFile fName 
        (show $ solve (tFEExpr2model $ desugarT mod))

solveBasicN :: (Int -> TExpr FDVar Int) -> Int -> [OvAnswer]
solveBasicN ex n = solve (tFEExpr2model $ desugarT $ ex n) 

solveBasicA :: TExpr FDVar Int -> [OvAnswer] 
solveBasicA ex = solve (tFEExpr2model $ desugarT ex) 

solveBasicPN :: (Int -> TExpr FDVar Int) -> Int -> IO ()
solveBasicPN ex n = solveBasic (tFEExpr2model $ desugarT $ ex n) 

solveBasicPA :: TExpr FDVar Int -> IO ()
solveBasicPA ex = solveBasic (tFEExpr2model $ desugarT ex) 

solveBasicOv :: (Debug -> TExpr FDVar Int) -> Debug -> IO ()
solveBasicOv ex b = solveBasic (tFEExpr2model $ desugarT $ ex b) 

---------------------------------------------------------------------------

transExp0 = runText2mzn experiment0
runAExp0 = solveBasicA experiment0

experiment0 :: TExpr FDVar Int
experiment0 = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (AND (IN_INTERVAL v1 1 5)
          (AND (IN_INTERVAL v2 1 5)
               (SUB v1 v2 v3))))


runAExp1 = solveBasicOv experiment1 False
debugAExp1 = solveBasicOv experiment1 True

experiment1 :: Debug -> TExpr FDVar Int
experiment1 dd = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
            ALL_IN_INTERVAL [v1,v2,v3] 1 5,
            ALL_DIFFERENT [v1,v2], -- superfluous (diff cannot be 0)
            SUB v1 v2 v3, 
            DIFFER_VAL v3 1 ]))
-- OK
-- [[3,1,2],[4,1,3],[4,2,2],[5,1,4],[5,2,3],[5,3,2]]

 
runAExp2 = solveBasicOv experiment2 False
debugAExp2 = solveBasicOv experiment2 True

experiment2 :: Debug -> TExpr FDVar Int 
experiment2 dd = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
            ALL_IN_INTERVAL [v1,v2,v3] 0 5,   
            ALL_DIFFERENT [v1,v2], 
            SUB v1 v2 v3, 
            DIFFER_VAL v3 1 ])) 
-- OK

-- removed alldiffOV from 2
runAExp3 = solveBasicOv experiment3 False
debugAExp3 = solveBasicOv experiment3 True

experiment3 :: Debug -> TExpr FDVar Int 
experiment3 dd = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2,v3] 0 5,
          SUB v1 v2 v3, 
          DIFFER_VAL v3 1 ]))
-- OK

-- replaced alldiff with ODiff in 2 - same as 2
runAExp4 = solveBasicOv experiment4 False
debugAExp4 = solveBasicOv experiment4 True

experiment4 :: Debug -> TExpr FDVar Int 
experiment4 dd = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2,v3] 0 5,
          DIFFER v1 v2, 
          SUB v1 v2 v3, 
          DIFFER_VAL v3 1 ]))
-- OK 

-- removed Hasnotvalue from 2
runAExp5 = solveBasicOv experiment5 False
debugAExp5 = solveBasicOv experiment5 True

experiment5 :: Debug -> TExpr FDVar Int 
experiment5 dd = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2,v3] 0 5,
          ALL_DIFFERENT [v1,v2],
          SUB v1 v2 v3 ]))
-- OK

-- revoved alldiff from 5
runAExp6 = solveBasicOv experiment6 False
debugAExp6 = solveBasicOv experiment6 True

experiment6 :: Debug -> TExpr FDVar Int 
experiment6 dd = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2,v3] 0 5,
          SUB v1 v2 v3 ]))
-- OK

-- added to 2 'noteq v2 0' 
runAExp7 = solveBasicOv experiment7 False
debugAExp7 = solveBasicOv experiment7 True

experiment7 :: Debug -> TExpr FDVar Int 
experiment7 dd = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2,v3] 0 5,
          ALL_DIFFERENT [v1,v2],
          DIFFER_VAL v2 0, 
          SUB v1 v2 v3, 
          DIFFER_VAL v3 1 ]))
-- OK

-- does not use arithmetics
runAExp8 = solveBasicOv experiment8 False
debugAExp8 = solveBasicOv experiment8 True

experiment8 :: Debug -> TExpr FDVar Int 
experiment8 dd = GEXIST 4 (\[v1,v2,v3,v4] ->
   SOLVE_FOR [v1,v2,v3,v4] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2] 0 5,
          ALL_IN_INTERVAL [v3,v4] 2 7,
          SAME v1 v3, 
          DIFFER v2 v4, 
          DIFFER_VAL v2 5,
          DIFFER_VAL v3 5, 
          HAS_VAL v4 7,
          DIFFER v3 v4 ]))
-- OK
-- [[2,0,2,7],[2,1,2,7],[2,2,2,7],[2,3,2,7],[2,4,2,7],[3,0,3,7],[3,1,3,7],[3,2,3,7],[3,3,3,7],[3,4,3,7],[4,0,4,7],[4,1,4,7],[4,2,4,7],[4,3,4,7],[4,4,4,7]]


runAExp9 = solveBasicOv experiment9 False
debugAExp9 = solveBasicOv experiment9 True

experiment9 :: Debug -> TExpr FDVar Int 
experiment9 dd = GEXIST 5 (\[v1,v2,v3,v4,v5] ->
   SOLVE_FOR [v1,v2,v3,v4,v5] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2] 0 5,
          ALL_IN_INTERVAL [v3,v4,v5] 2 9,  
          LESS v5 v4,      
          LESS v1 v5,      
          SAME v1 v3, 
          DIFFER v2 v4, 
          DIFFER_VAL v2 5,
          DIFFER_VAL v3 5, 
          HAS_VAL v4 7,
          DIFFER v3 v4 ]))
-- OK
-- [[2,0,2,7,3],[2,0,2,7,4],[2,0,2,7,5],[2,0,2,7,6],[2,1,2,7,3],[2,1,2,7,4],[2,1,2,7,5],[2,1,2,7,6],[2,2,2,7,3],[2,2,2,7,4],[2,2,2,7,5],[2,2,2,7,6],[2,3,2,7,3],[2,3,2,7,4],[2,3,2,7,5],[2,3,2,7,6],[2,4,2,7,3],[2,4,2,7,4],[2,4,2,7,5],[2,4,2,7,6],[3,0,3,7,4],[3,0,3,7,5],[3,0,3,7,6],[3,1,3,7,4],[3,1,3,7,5],[3,1,3,7,6],[3,2,3,7,4],[3,2,3,7,5],[3,2,3,7,6],[3,3,3,7,4],[3,3,3,7,5],[3,3,3,7,6],[3,4,3,7,4],[3,4,3,7,5],[3,4,3,7,6],[4,0,4,7,5],[4,0,4,7,6],[4,1,4,7,5],[4,1,4,7,6],[4,2,4,7,5],[4,2,4,7,6],[4,3,4,7,5],[4,3,4,7,6],[4,4,4,7,5],[4,4,4,7,6]]


runAExp10 = solveBasicOv experiment10 False
debugAExp10 = solveBasicOv experiment10 True

experiment10 :: Debug -> TExpr FDVar Int 
experiment10 dd = GEXIST 2 (\[v1,v2] ->
   SOLVE_FOR [v1,v2] 
     (CONJ [ 
          IN_INTERVAL v1 (-10) 10,
          IN_INTERVAL v2 (-5) 2,  
          ABS v2 v1, 
          ABS v1 v2 ]))

-- OK


runAExp11 = solveBasicOv experiment11 False
debugAExp11 = solveBasicOv experiment11 True

experiment11 :: Debug -> TExpr FDVar Int 
experiment11 dd = GEXIST 5 (\[v1,v2,v3,v4,v5] ->
   SOLVE_FOR [v1,v2,v3,v4,v5] 
     (CONJ [ 
          IN_INTERVAL v5 (-5) 1,
          IN_INTERVAL v2 (-10) 10,  
          IN_INTERVAL v1 0 5,  
          ALL_IN_INTERVAL [v3,v4] 2 7,
          ABS v5 v2,
          SAME v1 v3, 
          DIFFER v2 v4, 
          DIFFER_VAL v2 5,
          DIFFER_VAL v3 5, 
          HAS_VAL v4 7,
          DIFFER v3 v4 ]))
-- OK



runAExp12 = solveBasicOv experiment12 False
debugAExp12 = solveBasicOv experiment12 True

experiment12 :: Debug -> TExpr FDVar Int 
experiment12 dd = GEXIST 3 (\[v1,v2,v3] ->
   SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2,v3] 1 5,
          ALL_DIFFERENT [v1,v2,v3],
          ADD v1 v2 v3 ]))
-- OK



runAExp13 = solveBasicOv experiment13 False
debugAExp13 = solveBasicOv experiment13 True

experiment13 :: Debug -> TExpr FDVar Int 
experiment13 dd = GEXIST 3 ( \[v1,v2,v3 ] -> SOLVE_FOR [v1,v2,v3] 
     (CONJ [ 
          ALL_IN_INTERVAL [v1,v2] (-3) 3,  
          MULT v1 v2 v3 ]))
-- OK


runAExp14 = solveBasicOv experiment14 False
debugAExp14 = solveBasicOv experiment14 True

experiment14 :: Debug -> TExpr FDVar Int 
experiment14 dd = GEXIST 2 (\[v1,v2] ->
   SOLVE_FOR [v1,v2] 
     (CONJ [ 
          IN_INTERVAL v1 (-7) 2,  
          IN_INTERVAL v2 (-3) 2,  
          ABS v1 v2 ]))
-- OK

-----------------------------------------------------------------

modelAnsX :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
modelAnsX = exists (\v1 ->  
          exists (\v2 ->
            ((addC (OIsLess v1 7)) \/ (addC (OIsGreater v2 7)))
             >> ((addC (OIsGreater v1 7)) \/ (addC (OIsLess v2 7))) 
             >> (addTo (OIsGreater v1 2)  
             (addTo (OIsGreater v2 2)
             (addTo (OIsLess v1 12) 
             (addTo (OIsLess v2 12)
             (return ()))))) >> 
                     dynLabelling [v1,v2] [v1,v2]))
--              enumerateDyn [va,v1,vb,v2,vc] >> assignments [v1,v2])))))

runAnsX8 = solveBasic modelAnsX


-- modelAnsX translated to User Syntax
modelAnsY ::  Debug -> TExpr FDVar Int
modelAnsY dd = GEXIST 2  (\[v1,v2] -> 
                 SOLVE_FOR [v1,v2]
             (((LESS_VAL v1 7) \/. (GREATER_VAL v2 7))
             /\. (((GREATER_VAL v1 7) \/. (LESS_VAL v2 7)) 
             /\. ((GREATER_VAL v1 2)  
             /\. ((GREATER_VAL v2 2)
             /\. ((LESS_VAL v1 12) 
             /\. (LESS_VAL v2 12)))))))

runAnsY8 = solveBasicOv modelAnsY True


----------------------------------------------------------------

-- N QUEENS

diagonalsX :: [FDVar] -> Int -> Int -> CEx FDVar Int 
diagonalsX ls n1 n2 = CONJ [ 
   EXISTS (\qm ->
     (IN_INTERVAL qm n1 n2) /\.  
     ((SUB qi qj qm) /\.
     ((DIFFER_VAL qm (i - j)) /\. 
     ((DIFFER_VAL qm (j - i)))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ]


nqueensX :: Int -> [FDVar] -> CEx FDVar Int
nqueensX n ls = ALL_IN_INTERVAL ls 1 n /\. 
               (ALL_DIFFERENT ls /\.
                diagonalsX ls (-n) n) 


queensDyn :: Int -> TExpr FDVar Int
queensDyn n = GEXIST n (\ls -> SOLVE_FOR ls $ nqueensX n ls) 


runQueensAns :: Int -> IO ()
runQueensAns n = solveBasicPN queensDyn n

----------------------------------------------------------------

-- KNAPSACK

is_true :: Bool -> Int
is_true b = if b then 1 else 0


knapsackG :: Int -> Int -> [FDVar] -> [Int] -> CEx FDVar Int
knapsackG n w vars vals = 
      IF_THEN_ELSE (is_true $ w<0) FALSE
      (IF_THEN_ELSE (is_true $ w==0) (ALL_HAS_VAL vars 0)
               (DISJ [HAS_VAL vr vl 
                      /\. (knapsackG (n - 1) (w - vl) 
                                     (delete vr vars)
                                     (delete vl vals))
                                             | (vr, vl) <- zip vars vals])) 


knapsackF :: Int -> Int -> [FDVar] -> [Int] -> CEx FDVar Int
knapsackF n w vars vals = 
      IF_THEN_ELSE (if w<0 then 0 else (-1)) FALSE
      (IF_THEN_ELSE (if w==0 then 0 else (-1)) (ALL_HAS_VAL vars 0)
               (DISJ [HAS_VAL vr vl 
                      /\. (knapsackF (n - 1) (w - vl) 
                                     (delete vr vars)
                                     (delete vl vals))
                                             | (vr, vl) <- zip vars vals])) 

-- with User Syntax
knapsackMod :: Int -> [Int] -> TExpr FDVar Int
knapsackMod w vals = let n = length vals 
      in GEXIST n (\vars -> SOLVE_FOR vars $ knapsackG n w vars vals)

-- to run the model:
knapsackRun :: Int -> [Int] -> IO ()
knapsackRun n vals = 
     solveNub (tFEExpr2model $ desugarT $ knapsackMod n vals) 

-- eliminate duplicate solutions
solveNub :: (Eq a, Show a, Solver solver, Term solver t) 
      => Tree solver t [a] -> IO ()
solveNub cmod = print $ nub (solve cmod)



-- superseded, with search model syntax 
knapsackD :: Int -> [Int] -> 
         Free (CP OvertonFD FDVar) Bool 
knapsackD w vs | w < 0 = false
               | w == 0 = Return True
               | w > 0 = do v <- (foldr (\/) false . map Return) vs
                            exists (\x1 -> addTo (OHasValue x1 v)
                                                 (knapsackD (w - v) vs))
                 

knapsackDynX :: Int -> [Int] -> 
         Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
knapsackDynX w vs | w < 0 = false
                 | w == 0 = Return []
                 | w > 0 = do v <- (foldr (\/) false . map Return) vs
                              vs' <- knapsackDynX (w - v) vs
                              return (v:vs') 
                 

--------------------------------------------------------------------

-- Barecelona Meeting example

-- hack to get first solution only
runGrMod1F = solveBasicA grMod1F
 
grMod1F ::  TExpr FDVar Int 
grMod1F = GEXIST 5 (\[park,pavedP,greenP,distantP,cost] -> 
     ANN_SOLVE_FOR FIRST [park,pavedP,greenP,distantP,cost]
                   (topMod1 park pavedP greenP distantP cost)) 


-- run for all solutions
runGrMod1 = solveBasicA grMod1 

grMod1 ::  TExpr FDVar Int 
grMod1 = GEXIST 5 (\[park,pavedP,greenP,distantP,cost] -> 
     SOLVE_FOR [park,pavedP,greenP,distantP,cost]
                   (topMod1 park pavedP greenP distantP cost)) 

linFUNC :: Int -> Int -> FDVar -> FDVar -> CEx FDVar Int 
linFUNC t n = FUNC (\x -> x * n) [0..(t*n)]
-- linFUNC t n = FUNC (\x -> x * n) [0..(t*(n+1))]

topMod1 :: FDVar -> FDVar -> FDVar -> FDVar -> FDVar -> CEx FDVar Int 
topMod1 park pavedP greenP distantP cost = 
        let t = 100                                   -- triangle surface
            s = 60                                    -- square surface
            pneed = 60                                -- needed parking
            budget = 15000                            -- budget
            storageNeed = 5000                   -- minimum needed storage
         in EXIST 10 ( \[sp1,sp2,sp3,c1,c2,c3,c4,s1,s2,s3] -> CONJ [ 
              HAS_VAL sp1 t,                            -- triangle surface
              ALL_IN_INTERVAL [park,pavedP,greenP] 0 t, -- amounts in t
              IN_INTERVAL distantP 0 s,             -- amount of parking in s
              ALL_IN_INTERVAL [c1,c2,c3,c4,cost] 0 100000,    -- costs
              ALL_IN_INTERVAL [s1,s2,s3] 0 100000,   -- storage capacities
              SUM [park, pavedP, greenP] sp1,        -- use the whole triangle
              SUM [pavedP, greenP, distantP] sp2, 
              GREATER_EQ_VAL sp2 pneed, 
              linFUNC t 120 park c1,   -- linear for simpl.
              linFUNC t 50 pavedP c2,                      
              linFUNC t 200 greenP c3,      
              linFUNC t 80 distantP c4,                      
              SUM [c1, c2, c3, c4] cost,
              LESS_EQ_VAL cost budget,  
              linFUNC t 100 park s1,   -- linear for simpl.
              linFUNC t 10 pavedP s2,                  
              linFUNC t 50 greenP s3,                
              SUM [s1, s2, s3] sp3,
              GREATER_EQ_VAL sp3 storageNeed ]
              /\. (HAS_VAL pavedP 0 \/. HAS_VAL greenP 0))
                                     -- don't mix paved and green parking


-- run for all solutions
runGrMod2 = solveBasicOv grMod2 False

grMod2 ::  Debug -> TExpr FDVar Int 
grMod2 dd = GEXIST 2 (\[park,pavedP] -> 
     SOLVE_FOR [park,pavedP]
                   (topMod2 park pavedP)) 

topMod2 :: FDVar -> FDVar -> CEx FDVar Int 
topMod2 park pavedP = 
         EXIST 3 ( \[s1,s2,s3] -> CONJ [ 
              ALL_IN_INTERVAL [park,pavedP] 0 10, 
              ALL_IN_INTERVAL [s1,s2] 0 10000,        
              linFUNC 10 5 park s1, 
              linFUNC 10 7 pavedP s2,
              SUM [s1,s2] s3,
              LESS_EQ_VAL s3 49 ] )                                          
-- OK
-- [[0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[1,0],[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[2,0],[2,1],[2,2],[2,3],[2,4],[2,5],[3,0],[3,1],[3,2],[3,3],[3,4],[4,0],[4,1],[4,2],[4,3],[4,4],[5,0],[5,1],[5,2],[5,3],[6,0],[6,1],[6,2],[7,0],[7,1],[7,2],[8,0],[8,1],[9,0]]


{-
-- run for all solutions
runGrMod3 = solveBasicOv grMod3 False

grMod3 ::  Debug -> TExpr FDVar Int 
grMod3 dd = GEXIST 15 (\[park,pavedP,greenP,distantP,cost,
                        sp1,sp2,sp3,c1,c2,c3,c4,s1,s2,s3] -> 
     SOLVE_WITH_FOR [park,pavedP,greenP,distantP,cost,
                sp1,sp2,sp3,c1,c2,c3,c4,s1,s2,s3]
                    [park,pavedP,greenP,distantP,cost]
                   (topMod3 park pavedP greenP distantP cost  
                            sp1 sp2 sp3 c1 c2 c3 c4 s1 s2 s3)) 

-- topMod2 :: FDVar -> FDVar -> FDVar -> FDVar -> FDVar -> CEx FDVar Int 
topMod3 park pavedP greenP distantP cost 
           sp1 sp2 sp3 c1 c2 c3 c4 s1 s2 s3 = 
        let t = 100                                     -- triangle surface
            s = 60                                      -- square surface
            pneed = 60                                  -- needed parking
            budget = 15000                              -- budget
            storageNeed = 5000                     -- minimum needed storage
         in -- EXIST 10 ( \[sp1,sp2,sp3,c1,c2,c3,c4,s1,s2,s3] -> 
            CONJ [ 
              HAS_VAL sp1 t,                            -- triangle surface
              ALL_IN_INTERVAL [park,pavedP,greenP] 0 t, -- amounts in t
              IN_INTERVAL distantP 0 s,             -- amount of parking in s
              ALL_IN_INTERVAL [c1,c2,c3,c4,cost] 0 100000,         -- costs
              ALL_IN_INTERVAL [s1,s2,s3] 0 100000,        -- storage capacities
              SUM [park, pavedP, greenP] sp1,         -- use the whole triangle
              SUM [pavedP, greenP, distantP] sp2, 
              GREATER_EQ_VAL sp2 pneed, 
              linFUNC t 120 park c1, -- linear for simpl.
              linFUNC t 50 pavedP c2,                      
              linFUNC t 200 greenP c3,                      
              linFUNC t 80 distantP c4,                      
--              FUNC (\x -> x * 120) [0..(t*120)] park c1, -- linear for simpl.
--              FUNC (\x -> x * 50) [0..(t*50)] pavedP c2,                      
--              FUNC (\x -> x * 200) [0..(t*200)] greenP c3,               
--              FUNC (\x -> x * 80) [0..(t*80)] distantP c4,                
              SUM [c1, c2, c3, c4] cost,
              LESS_EQ_VAL cost budget,  
              linFUNC t 100 park s1, -- linear for simpl.
              linFUNC t 10 pavedP s2,                  
              linFUNC t 50 greenP s3,                
--              FUNC (\x -> x * 100) [0..(t*100)] park s1, -- linear for simpl.
--              FUNC (\x -> x * 10) [0..(t*10)] pavedP s2,                      
--              FUNC (\x -> x * 50) [0..(t*50)] greenP s3,                      
              SUM [s1, s2, s3] sp3,
              LESS_EQ_VAL sp3 storageNeed ]
                 /\. (HAS_VAL pavedP 0 \/. HAS_VAL greenP 0)
                                    -- don't mix paved and green parking

topMod1 :: FDVar -> FDVar -> FDVar -> FDVar -> FDVar -> CEx FDVar Int 
topMod1 park pavedP greenP distantP cost = 
        let t = 100                                   -- triangle surface
            s = 60                                    -- square surface
            pneed = 60                                -- needed parking
            budget = 15000                            -- budget
            storageNeed = 5000                   -- minimum needed storage
         in EXIST 10 ( \[sp1,sp2,sp3,c1,c2,c3,c4,s1,s2,s3] -> CONJ [ 
              HAS_VAL sp1 t,                            -- triangle surface
              ALL_IN_INTERVAL [park,pavedP,greenP] 0 t, -- amounts in t
              IN_INTERVAL distantP 0 s,             -- amount of parking in s
              ALL_IN_INTERVAL [c1,c2,c3,c4,cost] 0 100000,    -- costs
              ALL_IN_INTERVAL [s1,s2,s3] 0 100000,   -- storage capacities
              SUM [park, pavedP, greenP] sp1,        -- use the whole triangle
              SUM [pavedP, greenP, distantP] sp2, 
              GREATER_EQ_VAL sp2 pneed, 
              linFUNC t 120 park c1,   -- linear for simpl.
              linFUNC t 50 pavedP c2,                      
              linFUNC t 140 greenP c3, -- with 200 greenP is always 0     
              linFUNC t 80 distantP c4,                      
              SUM [c1, c2, c3, c4] cost,
              LESS_EQ_VAL cost budget,  
              linFUNC t 100 park s1,   -- linear for simpl.
              linFUNC t 10 pavedP s2,                  
              linFUNC t 50 greenP s3,                
              SUM [s1, s2, s3] sp3,
--             GREATER_EQ_VAL sp3 storageNeed ]
--             IN_INTERVAL sp3 storageNeed 6000 ]  -- reduced 
              LESS_EQ_VAL sp3 storageNeed ]           -- wrong condition
              /\. (HAS_VAL pavedP 0 \/. HAS_VAL greenP 0))
                                     -- don't mix paved and green parking
-}

-----------------------------------------------------------------

-- Other test cases (older ones)

diagonals1 :: [FDVar] -> Free (CP OvertonFD FDVar) Bool 
diagonals1 ls = conjB [ 
   exists (\qm ->
   exists (\qn ->
     addTo (OSub qi qj qm) 
     (addTo (OHasValue qn (i - j)) 
     (addTo (ODiff qm qn) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 

diagonals2 :: [FDVar] -> Free (CP OvertonFD FDVar) Bool 
diagonals2 ls = conjB [ 
   exists (\qm ->
   exists (\qn ->
     addTo (OSub qi qj qm) 
     (addTo (OHasValue qn (j - i)) 
     (addTo (ODiff qm qn) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 

diagonals :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals ls d = conjB [ 
   exists (\qm ->
     addTo (OInDomain qm d)  
     (addTo (OSub qi qj qm) 
     (addTo (OHasNotValue qm (i - j)) 
     (addTo (OHasNotValue qm (j - i)) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 


diagonals03 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals03 ls d = conjB [ 
     (addTo (OHasNotValue qi i) (return True)) | 
                  (qi,i) <- (zip ls [1..]) ] 


diagonals04 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals04 ls d = conjB [ 
   exists (\qm -> 
     addTo (OHasValue qm i)  
     (addTo (ODiff qi qm) (return True))) | 
                  (qi,i) <- (zip ls [1..]) ] 

diagonalsBad1 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonalsBad1 ls d = conjB [ 
   exists (\qm -> 
     addTo (OInDomain qm (Set (IntSet.fromList [(-3),(-1),0,1,3])))  
     (addTo (OSub qi qj qm)  
     (addTo (OHasNotValue qm 2) (return True)))) | 
            qi:qls <- tails ls, qj <- qls ] 


diagonalsBad2 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonalsBad2 ls d = conjB [ 
   exists (\qm -> 
   (exists (\qj ->
     addTo (OHasValue qj 1) 
     (addTo (OInDomain qm (Set (IntSet.fromList [1,3])))  
     (addTo (OSub qi qj qm)  
     (addTo (OHasNotValue qm 2) (return True))))))) | 
            qi <- ls ] 


nqueensA :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensA ls = allinOV ls 1 4 >> alldiffOV ls >> diagonals ls (Range (-4) 4) 
       >> trace (concat [lookupAndShow x ++ " ;; " | x <- []]) (alldiffOV ls)

nqueens :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueens ls = allinOV ls 1 4  >> traceDomain ls >> alldiffOV ls >> traceDomain ls    >> diagonals ls (Range (-4) 4) >> traceDomain ls


nqueensK :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensK ls = allinOV ls 1 4 >> alldiffOV ls -- >> diagonals ls 


nqueensY :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensY qs@(a:b:ls) = allinOV qs 1 4 >> alldiffOV qs >> 
   (exists (\qm ->
      addTo (OInDomain qm (Range 0 4))
      (addTo (OSub a b qm) 
      (addTo (OHasValue qm 1) (return True)))))


nqueensZ :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensZ qs@(a:b:ls) = allinOV ls 1 4 >> alldiffOV qs >> 
   (exists (\qm ->
      addTo (OSub a b qm) 
      (addTo (OHasNotValue qm 1) (return True))))


nqueensW :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensW ls = allinOV ls 1 4 >> alldiffOV ls >> 
   conjB [ exists (\qm ->
      addTo (OSub qi qj qm) 
      (addTo (OHasNotValue qm 1) (return True))) |
                           qi:qls <- tails ls, qj <- qls ] 




