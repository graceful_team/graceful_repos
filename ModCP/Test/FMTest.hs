{-
 -     Testing examples      
 -
 - 	Modular Constraint Programming
 -      https://bitbucket.org/graceful_team/graceful_repos
 - 	Paul Torrini
 - 
 -}


{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Test.FMTest (
   runEx0,
   runEx1,  
   runExDB1,  
   runExNB1,
   model5, 
   model6,
   noattack,
   runQueensAns3,
   runQueensAns4,
   runQueensAns5,
   runQueensAns6,
   runQueensAns7,
   runQueensAns8,
   runQueensAnsSt,
   runSubExper,
   runAns7,
   runAns8,
   runAns8DB,
   runAns8NB,
   runAns8DBx,
   runAns8NBx,
   alldiffOV,
   alldiffOVu,
   allinOV,
   knapsackDyn
) where 

import Control.CP.FD.Example
import Control.CP.FD.Model
import Data.Expr.Data
import Control.CP.FMSearchTree
import Control.CP.FMFunctorTree
import Control.CP.Solver
import Control.CP.EnumTerm
import Control.CP.FD.OvertonFD.OvertonFD
import qualified Control.CP.FD.OvertonFD.OvertonFD as OvertonFD
import Control.CP.FD.OvertonFD.Domain 
import Data.List (tails)
import qualified Data.IntSet as IntSet

import Control.CP.Debug
import Debug.Trace

{-
traceM :: (Monad m) => String -> m ()
traceM string = trace string $ return ()

traceMB :: (Monad m) => String -> m Bool
traceMB string = trace string $ return True

traceDomain :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
traceDomain ls = traceMB (concat [lookupAndShow x ++ " ;; " | x <- ls]) 
-}

------------------------------------------------------------------------


model0 :: Free (CP OvertonFD FDVar) Int -- ModelCol
model0 = Op (Add (OHasValue (FDVar 0) 3) 
                 (Return 0))


model1 :: Free (CP OvertonFD FDVar) Int -- ModelCol
model1 =   Op (NewVar (\v -> Op (Add (OHasValue v 3) 
                                (Return 0))))

model2 :: Free (CP OvertonFD FDVar) [Int] 
model2 =   Op (NewVar (\v -> Op (Add (OHasValue v 3) 
                    (Op (Label (fd_domain v >>= return . return))))))

model3 :: Free (CP OvertonFD FDVar) Int 
model3 =   Op (NewVar (\v -> Op (Add (OHasValue v 3) 
               (Op (Label (fd_domain v >>= return . return . head))))))


model4 :: Free (CP OvertonFD FDVar) [Int] 
model4 =   exists (\v -> addTo (OHasValue v 3) 
                  (label (fd_domain v >>= return . return)))


model5 :: Free (CP OvertonFD FDVar) ([Int],[Int]) 
model5 =   exists (\v1 -> 
           exists (\v2 ->  
           exists (\v3 ->
           exists (\v4 ->
             addTo (OHasValue v1 2)
             (addTo (OHasValue v4 8)
             (addTo (OLess v1 v2)  
             (addTo (OLess v2 v3)
             (addTo (OLess v3 v4)
                  (label (
                     fd_domain v2 >>= \v2 -> fd_domain v3 >>= \v3 -> return (return (v2,v3))))))))))))


model6 :: Free (CP OvertonFD FDVar) ([[Int]]) 
model6 =   exists (\v1 -> 
           exists (\v2 ->  
           exists (\v3 ->
           exists (\v4 ->
             addTo (OHasValue v1 2)
             (addTo (OHasValue v4 8)
             (addTo (OLess v1 v2)  
             (addTo (OLess v2 v3)
             (addTo (OLess v3 v4)
                  (label (
                     mapM fd_domain [v2,v3] >>= return . return))))))))))

model7 :: Free (CP OvertonFD FDVar) a -> Free (CP OvertonFD FDVar) a
model7 x = exists (\v1 -> 
           exists (\v2 ->  
           exists (\v3 ->
           exists (\v4 ->
             addTo (OHasValue v1 2)
             (addTo (OHasValue v4 8)
             (addTo (OLess v1 v2)  
             (addTo (OLess v2 v3)
             (addTo (OLess v3 v4) x)))))))) 


model7B :: Free (CP OvertonFD FDVar) ()
model7B = exists (\v1 -> 
           exists (\v2 ->  
           exists (\v3 ->
           exists (\v4 ->
             addTo (OHasValue v1 2)
             (addTo (OHasValue v4 8)
             (addTo (OLess v1 v2)  
             (addTo (OLess v2 v3)
             (addTo (OLess v3 v4) (return ()))))))))) 


modelAns7 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
modelAns7 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
             (addTo (OHasValue v1 2)
             (addTo (OHasValue v4 8)
             (addTo (OLess v1 v2)  
             (addTo (OLess v2 v3)
             (addTo (OLess v3 v4) (return ())))))) >> 
                    enumerateDyn [v1,v2,v3,v4] >> assignments [v2,v3]))))
 

modelAns8 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
modelAns8 = exists (\va -> 
          exists (\v1 ->  
          exists (\vb ->
          exists (\v2 ->
          exists (\vc ->
             (addTo (OHasValue va 2)
             (addTo (OHasValue vb 7)
             (addTo (OHasValue vc 12) 
             ((addC (OLess v1 vb)) \/ (addC (OLess vb v2)))))) -- v1<7 | 7<v2
             >> ((addC (OLess vb v1)) \/ (addC (OLess v2 vb))) -- v2<7 | 7<v1
             >> (addTo (OLess va v1)  
             (addTo (OLess va v2)
             (addTo (OLess v1 vc) 
             (addTo (OLess v2 vc)
             (return ()))))) >> 
                     dynLabelling [va,v1,vb,v2,vc] [v1,v2])))))
--               enumerateDyn [va,v1,vb,v2,vc] >> assignments [v1,v2])))))
 
-----------------------------------------------------------------------------

{-
allinOV :: [FDVar] -> Int -> Int -> Free (CP OvertonFD FDVar) Bool 
allinOV ls x y = 
    exists (\xq ->
    exists (\yq ->  
     addTo (OHasValue xq x)
     (addTo (OHasValue yq y)             
     (multiAdd [ OLess xq q | q <- ls ] >> multiAdd [ OLess q yq | q <- ls ]))))
-}


alldiffOVu :: [FDVar] -> Free (CP OvertonFD FDVar) () 
alldiffOVu ls = multiAddU [ ODiff qi qj | qi:qls <- tails ls, qj <- qls ]

alldiffOV :: [FDVar] -> Free (CP OvertonFD FDVar) Bool 
alldiffOV ls = multiAdd [ ODiff qi qj | qi:qls <- tails ls, qj <- qls ]

allinOV :: [FDVar] -> Int -> Int -> Free (CP OvertonFD FDVar) Bool 
allinOV ls x y = multiAdd [ OInDomain q (Range x y) | q <- ls ]


diagonals1 :: [FDVar] -> Free (CP OvertonFD FDVar) Bool 
diagonals1 ls = conjB [ 
   exists (\qm ->
   exists (\qn ->
     addTo (OSub qi qj qm) 
     (addTo (OHasValue qn (i - j)) 
     (addTo (ODiff qm qn) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 

diagonals2 :: [FDVar] -> Free (CP OvertonFD FDVar) Bool 
diagonals2 ls = conjB [ 
   exists (\qm ->
   exists (\qn ->
     addTo (OSub qi qj qm) 
     (addTo (OHasValue qn (j - i)) 
     (addTo (ODiff qm qn) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 

diagonals :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals ls d = conjB [ 
   exists (\qm ->
     addTo (OInDomain qm d)  
     (addTo (OSub qi qj qm) 
     (addTo (OHasNotValue qm (i - j)) 
     (addTo (OHasNotValue qm (j - i)) (return True))))) | 
                  (qi,i):qls <- tails (zip ls [1..]), (qj,j) <- qls ] 


diagonals03 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals03 ls d = conjB [ 
     (addTo (OHasNotValue qi i) (return True)) | 
                  (qi,i) <- (zip ls [1..]) ] 


diagonals04 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonals04 ls d = conjB [ 
   exists (\qm -> 
     addTo (OHasValue qm i)  
     (addTo (ODiff qi qm) (return True))) | 
                  (qi,i) <- (zip ls [1..]) ] 

diagonalsBad1 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonalsBad1 ls d = conjB [ 
   exists (\qm -> 
     addTo (OInDomain qm (Set (IntSet.fromList [(-3),(-1),0,1,3])))  
     (addTo (OSub qi qj qm)  
     (addTo (OHasNotValue qm 2) (return True)))) | 
            qi:qls <- tails ls, qj <- qls ] 




diagonalsBad2 :: [FDVar] -> Domain -> Free (CP OvertonFD FDVar) Bool 
diagonalsBad2 ls d = conjB [ 
   exists (\qm -> 
   (exists (\qj ->
     addTo (OHasValue qj 1) 
     (addTo (OInDomain qm (Set (IntSet.fromList [1,3])))  
     (addTo (OSub qi qj qm)  
     (addTo (OHasNotValue qm 2) (return True))))))) | 
            qi <- ls ] 


nqueensA :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensA ls = allinOV ls 1 4 >> alldiffOV ls >> diagonals ls (Range (-4) 4) 
       >> trace (concat [lookupAndShow x ++ " ;; " | x <- []]) (alldiffOV ls)

-- without tracing 
nqueens :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueens ls = allinOV ls 1 4  >> alldiffOV ls >> diagonals ls (Range (-4) 4) 
-- >> (alldiffOVx ls)

nqueensN :: Int -> [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensN n ls = allinOV ls 1 n  >> alldiffOV ls >> diagonals ls (Range (-n) n) 

-- with tracing 
nqueensT :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensT ls = allinOV ls 1 4  >> traceDomain ls >> alldiffOV ls >> traceDomain ls    >> diagonals ls (Range (-4) 4) >> traceDomain ls
-- >> (alldiffOVx ls)


nqueensX :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensX ls = allinOV ls 1 4 >> alldiffOV ls -- >> diagonals ls 


nqueensY :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensY qs@(a:b:ls) = allinOV qs 1 4 >> alldiffOV qs >> 
   (exists (\qm ->
      addTo (OInDomain qm (Range 0 4))
      (addTo (OSub a b qm) 
      (addTo (OHasValue qm 1) (return True)))))


nqueensZ :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensZ qs@(a:b:ls) = allinOV ls 1 4 >> alldiffOV qs >> 
   (exists (\qm ->
      addTo (OSub a b qm) 
      (addTo (OHasNotValue qm 1) (return True))))


nqueensW :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
nqueensW ls = allinOV ls 1 4 >> alldiffOV ls >> 
   conjB [ exists (\qm ->
      addTo (OSub qi qj qm) 
      (addTo (OHasNotValue qm 1) (return True))) |
                           qi:qls <- tails ls, qj <- qls ] 



{-
queensDynX :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDynX = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in
          allinOV ls 1 4 >>
          (addTo (ODiff v1 v2) 
          (addTo (ODiff v1 v3)
          (addTo (ODiff v1 v4)
          (addTo (ODiff v2 v3)
          (addTo (ODiff v2 v4)
          (addTo (ODiff v3 v4)
          (return())))))))        
          >> dynLabelling ls ls))))
-}


--queens :: Free (CP OvertonFD FDVar) a -> Free (CP OvertonFD FDVar) a
queens :: Free (CP OvertonFD FDVar) ()
queens = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueens ls >> enumerateOV ls [1..4])))) 
--          alldiffOV ls >> diagonals1 ls >> diagonals2 ls)))))


queensDyn3 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDyn3 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          let ls = [v1,v2,v3] in 
              nqueensN 3 ls >> dynLabelling ls ls)))


queensDyn4 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDyn4 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueensN 4 ls >> dynLabelling ls ls))))

queensDyn5 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDyn5 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          exists (\v5 ->
          let ls = [v1,v2,v3,v4,v5] in 
              nqueensN 5 ls >> dynLabelling ls ls)))))

queensDyn6 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDyn6 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          exists (\v5 ->
          exists (\v6 ->
          let ls = [v1,v2,v3,v4,v5,v6] in 
              nqueensN 6 ls >> dynLabelling ls ls))))))

queensDyn7 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDyn7 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          exists (\v5 ->
          exists (\v6 ->
          exists (\v7 ->
          let ls = [v1,v2,v3,v4,v5,v6,v7] in 
              nqueensN 7 ls >> dynLabelling ls ls)))))))

queensDyn8 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensDyn8 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          exists (\v5 ->
          exists (\v6 ->
          exists (\v7 ->
          exists (\v8 ->
          let ls = [v1,v2,v3,v4,v5,v6,v7,v8] in 
              nqueensN 8 ls >> dynLabelling ls ls))))))))




knapsackDyn :: Int -> [Int] -> 
         Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
knapsackDyn w vs | w < 0 = false
                 | w == 0 = Return []
                 | w > 0 = do v <- (foldr (\/) false . map Return) vs
                              vs' <- knapsackDyn (w - v) vs
                              return (v:vs') 
                 




{-
knapsackDyn :: Int -> DMap Int Int -> 
         Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
knapsackDyn w ms = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] 
          let domK = mkDom (keys ms) 
          let domR = mkDom (range ms)
          allinDom [v1,v2,v3,v4,vx] domK >>
          alldiffOV [v1,v2,v3,v4,vx] >>
          allhasVal [v5,v6,v7,v8] [v1,v2,v3,v4] (get ms) >>
          minimise vx (get ms) vy
          allSum [v5,v6,v7,v8] v9
          allSum [v5,v6,v7,v8,vy] v10
          Less v9 w
          Greater v10 w
          return [v5,v6,v7,v8] >> dynLabelling ls ls))))
-}


subExper1 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
subExper1 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          allinOV [v1,v2,v3] 1 5 >>
          alldiffOV [v1,v2] >>
          (addTo (OSub v1 v2 v3) 
          (addTo (OHasNotValue v3 1) (return ()))) >> 
          dynLabelling [v1,v2,v3] [v1,v2,v3])))

subExper2 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
subExper2 = exists (\v1 -> 
          allinOV [v1] 1 5 >>
          (addTo (OHasNotValue v1 3) (return ())) >> 
          dynLabelling [v1] [v1])


runSubExper = print $ solve subExper1

runQueensAnsSt = print $ solve queens


runQueensAns3 = print $ solve queensDyn3

runQueensAns4 = print $ solve queensDyn4

runQueensAns5 = print $ solve queensDyn5

runQueensAns6 = print $ solve queensDyn6

runQueensAns7 = print $ solve queensDyn7

runQueensAns8 = print $ solve queensDyn8


-- TermBaseType OvertonFD FDVar = Int (from OvertonFD.hs)
queensAns :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensAns = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueens ls >> enumerateDyn ls >> assignments ls)))) 

queensAns0 :: Free (CP OvertonFD FDVar) [TermBaseType OvertonFD FDVar]
queensAns0 = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueens ls >> enumerateOV ls [1..4] >> assignments ls)))) 



---------------------------------------
runEx0 = print $ solve model0

runEx1 = print $ solve model1

runExDB1 = print $ solveDB 1 model1

runExNB1 = print $ solveNB 1 model1

runAns7 = print $ solve modelAns7

-- runAns8 = print $ solve modelAns8
runAns8 = solveBasic modelAns8

-- runAns8DB = print $ solveDB 100 modelAns8
runAns8DB = solveBasicDB 100 modelAns8

-- runAns8NB = print $ solveNB 19 modelAns8
runAns8NB = solveBasicNB 19 modelAns8

-- runAns8DBx = print $ solveDB 10 modelAns8
runAns8DBx = solveBasicDB 10 modelAns8

-- runAns8NBx = print $ solveNB 18 modelAns8
runAns8NBx = solveBasicNB 18 modelAns8


noattack :: (Num a, FunctorTree f t,
      ModelExprClass a,
      Constraint (TreeSolver f t) ~ Either Model q) =>
     a -> a -> a -> a -> Free (f t) ()
noattack i j qi qj = do
  qi        @/=  qj
  qi  +  i  @/=  qj  +  j
  qi  -  i  @/=  qj  -  j




{-
queensDyn :: Free (CP OvertonFD FDVar) Bool
queensDyn = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          let ls = [v1,v2,v3,v4] in 
              nqueens ls >> enumerateDyn ls))))
-}
{-
enumerateOV :: [FDVar] -> [Int] -> Free (CP OvertonFD FDVar) Bool
enumerateOV vars values = conjB [ enumOV var values | var <- vars ] 

-- essentially, gives 
-- disj(x1,x2,...|v) = (v=x1) \/ (v=x2) \/ ... \/ false :: 
--                                           Free ((CP OvertonFD FDVar) Bool
-- where a1 \/ a2 = Op (Try a1 a2)
enumOV :: FDVar -> [Int] -> Free (CP OvertonFD FDVar) Bool
enumOV var values = disjB $ multiConstr [ OHasValue var val | val <- values ] 

-- Dynamic
propagate :: [FDVar] -> OvertonFD (Free (CP OvertonFD FDVar) Bool)
propagate [] = return (return True)
propagate (v:vs) = do d <- fd_domain v -- reads the current constraint store
                      return $ enumOV v d >> enumerateDyn vs  

-- gives
--  label (return ((disj(d1|v1) /\ label(return (disj(d2|v2) /\ ... /\ label (return true)...))))) 
enumerateDyn :: [FDVar] -> Free (CP OvertonFD FDVar) Bool
enumerateDyn = label . propagate 
-}

------------------------------------------------------------
{-
assignment :: (EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  t -> Free (f t) (TermBaseType s t)
assignment q = label $ getValue q >>= \y -> (case y of Just x -> return $ return x; _ -> return false)

assignments :: (EnumTerm s t, FunctorTree f t, TreeSolver f t ~ s) => 
                  [t] -> Free (f t) [TermBaseType s t]
assignments = mapM assignment
-}

{-
queen :: Free (CP OvertonFD FDVar) a -> Free (CP OvertonFD FDVar) a
queen x = exists (\v1 -> 
          exists (\v2 ->  
          exists (\v3 ->
          exists (\v4 ->
          exists (\v5 ->             

             addTo (OHasValue v1 2)
             (addTo (OHasValue v4 8)
             (addTo (OLess v1 v2)  
             (addTo (OLess v2 v3)
             (addTo (OLess v3 v4) x)))))))) 
-}

--                  (label (
--                     mapM fd_domain [v2,v3] >>= return . return))))))))))

--------------------------------------------------------------------------

-- model :: ExampleModel FDVar ModelInt
{-
model n = exists $ \p -> do
  size p @= n
  p `allin` (cte 0,n-1)
  allDiff p
  loopall (cte 0,n-2) $ \i -> 
    loopall (i+1,n-1) $ \j ->
      noattack i j (p!i) (p!j)
  return p
-}
-- main = example_sat_main_single_expr model


{-
model2 :: Free (CP OvertonFD FDVar) [Int] -- ModelCol
model2 =   Op (NewVar (\v -> 
                    (return (Label (fd_domain v >>= Op (Add (OHasValue v 3)))))))
-}
{-
model2 :: Free (CP OvertonFD FDVar) [Int] -- ModelCol
model2 =   Op (NewVar (\v -> Op (Add (OHasValue v 3) 
                    (return (Label (fd_domain v))))))
-}
--                           (Op (Label (fd_domain v >>= \v -> return v))))))

--                             (label . fd_domain v))))
--                                (Return 0))))


{-
model :: ExampleModel () (Expr (ModelIntTerm ModelFunctions)
             (ModelColTerm ModelFunctions)
             (ModelBoolTerm ModelFunctions))
model _ = exists $ \arr -> do
  arr `allin` (cte 0,cte 10)
  size arr @= 4
  xsum arr @= 10
  xsum (xmap (\x -> x*x) arr) @= 30
  sorted arr
  return arr
-}
{-
main = example_main_void model

model :: ConcreteExModel ()
model _ = exists $ \arr -> do
  arr `allin` (cte 0,cte 10)
  size arr @= 4
  xsum arr @= 10
  xsum (xmap (\x -> x*x) arr) @= 30
  sorted arr
  return arr

runEx = example_sat_main_void model EV
-}

