-- Based on the MiniZinc 2.0 Specification
-- See http://www.minizinc.org/2.0/doc-lib/minizinc-spec.pdf

module MZTypes (
  MZModel,
  Item(..),
  Expr(..),
  VarType(..),
  Bop(..),
  Uop(..),
  Func(..),
  Inst(..),
  Solve(..),
  CompTail,
  Generator,
  TypeInst,
  Param,
  Ident,
  Filename
) where 

type MZModel = [Item]

data Item = Comment String
          | Include Filename
          | Declare Inst VarType Ident (Maybe Expr)
          | Assign Ident Expr
          | Constraint Expr
          | Solve Solve
          | Output Expr
          | Pred Ident [Param] (Maybe Expr)
          | Test Ident [Param] (Maybe Expr)
          | Function TypeInst Ident [Param] (Maybe Expr)
          | Annotation
          | Empty        
  deriving Eq
          
data Expr = AnonVar
          | Var Ident
          | BConst Bool
          | IConst Int
          | FConst Float
          | SConst String
          | Interval Expr Expr
          | SetLit [Expr]
          | SetComp Expr CompTail
          | ArrayLit [Expr]
          | ArrayLit2D [[Expr]]
          | ArrayComp Expr CompTail
          | ArrayElem Ident [Expr]
          | Bi Bop Expr Expr
          | U Uop Expr
          | Call Func [Expr]
          | ITE [(Expr, Expr)] Expr
          | Let [Item] Expr
          | GenCall Func CompTail Expr 
  deriving Eq
          
data VarType = Bool
             | Int
             | Float
             | String
             | Set VarType
             | Array [VarType] TypeInst
             | List TypeInst
             | Opt VarType
             | Range Expr Expr
             | Elems [Expr]
             | AOS Ident
             | Any
  deriving Eq

data Bop = Gt | Lt | Gte | Lte | Eqq | Eq | Neq         -- Comparison
         | BPlus | BMinus | Times | Div | IDiv | Mod    -- Arithmetic
         | LRarrow | Rarrow | Larrow | And | Or         -- Logical
         | In | Sub | Super | Union | Inters            -- Sets
         | Diff | SDiff | RangeOp
         | Concat                                       -- Arrays
  deriving Eq
         
data Uop = Not
         | UPlus 
         | UMinus
 deriving Eq

data Func = UserD Ident
          | BoolToInt | IntToFloat | SetToArray
          | Forall | Xorall | Exists
          | Domain | ArrDom | SizeDom
          | Occurs | Absent | Deopt
          | Assert | Abort | Trace | Fix | IsFixed
          | Show | ShowInt | ShowFloat
          | Sum | Product
          | Min | Max
          | Abs | Sqrt
          | Power | Exp | Ln | Log
          | Sin | Cos | Tan | Asin | Acos | Atan | Sinh | Cosh | Tanh | Asinh | Acosh | Atanh
          | Card | ArrUnion | ArrInters
          | Length | Index | Index12 | Index22
          | Array1D | Array2D | Array3D | Array4D | Array5D | Array6D
          | Ceil | Floor | Round
          | MConcat | Join
          | Lb | Ub | LbArray | UbArray
          | QuotBop Bop
  deriving Eq

data Inst = Par | Dec
  deriving Eq

data Solve = Satisfy
           | Minimize Expr
           | Maximize Expr
  deriving Eq

type CompTail = ([Generator], Maybe Expr)

type Generator = ([Ident], Expr)
 
type TypeInst = (Inst, VarType)

type Param = (Inst, VarType, Ident)

type Ident = String

type Filename = String
