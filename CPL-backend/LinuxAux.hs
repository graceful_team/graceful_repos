{-
  There is some configuration needed before one can run
  a model. This script works for Linux. See comments below.
-}


module LinuxAux(
 module Posix,
 mfzn,
 flatzinc,
 cp,
 chocoSolver,
 chocoParser,
 antlr  
) where

import System.Process
import System.FilePath.Posix as Posix

-- Replace with YOUR path to the MiniZinc installation directory
-- Remember to escape backslash and quotes!
minizincInstallPath = "\"/home/ptorr/lib/MiniZincIDE-2.0.10-bundle-linux-x86_64/" --mzn2fzn and flatzinc executables

chocoLibsPath   = "choco/" --choco-solver-3.3.3-with-dependencies.jar, choco-parsers-3.3.2.jar and antlr-4.5.1-complete.jar
mfzn      = minizincInstallPath ++ "mzn2fzn\" "
flatzinc  = minizincInstallPath ++ "flatzinc\" "
cp        = chocoLibsPath

chocoSolver = "choco-solver-3.3.3-with-dependencies.jar"
chocoParser = "choco-parsers-3.3.3.jar"
antlr       = "antlr-4.5.2-complete.jar"

