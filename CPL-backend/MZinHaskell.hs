module MZinHaskell (
  module MZPrinter,
  testModel,
  writeData
) where

{-
  Changes must be made and in the imports and either in WindowsAux 
  (if you run with Windows) or LinuxAux (if you run with 
  Linux). See comments in those files.
-}

-- Change 'WindowsAux' to 'LinuxAux' to run with Linux.
  import WindowsAux
  -----------------

  import System.Process
  import MZPrinter
  import MZOutputParser


  -- Auxiliary datatype for testModel function
  type TestArgs = (String, String)

  -- Runs a model
  testModel :: MZModel -> IO ()
  testModel m = do
    putStrLn "Enter MiniZinc model's filepath (without .mzn extention):"
    path <- getLine
    let (filename, ext) = splitExtension path
    putStrLn "Is there a data file? If yes, provide its filepath:"
    dpath <- getLine
    writeFile (path++".mzn") (show $ printModel m)
    readCreateProcess (shell (mfzn ++ path ++ ".mzn " ++ dpath)) ""
    putStrLn "Type \"fd\" for G12/FD solver or leave empty for choco solver."
    solver <- getLine
    putStrLn "Enter 0 to output all solutions."
    mode <- getLine
    let opt = if mode == "0"
              then " -a "
              else " "
    res <- if solver /= "" 
           then readCreateProcess (shell $ flatzinc ++ opt ++ "-b fd " ++ filename ++ ".fzn > " ++ filename ++ ".fzn.results.txt") ""
           else readCreateProcess (shell $ "java -cp ." ++ [searchPathSeparator] ++ cp ++ chocoSolver ++ [searchPathSeparator] ++ cp ++ chocoParser ++ [searchPathSeparator] ++ cp ++ antlr ++ " org.chocosolver.parser.flatzinc.ChocoFZN" ++ opt ++ filename ++ ".fzn> " ++ filename ++ ".fzn.results.txt") ""
    getSolution $ filename ++ ".fzn.results.txt"

  -- Writes the model's data file
  writeData :: MZModel -> IO ()
  writeData m = do
    putStrLn "Enter MiniZinc datafile's filepath:"
    datapath <- getLine
    writeFile datapath (show $ printModel m)
