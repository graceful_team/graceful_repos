{-
  There is some configuration needed before one can run
  a model. This script works for Windows. See comments below.
-}

module WindowsAux(
 module Windows,
 mfzn,
 flatzinc,
 cp,
 chocoSolver,
 chocoParser,
 antlr,
 spaceFix  
) where


import System.Process
import System.FilePath.Windows as Windows

-- Replace with YOUR path to the directory where mzn2fzn and flatzinc executables are.
-- Remember to escape backslash and quotes!
minizincInstallPath = "C:\\Program Files (x86)\\MiniZinc 2\\bin\\"

chocoLibsPath    = "choco\\"    --choco-solver-3.3.3-with-dependencies.jar, choco-parsers-3.3.2.jar and antlr-4.5.1-complete.jar
mfzn        = spaceFix $ minizincInstallPath ++ "mzn2fzn"
flatzinc    = spaceFix $ minizincInstallPath ++ "flatzinc"
cp          = spaceFix chocoLibsPath

-- Function only needed for filepaths in Windows
spaceFix :: String -> String
spaceFix str = if elem ' ' str
               then "\"" ++ str ++ "\" "
               else str

chocoSolver = "choco-solver-3.3.3-with-dependencies.jar"
chocoParser = "choco-parsers-3.3.3.jar"
antlr       = "antlr-4.5.2-complete.jar"

