-- FlatZinc language specification

module FZinc where

data FlatZincModel = FZModel { pred_decls  :: PredDecls, 
                               var_decls   :: VarDecls, 
                               constraints :: Constraints,
                               solve_goal  :: SolveGoal } 

type PredDecls = [PredDecl]

type VarDecls = [VarDecl]

type Constraints = [Constraint]

---------------------------------------------------------------

-- auxiliary
type PredName = String

type PredAnnId = String

type VarParId = String

newtype BoolConst = BConst Bool 

newtype FloatConst = FConst Float

newtype IntConst = IConst Int


----------------------------------------------------------------

type ArrayExpr = [ZExpr]

data SetConst = IInterval { int_start :: IntConst,
                               int_end   :: IntConst }
                 | Set { int_first :: IntConst,
                         int_rest  :: [IntConst] }

-- auxiliary 
data FloatSetConst = FInterval { float_start :: FloatConst,
                                 float_end   :: FloatConst }

data IndexSet = SimpleIndexSet Int | IntervalIndexSet IntConst

----------------------------------------------------------------

data PredDecl = Predicate { pred_identifier :: PredName,
                            pred_params     :: PredParams }
  
data PredParam = PredPar { pred_ann_identifier :: PredAnnId,
                           pred_param_type     :: PredParamType }

data PredParams = PredPars { par_first :: PredParam,
                             par_rest  :: [PredParam] }

data PredParamType = PPPT { par_pred_param_type  :: ParPredParamType } 
                   | VPPT { var_pred_param_type :: VarPredParamType }

------------------------------------------------------------------

-- auxiliary 
data SimpleType = ZBool  
                | ZFloat 
                | ZInt 
                | ZIntSet

data ParType = Simple { simple_type :: SimpleType }
             | ArrayType { array_type  :: SimpleType,
                           a_index_set :: IndexSet }

-- auxiliary
data IntTag = IInt 
            | Subset 
            | ArrayOfIntInterval { aoi_index_set :: IndexSet } 
            | ArrayOfSubset { aos_index_set :: IndexSet }

-- auxiliary
data FloatTag = FloatIntererval 
              | ArrayOfFloatInterval { aof_index_set :: IndexSet }

data ParPredParamType = ParT { par_pp_type :: ParType }
     | FloatInterval { float_tag   :: FloatTag, 
                       float_set :: FloatSetConst }
     | IntInterval { int_tag   :: IntTag,
                     int_set   :: SetConst }

-----------------------------------------------------------------


data VarPredParamType = VatT { var_pp_type :: VarType }
                      | IntSetVarT
                      | IntSetVarArrayT { isva_index_set :: IndexSet }

data VarType = BoolVar
             | FloatVar
             | IntVar
             | FoatIntervalVar { float_v_tag   :: FloatTag,
                                 float_v_set   :: FloatSetConst }
             | IntSetVar { int_v_tag   :: IntTag,
                           int_v_set   :: SetConst } 
             | BoolVarArray { bv_index_set  :: IndexSet }
             | FloatVarArray { fv_index_set :: IndexSet }
             | IntVarArray { iv_index_set   :: IndexSet }


---------------------------------------------------------------
 
type BoolArray = [BoolConst]

type FloatArray = [FloatConst]

type IntArray = [IntConst]

type SetArray = [SetConst]

data ExtSimpleExpr = BExpr BoolConst 
                   | FExpr FloatConst
                   | IExpr IntConst 
                   | SExpr SetConst 
                   | BArray BoolArray
                   | FArray FloatArray
                   | IArray IntArray
                   | SArray SetArray

data ZExpr = SimpExpr ExtSimpleExpr    
           | VPExpr VarParId (Maybe IntConst)
           | ArrayExpr ZExpr

type SimpleExpr = 
       Either BoolConst (Either FloatConst (Either IntConst SetConst))

----------------------------------------------------------------

data ParamDecl = ParamD { par_type     :: ParType,
                          p_var_par_id :: VarParId,
                          expr         :: ExtSimpleExpr }


data VarDecl = VarD { var_type     :: VarParId,
                      v_var_par_id :: VarParId,
                      annotations  :: Annotations,
                      opt_expr     :: Maybe ZExpr }
                          
data Constraint = CC { pred_name   :: PredName,
                       expr_first  :: ZExpr,
                       expr_rest   :: [ZExpr],
                       annos       :: Annotations }

type Annotations = [Annotation]

data Annotation = Ann { pred_ann_id    :: PredAnnId,
                        a_expr_first   :: ZExpr,
                        a_expr_rest    :: [ZExpr] }

data SolveGoal = SatSolve { sat_annos :: Annotations }
               | MinSolve { min_annos :: Annotations,
                            min_expr  :: SimpleExpr }            
               | MaxSolve { max_annos :: Annotations,
                            max_expr  :: SimpleExpr }            

-------------------------------------------------------