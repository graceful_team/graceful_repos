== REQUIREMENTS ==
 * GHC 7.10.3
 * MiniZinc 2.0
 * JDK 8+

== Installation ==

  1. You will need to provide the directory of the MiniZinc bundle. Depending on 
     your OS, follow the instructions in WindowsAux.hs or LinuxAux.hs.
  2. You will then, need, to import module WindowsAux or LinuxAux. Depending on
     your OS, make the appropriate changes in testPrinter.hs, as indicated in 
     the comments.
  

